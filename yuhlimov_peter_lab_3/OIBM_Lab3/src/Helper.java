public class Helper {
	// Calculation
	public static String calculation(int[] numOne, int[] numTwo, String operation) {
		String result = "";
		try {
			if (operation.equals("+")) {
				result = BynarOperations.sum(numOne, numTwo);
			} else if (operation.equals("-")) {
				result = BynarOperations.sub(numOne, numTwo);
			} else if (operation.equals("*")) {
				result = BynarOperations.multiply(numOne, numTwo);
			} else if (operation.equals("/")) {
				result = BynarOperations.div(numOne, numTwo);
			} else {
				throw new Exception("Invalid operation");
			}
		}
		catch (Exception e) {
			System.out.print(e.getMessage());
		}
		return result;
	}

	// Conversion to decimal notation
	public static int toTen(String numb, int NowO) {
		int degree = 0;
		int r_numb = 0;
		int[] arr_numb = toArrNumbs(numb);
		if (arr_numb == null) {
			return (Integer) null;
		}
		for (int i = arr_numb.length - 1; i >= 0; i--) {
			int a = (int) Math.pow(NowO, degree);// Base in the degree of discharge
			int b = arr_numb[i];// Digit of the number
			r_numb += a * b;
			degree++;
		}
		return r_numb;
	}

	// Converting a string representing a number to an array of digits of that
	// number
	public static int[] toArrNumbs(String numb) {
		int[] arr = new int[numb.length()];
		char[] item = numb.toCharArray();

		for (int i = 0; i < item.length; i++) {
			String safe_str = "" + item[i];
			if (isDigit(safe_str)) {
				arr[i] = Integer.parseInt(safe_str);
			} else {
				switch (safe_str) {
				case "A":
					arr[i] = 10;
					break;
				case "B":
					arr[i] = 11;
					break;
				case "C":
					arr[i] = 12;
					break;
				case "D":
					arr[i] = 13;
					break;
				case "E":
					arr[i] = 14;
					break;
				case "F":
					arr[i] = 15;
					break;
				default:
					return null;
				}
			}
		}

		return arr;
	}

	// Conversion to binary notation
	public static int[] toTwo(int numb) {
		int safeNumb;
		int[] NextNumb = new int[20];
		int rang = 0;
		while (numb > 0) {
			safeNumb = numb / 2;
			NextNumb[rang] = numb - safeNumb * 2;
			rang++;
			numb = safeNumb;
		}
		int[] newNumb = new int[20];
		int newRang = 0;
		for (int i = rang - 1; i >= 0; i--) {
			newNumb[newRang] = NextNumb[i];
			newRang++;
		}
		return NextNumb;
	}

	// Method that checks whether a string is a number
	private static boolean isDigit(String s) throws NumberFormatException {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	// Converting an array of digits to a string representing a number
	public static String arrNumbToString(int[] result) {
		String str_numb = "";
		int newLength = 0;
		for (int i = result.length - 1; i >= 0; i--) {
			if (result[i] == 1) {
				newLength = i;
				break;
			}
		}
		for (int i = newLength; i >= 0; i--) {
			str_numb += result[i];
		}
		return str_numb;
	}

	// Comparison of two numbers written in an array
	public static boolean checkNumbers(int[] numb1, int[] numb2) {
		int num_len1 = Helper.arrNumbToString(numb1).length();
		int num_len2 = Helper.arrNumbToString(numb2).length();
		if (num_len1 > num_len2) {
			return true;
		}
		if (num_len1 < num_len2) {
			return false;
		}
		String str_numb = "";
		int newLength = 0;
		for (int i = 0; i < numb1.length; i++) {
			if (numb1[i] > numb2[i]) {
				return true;
			}
			if (numb1[i] < numb2[i]) {
				return false;
			}
		}
		return true;
	}
}

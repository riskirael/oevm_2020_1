public class MathCC {
    private static final int maxRank = 16;

    /**
     * Сложение
     */
    public static String augment(String value1, String value2) {
        boolean value1Negation = false;
        boolean value2Negation = false;

        // Убираем скобки //
        if (value1.indexOf('(') == 0) {
            value1 = parseToAdditionCode(value1);
            value1Negation = true;
        }
        if (value2.indexOf('(') == 0) {
            value2 = parseToAdditionCode(value2);
            value2Negation = true;
        }
        String result = sumBitByBit(value1, value2);
        result = convertToDesiredForm(result, value1Negation, value2Negation);
        return result;
    }

    private static String parseToAdditionCode(String value) {
        value = removeParentheses(value);
        value = value.replace('0', '.');
        value = value.replace('1', '0');
        value = value.replace('.', '1');
        StringBuilder valueSb = new StringBuilder("1111111111111111");
        valueSb.insert(valueSb.length() - value.length(), value);
        valueSb.delete(maxRank, maxRank * 2);
        value = valueSb.toString();
        value = augment(value, "1");
        return value;
    }

    // Удаляем лишние разряды и незначимые нули
    private static String deleteZero(String value) {
        if (value.length() >= maxRank) {
            value = value.substring(value.length() - maxRank);
        }
        if (value.indexOf('1') != -1) {
            value = value.substring(value.indexOf('1'));
        } else {
            value = "0";
        }
        return value;
    }

    private static String parseToDirectCodeFromAddition(String value) {
        value = augment(value, "(-1)");
        value = value.replace('0', '.');
        value = value.replace('1', '0');
        value = value.replace('.', '1');
        value = value.substring(value.indexOf('1'));
        return value;
    }

    /**
     * Вычитание
     */
    public static String subtract(String value1, String value2) {
        if (value2.toCharArray()[0] == '(') {
            value2 = removeParentheses(value2);
        } else {
            value2 = "(-" + value2 + ")";
        }
        return augment(value1, value2);
    }

    // Убираем скобки
    private static String removeParentheses(String value) {
        value = value.substring(2, value.length() - 1);
        return value;
    }

    /**
     * Умножение
     */
    public static String multiply(String value1, String value2) {
        boolean value1Negation = false;
        boolean value2Negation = false;

        if (value1.equals("0") || value2.equals("0")) {
            return "0";
        }

        if (value1.toCharArray()[0] == '(') {
            value1 = removeParentheses(value1);
            value1Negation = true;
        }
        if (value2.toCharArray()[0] == '(') {
            value2 = removeParentheses(value2);
            value2Negation = true;
        }

        char[] secondMultiplier = new StringBuilder(value2).reverse().toString().toCharArray();

        String result = "0";
        StringBuilder bitShift = new StringBuilder();
        for (char c : secondMultiplier) {
            if (c == '1') {
                result = augment(result, value1 + bitShift.toString());
            }
            bitShift.append(0);
        }

        if (!(value1Negation && value2Negation) && (value1Negation || value2Negation)) {
            result = "-" + result;
        }
        return result;
    }

    /**
     * Деление
     */
    public static String division(String value1, String value2) {
        boolean value1Negation = false;
        boolean value2Negation = false;
        StringBuilder resultSb = new StringBuilder();
        StringBuilder remains = new StringBuilder();
        char[] dividend = value1.toCharArray();

        if (value1.toCharArray()[0] == '(') {
            value1 = removeParentheses(value1);
            value1Negation = true;
        }
        if (value2.toCharArray()[0] == '(') {
            value2 = removeParentheses(value2);
            value2Negation = true;
        }

        if (value2.equals("0")) {
            return "ОШИБКА! ДЕЛИТЬ НА 0 НЕЗЯ!!!!!";
        }
        if (value1.equals("0") || subtract(value1, value2).toCharArray()[0] == '-') {
            return "0";
        }

        for (int i = 0; i < value1.length(); i++) {
            remains.append(dividend[i]);
            if (subtract(remains.toString(), value2).toCharArray()[0] == '-') {
                resultSb.append(0);
            } else {
                remains = new StringBuilder(subtract(remains.toString(), value2));
                resultSb.append(1);
            }
        }

        String result = resultSb.toString();
        result = deleteZero(result);
        if (!(value1Negation && value2Negation) && (value1Negation || value2Negation)) {
            result = "-" + result;
        }
        return result;
    }

    private static String sumBitByBit(String value1, String value2) {
        // Переворачиваем строку и переводим её в массив символов
        char[] value1CharArray = new StringBuilder(value1).reverse().toString().toCharArray();
        char[] value2CharArray = new StringBuilder(value2).reverse().toString().toCharArray();
        StringBuilder result = new StringBuilder();

        // Находим наименьшую длину значения, чтобы не выйти за границы массива
        int smallerLength = Math.min(value1CharArray.length, value2CharArray.length);

        // Добавляемый разряд при сложении
        boolean discharge = false;

        for (int i = 0; i < smallerLength; i++) {
            if (value1CharArray[i] == '0' && value2CharArray[i] == '0') {
                if (discharge) {
                    result.append(1);
                    discharge = false;
                } else {
                    result.append(0);
                }
            } else if (value1CharArray[i] == '1' && value2CharArray[i] == '1') {
                if (discharge) {
                    result.append(1);
                } else {
                    result.append(0);
                    discharge = true;
                }
            } else if (value1CharArray[i] == '1' || value2CharArray[i] == '1') {
                if (discharge) {
                    result.append(0);
                } else {
                    result.append(1);
                }
            }
        }

        // Массив с большим значением, чтобы добавить к ответу его оставшиеся разряды
        char[] biggerArray;
        if (value1CharArray.length >= value2CharArray.length) {
            biggerArray = value1CharArray;
        } else {
            biggerArray = value2CharArray;
        }

        for (int i = smallerLength; i < biggerArray.length; i++) {
            if (biggerArray[i] == '0') {
                if (discharge) {
                    result.append(1);
                    discharge = false;
                } else {
                    result.append(0);
                }
            } else if (biggerArray[i] == '1') {
                if (discharge) {
                    result.append(0);
                } else {
                    result.append(1);
                }
            }
        }
        if (discharge) {
            result.append(1);
        }
        return result.reverse().toString();
    }

    private static String convertToDesiredForm(String result, boolean value1Negation, boolean value2Negation) {
        if (value1Negation && value2Negation) {
            result = parseToDirectCodeFromAddition(result);
            result = "-" + deleteZero(result);
        } else if (value1Negation || value2Negation) {
            if (result.length() == maxRank) {
                result = '-' + parseToDirectCodeFromAddition(result);
            } else {
                result = deleteZero(result);
            }
        }
        return result;
    }

}

import javax.swing.*;

public class Frame extends JFrame {
    private Frame(String name) {
        super(name);
    }

    public static Frame createFrame() {
        Frame frame = new Frame("Визуализация минимизации");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(700, 700);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setLayout(null);
        return frame;
    }

    public void initButtons(VisualizePanel visualizePanel) {
        JButton buttonUpdate = new JButton("Обновить");
        JButton buttonMinimize = new JButton("Минимизировать");
        getContentPane().add(buttonUpdate);
        getContentPane().add(buttonMinimize);
        buttonUpdate.setBounds(20, 540, 150, 30);
        buttonMinimize.setBounds(20, 575, 150, 30);
        buttonUpdate.addActionListener(e -> {
            visualizePanel.setTruthTable(TruthTable.getTruthTable());
            visualizePanel.clear();
            repaint();
        });
        buttonMinimize.addActionListener(e -> visualizePanel.minimizeStart(this));
    }

    public void setVisualizePanel(VisualizePanel visualizePanel) {
        getContentPane().add(visualizePanel);
        visualizePanel.setBounds(0, 0, getWidth(), getHeight());
    }
}

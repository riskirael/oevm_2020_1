package com.company;

public class Main {

    public static void main(String[] args) {
        Pascal PascalCode = new Pascal();

        Assembler assemblerCode = new Assembler();

        Parser Parser = new Parser(PascalCode.read(), assemblerCode);
        Parser.parseBody();

        assemblerCode.createResult();

    }
}

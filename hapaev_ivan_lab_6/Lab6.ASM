format PE console

entry start

include 'win32a.inc'

section '.data' data readable writable
        ;����� � �������
        strX db 'Enter X: ', 0
        strY db 'Enter Y: ', 0
        inputStr db ' %d', 0

        addStr db 'X+Y=%d', 0dh, 0ah, 0
        subtractStr db 'X-Y=%d', 0dh, 0ah, 0
        multipryStr db 'X*Y=%d', 0dh, 0ah, 0
        divideStr db 'X/Y=%d', 0

        X dd ?
        Y dd ?

        NULL = 0

section '.code' code readable executable

        start:
                push strX
                call [printf]

                push X
                push inputStr
                call [scanf]

                push strY
                call [printf]

                push Y
                push inputStr
                call [scanf]

                ;��������
                    mov ecx, [X]
                    add ecx, [Y]

                    push ecx
                    push addStr
                    call [printf]

                ;���������
                    mov ecx, [X]
                    sub ecx, [Y]

                    push ecx
                    push subtractStr
                    call [printf]

                ;���������
                    mov ecx, [X]
                    imul ecx, [Y]

                    push ecx
                    push multipryStr
                    call [printf]

                ;�������
                    mov eax, [X]
                    cdq
                    mov ecx, [Y]
                    idiv ecx

                    push eax
                    push divideStr
                    call [printf]

                call [getch];      

                push NULL
                call [ExitProcess]

section '.idata' import data readable
        ;����������� ���������
        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvcrt,\
               printf, 'printf',\
               scanf, 'scanf',\
               getch, '_getch'
          


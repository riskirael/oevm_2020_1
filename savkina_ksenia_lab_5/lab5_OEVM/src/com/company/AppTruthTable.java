package com.company;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AppTruthTable {

    private JFrame frame;
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    AppTruthTable window = new AppTruthTable();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public AppTruthTable() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(300, 100, 900, 625);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        TruthTable truthTable = new TruthTable();;
        TruthTablePanel truthTablePanel = new TruthTablePanel(truthTable);
        truthTablePanel.setBounds(0, 0, 690, 518);
        frame.getContentPane().add(truthTablePanel);

        JButton btnCreateTable = new JButton("<html>Создать таблицу</html>");
        btnCreateTable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                truthTable.createTable();
                truthTablePanel.repaint();
            }
        });
        frame.getContentPane().add(btnCreateTable);
        btnCreateTable.setBounds(5, 530, 190, 50);

        JButton btnKNF = new JButton("<html>Представление в КНФ</html>");
        btnKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                truthTable.presentInKNF();
                truthTablePanel.repaint();
            }
        });
        frame.getContentPane().add(btnKNF);
        btnKNF.setBounds(700, 360, 150, 50);

        JButton btnDNF = new JButton("<html>Представление в ДНФ</html>");
        btnDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                truthTable.presentInDNF();
                truthTablePanel.repaint();
            }
        });
        frame.getContentPane().add(btnDNF);
        btnDNF.setBounds(700, 100, 150, 50);
    }
}

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Window {
    NormalForm form = new NormalForm();
    TruthTable truthTable = new TruthTable();
    DrawTable dTable = new DrawTable();
    private int[][] table;
    private JFrame frame = new JFrame();
    private JButton CreateTable_button = new JButton("вывести таблицу");
    private JButton CreateDNF_button = new JButton("вывести ДНФ целиком");
    private JButton CreateCNF_button = new JButton("вывести КНФ целиком");
    private JButton CreateDNFInc_button = new JButton("вывести ДНФ пошагово");
    private JButton CreateCNFInc_button = new JButton("вывести КНФ пошагово");
    private JLabel DNF_label = new JLabel("ДНФ");
    private JLabel CNF_label = new JLabel("КНФ");
    private JTextArea DNF_text = new JTextArea();
    private JTextArea CNF_text = new JTextArea();


    public Window() {
        int width = 1900;
        frame.setVisible(true);
        frame.setBounds(0, 0, width, 800);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel Table_label = new JLabel("таблица истинности");
        CreateTable_button.setBounds(width - 280, 100, 220, 70);
        CreateDNF_button.setBounds(width - 280, 200, 220, 70);
        CreateCNF_button.setBounds(width - 280, 300, 220, 70);
        CreateDNFInc_button.setBounds(width - 280, 400, 220, 70);
        CreateCNFInc_button.setBounds(width - 280, 500, 220, 70);
        Table_label.setBounds(250, 70, 200, 20);
        DNF_label.setBounds(830, 70, 100, 20);
        CNF_label.setBounds(1230, 70, 100, 20);
        DNF_text.setBounds(700, 100, 300, 500);
        CNF_text.setBounds(1100, 100, 300, 500);

        frame.getContentPane().add(CreateTable_button);
        frame.getContentPane().add(CreateDNF_button);
        frame.getContentPane().add(CreateCNF_button);
        frame.getContentPane().add(CreateDNFInc_button);
        frame.getContentPane().add(CreateCNFInc_button);
        frame.getContentPane().add(Table_label);
        frame.getContentPane().add(DNF_label);
        frame.getContentPane().add(CNF_label);
        frame.getContentPane().add(DNF_text);
        frame.getContentPane().add(CNF_text);
        CreateDNF_button.setVisible(false);
        CreateCNF_button.setVisible(false);
        CreateDNFInc_button.setVisible(false);
        CreateCNFInc_button.setVisible(false);
        dTable.setBounds(250, 100, 1000, 650);
        frame.getContentPane().add(dTable);
        frame.repaint();
    }

    public void buttons() throws IOException {
        CreateTable_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                truthTable.fillTable();
                truthTable.printTable();
                table = truthTable.getTruthTable();
                dTable.setTable(table);
                DNF_text.setText("");
                CNF_text.setText("");
                zeroingDNF();
                zeroingCNF();
                CreateDNF_button.setVisible(true);
                CreateCNF_button.setVisible(true);
                CreateDNFInc_button.setVisible(true);
                CreateCNFInc_button.setVisible(true);
                frame.repaint();
            }
        });
        CreateDNF_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DNF_text.setText(form.convertToDisjunctiveNormalForm(table).toString());
                CreateDNFInc_button.setVisible(false);
                dTable.setDNFInc(false);
                dTable.setActiveDNFIncCEll(form.DNFIncRow,form.DNFIncColumn);
                frame.repaint();
            }
        });
        CreateCNF_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CNF_text.setText(form.convertToConjunctiveNormalForm(table).toString());
                CreateCNFInc_button.setVisible(false);
                dTable.setCNFInc(false);
                dTable.setActiveCNFIncCEll(form.CNFIncRow,form.CNFIncColumn);
                frame.repaint();
            }
        });
        CreateDNFInc_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String dnf=form.convertToDisjunctiveNormalFormInc(table).toString();
                DNF_text.setText(dnf);
                System.out.println(dnf);
                dTable.setDNFInc(true);
                dTable.setActiveDNFIncCEll(form.DNFIncRow,form.DNFIncColumn);
                frame.repaint();
            }
        });
        CreateCNFInc_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String CNF=form.convertToConjunctiveNormalFormInc(table).toString();
                CNF_text.setText(CNF);
                System.out.println(CNF);
                dTable.setCNFInc(true);
                dTable.setActiveCNFIncCEll(form.CNFIncRow,form.CNFIncColumn);
                frame.repaint();
            }
        });
    }
    public void zeroingDNF() {
        StringBuilder empty = new StringBuilder();
        form.setDNFInc(empty);
        form.setDNFIncColumn(0);
        form.setDNFIncRow(0);
        form.setDNFIncPlus(false);
        dTable.setDNFInc(false);
        dTable.setActiveDNFIncCEll(form.DNFIncRow,form.DNFIncColumn);
    }
    public void zeroingCNF() {
        StringBuilder empty = new StringBuilder();
        form.setCNFInc(empty);
        form.setCNFIncColumn(0);
        form.setCNFIncRow(0);
        form.CNFIncMultiplication(false);
        dTable.setCNFInc(false);
        dTable.setActiveCNFIncCEll(form.CNFIncRow,form.CNFIncColumn);
    }

}

public class Converter {

    public static String convert(int cc1, int cc2, String digit) {

        //константы для преобразования символов из ASCII в значение в десятичной системе счисления
        final int TOCHAR = 55;
        final int TODIGIT = 48;
        boolean minus = false;

        final String ERROR = "-1";
        
        char[] digitString = digit.toCharArray();
        long digit10 = 0;
        
        //преобразование в десятичную систему
        if (cc1 != 10) {
            for (int i = 0; i < digitString.length; i++) {

                double pow = Math.pow(cc1, digitString.length - i - 1);
                if (Character.isDigit(digitString[i])) {
                    if (digitString[i] - TODIGIT < cc1 && digitString[i] - TODIGIT >= 0) {
                        digit10 = (long) (digit10 + (digitString[i] - TODIGIT) * pow);
                    } else {
                        return ERROR;
                    }
                } else if (Character.isLetter(digitString[i])) {
                    if ((int) digitString[i] - TOCHAR < cc1 && (int) digitString[i] - TOCHAR >= 10) {
                        digit10 = (long) (digit10 + (digitString[i] - TOCHAR) * pow);
                    } else {
                        return ERROR;
                    }
                } else {
                    if (digitString[0] == '-') {
                        minus = true;
                    }
                    else {
                        return ERROR;
                    }
                }
            }
        } else {
            try {
                digit10 = Integer.parseInt(digit);
                if (digit10 < 0) {
                    digit10 = Math.abs(digit10);
                    minus = true;
                }
            } catch (Exception ignored) {
                return ERROR;
            }
        }
        
        //вывод ответа
        StringBuilder answer = new StringBuilder();
        if (digit10 == 0) {
            return "0";
        } else {
            while (digit10 > 0) {
                if (Character.isLetter((char) ((digit10 % cc2) + TOCHAR))) {
                    answer.append((char) ((digit10 % cc2) + TOCHAR));
                } else {
                    answer.append((char) ((digit10 % cc2) + TODIGIT));
                }
                digit10 = digit10 / cc2;
            }
            if (answer.length() > 8) {
                return "out";
            }
            if (minus) {
                answer.append("-(");
                answer.insert(0, ')');
            }
            return (answer.reverse().toString());
        }
    }
}

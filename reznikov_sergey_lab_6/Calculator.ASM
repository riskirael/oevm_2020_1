format PE console

entry Start

include 'win32a.inc'

section '.data' data readable writable

        strX db 'Enter X:', 0
        strY db 'Enter Y:', 0

        strAdd db '+', 0
        strSub db '-', 0
        strMul db '*', 0
        strMod db '/', 0

        resStr db 'X %s Y = %d', 0xA, 0
        resMod db '/%d', 0

        spaceStr db ' %d', 0
        emptyStr db '%d', 0

        infinity db 'infinity', 0
        point db ',', 0

        X dd ?
        Y dd ?
        C dd ?

        NULL=0

section '.code' code readable executable

        Start:
                push strX
                call [printf]

                push X
                push spaceStr
                call [scanf]

                push strY
                call [printf]

                push Y
                push spaceStr
                call [scanf]

                mov ecx, [X]
                add ecx, [Y]

                push ecx
                push strAdd
                push resStr
                call [printf]


                mov ecx, [X]
                sub ecx, [Y]

                push ecx
                push strSub
                push resStr
                call [printf]

                mov ecx, [X]
                imul ecx, [Y]

                push ecx
                push strMul
                push resStr
                call [printf]

                mov eax, [X]
                cdq
                mov ecx, [Y]
                idiv ecx

                push eax
                push strMod
                push resStr
                call [printf]

                finish:

                call[getch]

                push NULL
                call[ExitProcess]


section '.idata' import data readable

        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import  kernel,\
                ExitProcess, 'ExitProcess'

        import msvcrt,\
               printf, 'printf',\
               scanf, 'scanf',\
               getch,'_getch'

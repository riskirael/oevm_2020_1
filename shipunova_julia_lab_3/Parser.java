public class Parser {
    public static String convert(int ss1, int ss2, String value) {
        // Значение в 10 сс
        int ss10 = 0;
        int fromAsciiToDigit = 48;
        int fromAsciiToLetter = 55;
        boolean negation = false;

        String problem = "Ошибка программы!!!";

        // Переводим int в массив символов
        char[] valueString = value.toCharArray();

        // Проходим по каждому символу
        for (int i = 0; i < valueString.length; i++) {
            double powCC = Math.pow(ss1, valueString.length - 1 - i);

            // Проверяем является ли символ цифрой
            if (Character.isDigit(valueString[i])) {
                if (valueString[i] - fromAsciiToDigit < ss1 && valueString[i] - fromAsciiToDigit >= 0) {
                    // Если символ подходит под исходную сс, преобразуем его из АSCII
                    ss10 += (valueString[i] - fromAsciiToDigit) * powCC;
                } else {
                    return problem;
                }
            } else if (Character.isLetter(valueString[i])) {
                if (valueString[i] - fromAsciiToLetter < ss1 && valueString[i] - fromAsciiToLetter >= 0) {
                    // Если символ подходит под исходную сс, преобразуем его из АSCII
                    ss10 += (valueString[i] - fromAsciiToLetter) * powCC;
                } else {
                    return problem;
                }
            } else{
                if(i == 0 && valueString[i] == '-')
                {
                    negation = true;
                }
                else {
                    return problem;
                }
            }
        }

        // Значение в нужной сс
        StringBuilder valueSs2 = new StringBuilder();
        if (ss10 == 0) {
            return "0";
        } else {
            // Преобразуем в нужную сс
            while (ss10 > 0) {
                if (Character.isDigit(ss10 % ss2 + fromAsciiToDigit)) {
                    valueSs2.append((char) (ss10 % ss2 + fromAsciiToDigit));
                } else {
                    valueSs2.append((char) (ss10 % ss2 + fromAsciiToLetter));
                }
                ss10 /= ss2;
            }
            if (negation)
            {
                valueSs2.append("-(" );
                valueSs2.insert(0,')');
            }
            String answer = valueSs2.reverse().toString();
            // Отражаем строчку и возвращаем её
            return answer;
        }
    }
}

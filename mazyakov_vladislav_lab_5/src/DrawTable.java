import javax.swing.*;
import java.awt.*;

public class DrawTable extends JPanel {
    private int[] activeDNFIncCEll;
    private int[] activeCNFIncCEll;
    private int[][] truthTable;
    boolean tableExist = false;
    boolean DNFInc = false;
    boolean CNFInc = false;

    public void setDNFInc(boolean DNFInc) {
        this.DNFInc = DNFInc;
    }
    public void setActiveDNFIncCEll(int row, int column) {
        this.activeDNFIncCEll = new int[2];
        this.activeDNFIncCEll[0] = row;
        this.activeDNFIncCEll[1] = column;
    }

    public void setActiveCNFIncCEll(int row, int column) {
        this.activeCNFIncCEll = new int[2];
        this.activeCNFIncCEll[0] = row;
        this.activeCNFIncCEll[1] = column;
    }

    public void setCNFInc(boolean CNFInc) {
        this.CNFInc = CNFInc;
    }

    private int countRows = 16;
    private int countColumns = 5;

    public void setTable(int[][] table) {
        this.truthTable = table;
        tableExist = true;
    }

    public void paint(Graphics g) {
        g.setColor(Color.black);
        int widthCell = 20;
        int heightCell = 20;
        if (tableExist) {
            for (int i = 0; i < countRows; i++) {
                for (int j = 0; j < countColumns; j++) {
                    g.drawRect(widthCell * j, heightCell * i, widthCell, heightCell);
                    g.drawString(String.valueOf(truthTable[j][i]), widthCell * j + widthCell * 2 / 5, heightCell * i + heightCell * 4 / 5);
                }
            }
            if (DNFInc && activeDNFIncCEll[0] < countRows) {
                g.setColor(Color.blue);
                for (int j = 0; j < countColumns; j++) {
                    g.drawRect(widthCell * j, heightCell * activeDNFIncCEll[0], widthCell, heightCell);
                }
                g.setColor(Color.green);
                g.drawRect(widthCell * activeDNFIncCEll[1], heightCell * activeDNFIncCEll[0], widthCell, heightCell);
                g.setColor(Color.red);
                g.drawRect(widthCell * 4, heightCell * activeDNFIncCEll[0], widthCell, heightCell);
            }
            if (CNFInc && activeCNFIncCEll[0] < countRows) {
                g.setColor(Color.red);
                for (int j = 0; j < countColumns; j++) {
                    g.drawRect(widthCell * j, heightCell * activeCNFIncCEll[0], widthCell, heightCell);
                }
                g.setColor(Color.green);
                g.drawRect(widthCell * activeCNFIncCEll[1], heightCell * activeCNFIncCEll[0], widthCell, heightCell);
                g.setColor(Color.blue);
                g.drawRect(widthCell * 4, heightCell * activeCNFIncCEll[0], widthCell, heightCell);
            }
        }
    }
}

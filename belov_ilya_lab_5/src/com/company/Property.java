package com.company;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.lang.Integer;
import java.awt.Color;

public class Property {
    public int getPropertyInt(String propertyName) {
        int propertyValue = 0;
        Properties prop = new Properties();

        try (InputStream inputStream = this.getClass().getResourceAsStream("config.properties")) {
            prop.load(inputStream);
            propertyValue = Integer.valueOf(prop.getProperty(propertyName));
        } catch (IOException e) {
            System.out.print(e);
        }
        return propertyValue;
    }

    public String getPropertyString(String propertyName) {
        String propertyValue = "";
        Properties prop = new Properties();

        try (InputStream inputStream = this.getClass().getResourceAsStream("config.properties")) {
            prop.load(inputStream);
            propertyValue = prop.getProperty(propertyName);
        } catch (IOException e) {
            System.out.print(e);
        }
        return propertyValue;
    }

}

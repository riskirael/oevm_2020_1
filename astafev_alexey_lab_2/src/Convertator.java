public class Convertator
{
    private final int[] SETINT = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    private final char[] SETCHAR = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private int _originalNumberSystem;
    private int _newNumberSystem;
    private String _number;

    public Convertator(int originalNumberSystem, int newNumberSystem, String number) {
        _originalNumberSystem = originalNumberSystem;
        _newNumberSystem = newNumberSystem;
        _number = number;
    }

    public String convertToNewNumberSystem() {
        int deciminal = convertToDecimalNumber();
        int ranks = 0;

        while (deciminal >= _newNumberSystem) {
            deciminal = deciminal / _newNumberSystem;
            ranks++;
        }
        ranks++;

        char[] Result = new char[ranks];
        deciminal = convertToDecimalNumber();

        for (int i = ranks - 1; i > -1; i--) {
            for (int j = 0; j < _newNumberSystem; j++) {
                if (deciminal % _newNumberSystem == SETINT[j]) {
                    Result[i] = SETCHAR[j];
                    deciminal = deciminal / _newNumberSystem;
                    break;
                }
            }
        }
        return new String(Result);
    }

    private int convertToDecimalNumber() {
        int deciminal = 0;
        char[] charArr = _number.toCharArray();

        int i = 0, j;

        while(i < charArr.length) {
            int power = charArr.length - 1 - i;

            j = 0;

            while(j < _originalNumberSystem) {
                if (charArr[i] == SETCHAR[j]) {
                    deciminal = deciminal + (SETINT[j] * (int) Math.pow(_originalNumberSystem, power));
                    break;
                }
                j++;
            }
            i++;
        }
        return deciminal;
    }
}

public enum Operations {
    Addition,
    Subtraction,
    Multiplication,
    Division;

    public static Operations getOperations(String choice) {
        switch (choice) {
            case "+": {
                return Operations.Addition;
            }
            case "-": {
                return Operations.Subtraction;
            }
            case "*": {
                return Operations.Multiplication;
            }
            case "/": {
                return Operations.Division;
            }
        }
        return null;
    }
}


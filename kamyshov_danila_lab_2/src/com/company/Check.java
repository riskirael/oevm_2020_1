package com.company;

public class Check {

    public boolean checkValidNumber(String number, String start_system) {
        char[] checkArray = number.toCharArray();
        int charNumber = 48;
        int count = 0;
        for (char c : checkArray) {
            if (Character.isDigit(c)) {
                if (c - charNumber >= Integer.parseInt(start_system)) {
                    count++;
                }
            } else {
                int checkNumber = switch (c) {
                    case 'A' -> 10;
                    case 'B' -> 11;
                    case 'C' -> 12;
                    case 'D' -> 13;
                    case 'E' -> 14;
                    case 'F' -> 15;
                    default -> 0;
                };
                if (checkNumber >= Integer.parseInt(start_system)) {
                    count++;
                }
            }
        }
        return count == 0;
    }

    public boolean checkValidSystem(String startSystem, String finishSystem) {
        try {
            if (Integer.parseInt(startSystem) < 2 || Integer.parseInt(startSystem) > 16 || Integer.parseInt(finishSystem) < 2
                    || Integer.parseInt(finishSystem) > 16) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}

package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Translation trnsltn = new Translation();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите исходную систему счисления (2-16)");
        long originalNS = scanner.nextLong();
        System.out.println("Введите конечную систему счисления (2-16)");
        long finiteNS = scanner.nextLong();
        scanner.nextLine();
        System.out.println("Введите число в исходной системе счисления");
        String originalNum = scanner.nextLine();
        scanner.close();

        if (originalNS >= 2 && originalNS <= 16 && finiteNS >= 2 && finiteNS <= 16) {
            if (trnsltn.isNumOrgNS(originalNS, originalNum)) {
                System.out.println("Результат:");
                if (originalNum.equals("0"))
                    System.out.println("0");
                else
                    System.out.println(trnsltn.translation(originalNS, finiteNS, originalNum));
            } else {
                System.out.println("Данных цифр в исходной системе счисления не существует");
            }
        } else {
            System.out.println("Введеная система счисления не входит в промежуток (2-16)");
        }
    }
}

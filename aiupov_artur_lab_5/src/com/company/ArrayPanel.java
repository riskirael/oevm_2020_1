package com.company;
import javax.swing.*;
import java.awt.*;


public class ArrayPanel extends JPanel {

    private int arrayOfNumbers[][];
    Helper methods = new Helper();

    public ArrayPanel(int arrayOfNumbers[][]) {
        this.arrayOfNumbers = arrayOfNumbers;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Helper.draw(arrayOfNumbers, g);
    }
}

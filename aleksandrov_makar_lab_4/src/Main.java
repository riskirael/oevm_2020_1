import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите ДНФ или КНФ для осуществления соответсвующей опрации: ");
        String operation = in.next();

        int[][] array = DNFandKNF.generateArray();
        for (int[] item: array ) {
            System.out.printf("%d %d %d %d = %d%n", item[0], item[1], item[2], item[3], item[4]);
        }

        if (operation.equals("ДНФ")) {
            DNFandKNF.DNF(array);
        }
        else if (operation.equals("КНФ")){
            DNFandKNF.KNF(array);
        }
        else {
            System.out.println("Не могу осуществить подобное действие");
        }
    }
}
package com.example.program;
import java.util.Scanner;

public class Program
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.printf("Enter numeral system (from): ");
        int fromNS = in.nextInt();
        System.out.printf("Enter values: ");
        String value_1 = in.next();
        String value_2 = in.next();
        System.out.printf("Enter arithmetic operation: ");
        char operation = in.next().charAt(0);

        BinaryArithmetic arithmetic = new BinaryArithmetic(fromNS, value_1, value_2, operation);
        System.out.printf(arithmetic.binaryArithmetic());
    }
}

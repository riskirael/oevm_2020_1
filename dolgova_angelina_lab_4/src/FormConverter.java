public class FormConverter {
    private final int NUMBER_OF_ROWS;
    private final int NUMBER_OF_COLUMNS;
    public final String[] VARIABLES = {"X_1", "X_2", "X_3", "X_4" };

    public FormConverter(int countOfRows, int countOfColumns){
        this.NUMBER_OF_ROWS = countOfRows;
        this.NUMBER_OF_COLUMNS = countOfColumns;
    }

    public void bringToDNF(int[][] truthTable) {
        boolean sumSign = false;
        for (int i = 0; i < NUMBER_OF_ROWS; ++i) {
            if (truthTable[i][NUMBER_OF_COLUMNS - 1] == 1) {
                if (sumSign) {
                    System.out.print(" + ");
                }
                System.out.print("(");
                sumSign = true;
                for (int j = 0; j < NUMBER_OF_COLUMNS - 1; ++j) {
                    if (truthTable[i][j] == 0) {
                        System.out.print("!");
                    }
                    System.out.print(VARIABLES[j]);
                    if (j < NUMBER_OF_COLUMNS - 2) {
                        System.out.print("*");
                    }
                    else {
                        System.out.print(")");
                    }
                }
            }
        }
    }

    public void bringToCNF(int[][] truthTable) {
        boolean multiplicationSign = false;
        for (int i = 0; i < NUMBER_OF_ROWS; i++) {
            if (truthTable[i][NUMBER_OF_COLUMNS - 1] == 0) {
                if (multiplicationSign) {
                    System.out.print("*");
                }
                System.out.print("(");
                multiplicationSign = true;
                for (int j = 0; j < NUMBER_OF_COLUMNS - 1; j++) {
                    if (truthTable[i][j] == 1) {
                        System.out.print("!");
                    }
                    System.out.print(VARIABLES[j]);
                    if (j < NUMBER_OF_COLUMNS - 2) {
                        System.out.print(" + ");
                    }
                    else {
                        System.out.print(")");
                    }
                }
            }
        }
    }
}
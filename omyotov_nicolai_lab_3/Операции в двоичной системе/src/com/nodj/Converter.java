package com.nodj;

public class Converter {
    private int baseIn;
    private int baseOut;
    private String numIn;

    public void setBaseIn(int baseIn) {
        this.baseIn = baseIn;
    }

    public void setBaseOut(int baseOut) {
        this.baseOut = baseOut;
    }

    public void setNumIn(String numIn) {
        this.numIn = numIn;
    }

    public String convert() {
        int num = 0;
        StringBuilder numOut = new StringBuilder();
        char[] numInCharArray = numIn.toCharArray();
        String nums = "0123456789ABCDEF";
        for (int i = 0; i < numIn.length(); i++) {
            num += nums.indexOf(numInCharArray[i]) * Math.pow(baseIn, numIn.length() - i - 1);
        }
        if (num == 0) {
            return "0";
        } else {
            while (num != 0) {
                numOut.append(nums.charAt(num % baseOut));
                num /= baseOut;
            }
            return numOut.reverse().toString();
        }
    }
}

   format PE console

entry start

include 'win32a.inc'

section '.info' data readable writable

        enterA db 'A = ', 0
        enterB db 'B = ', 0
        spaceStr db ' %d', 0

        addStr db 'A + B = %d', 0dh, 0ah, 0
        subStr db 'A - B = %d', 0dh, 0ah, 0
        mulStr db 'A * B = %d', 0dh, 0ah, 0
        divStr db 'A / B = %d', 0

        A dd ?
        B dd ?

        NULL = 0

section '.program' code readable executable

        start:

                push enterA
                call [printf]

                push A
                push spaceStr
                call [scanf]

                push enterB
                call [printf]

                push B
                push spaceStr
                call [scanf]

                ;
                    mov ecx, [A]
                    add ecx, [B]

                    push ecx
                    push addStr
                    call [printf]

                ;
                    mov ecx, [A]
                    sub ecx, [B]

                    push ecx
                    push subStr
                    call [printf]

                ;
                    mov ecx, [A]
                    imul ecx, [B]

                    push ecx
                    push mulStr
                    call [printf]

                ;
                    mov eax, [A]
                    cdq
                    mov ecx, [B]
                    idiv ecx

                    push eax
                    push divStr
                    call [printf]

                call [getch];      

                push NULL
                call [ExitProcess]

section '.data' import data readable

        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvcrt,\
               printf, 'printf',\
               scanf, 'scanf',\
               getch, '_getch'

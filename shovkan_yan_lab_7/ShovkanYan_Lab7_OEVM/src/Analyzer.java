import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Analyzer {
    Writer writer;

    private LinkedList<String> varStr = new LinkedList<>();
    private LinkedList<String> codeStr = new LinkedList<>();
    private LinkedList<String> vars = new LinkedList<>();

    private String string;
    private String blockOfVars;
    private String blockOfCods;

    public Analyzer(String string, Writer writer) {
        this.string = string;
        this.writer = writer;
        parseParthBody();
        parseVarsStrings();
        parseVarsInteger();
        parseCodsStrings();
        writer.setVariables(vars);
        createCode();
    }

    //анализмрует строки программы используя паттерны
    public void parseParthBody() {
        Pattern patternForBlocOfVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherForBlocOfVariables = patternForBlocOfVariables.matcher(string);

        while (matcherForBlocOfVariables.find()) {
            blockOfVars = string.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end());
        }

        Pattern patternForBlocOfCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherForBlocOfCode = patternForBlocOfCode.matcher(string);

        while (matcherForBlocOfCode.find()) {
            blockOfCods = string.substring(matcherForBlocOfCode.start(), matcherForBlocOfCode.end());
        }

    }

    //анализмрует строки с str
    private void parseVarsStrings() {
        Pattern patternForBlocOfVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherForBlocOfVariables = patternForBlocOfVariables.matcher(blockOfVars);

        while (matcherForBlocOfVariables.find()) {
            varStr.add(blockOfVars.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end()));
        }
    }

    //отделяет инты
    private void parseVarsInteger() {
        Pattern patternForBlocOfVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherForBlocOfVariables;
        String var;

        for (String string : varStr) {

            matcherForBlocOfVariables = patternForBlocOfVariables.matcher(string);

            while (matcherForBlocOfVariables.find()) {
                var = string.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end());
                if (!var.equals("integer")) {
                    vars.add(var);
                }
            }
        }
        writer.setVariables(vars);
    }

    //отделяет все функции и операции в список
    private void parseCodsStrings() {

        Pattern patternForStringsOfCods = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherForStringsOfCods = patternForStringsOfCods.matcher(blockOfCods);

        while (matcherForStringsOfCods.find()) {
            codeStr.add(blockOfCods.substring(matcherForStringsOfCods.start(), matcherForStringsOfCods.end()));
        }
        createCode();
    }

    //создает код
    private void createCode() {

        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternReadLine = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternNumericOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");
        Pattern patternWriteLine = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        for (String string : codeStr) {

            if (string.matches(patternWrite.toString())) {
                Matcher matcherForCode = patternWrite.matcher(string);

                if (matcherForCode.find()) {
                    writer.addWrite(matcherForCode.group(1));
                    continue;
                }
            } else if (string.matches(patternReadLine.toString())) {
                Matcher matcherForCode = patternReadLine.matcher(string);

                if (matcherForCode.find()) {
                    writer.addReadLine(matcherForCode.group(1));
                    continue;
                }
            } else if (string.matches(patternWriteLine.toString())) {
                Matcher matcherForCode = patternWriteLine.matcher(string);

                if (matcherForCode.find()) {
                    writer.addWriteLine(matcherForCode.group(1));
                    continue;
                }
            } else {
                Matcher matcherForCode = patternNumericOperation.matcher(string);

                if (matcherForCode.find()) {
                    writer.addNumericOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                    continue;
                }
            }
        }
    }
}

package com.company;

import javax.swing.*;
import javax.swing.border.BevelBorder;


public class Form {
    private int COLUMN = 5;
    private int ROW = 16;
    int[][] arr = new int[ROW][COLUMN];
    public JFrame frame;
    private final OperationClass operationClass = new OperationClass(arr);
    private final BooleanPanel panel = new BooleanPanel(operationClass);


    /**
     * Launch the application.
     */
    Form() {
        initialize();
    }

    private void initialize() {

        int width = 200;
        int height = 450;
        frame = new JFrame();
        frame.setBounds(200, 300, 700, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        panel.setBorder(new BevelBorder(BevelBorder.LOWERED,
                null, null, null, null));
        panel.setBounds(10, 11, width, height);
        frame.getContentPane().add(panel);

        JTextArea functionTextArea = new JTextArea();
        functionTextArea.setBounds(400, 10, 200, 300);
        frame.getContentPane().add(functionTextArea);

        JButton generate = new JButton("Generate");
        generate.addActionListener(e -> {
            operationClass.filling();
            functionTextArea.setText(operationClass.output());
            panel.repaint();
        });
        generate.setBounds(250, 10, 100, 50);
        frame.getContentPane().add(generate);


        JButton showKNFButton = new JButton("KNF");
        showKNFButton.setBounds(250, 70, 100, 50);
        frame.getContentPane().add(showKNFButton);
        showKNFButton.addActionListener(e -> {
            if(operationClass.getStart()) {
                return;
            }
               operationClass.runForm(true);
               panel.repaint();
        });

        JButton showDNFButton = new JButton("DNF");
        showDNFButton.setBounds(250, 130, 100, 50);
        frame.getContentPane().add(showDNFButton);
        showDNFButton.addActionListener(e -> {
            if(operationClass.getStart()) {
                return;
            }
            operationClass.runForm(false);
            panel.repaint();
        });
    }
}

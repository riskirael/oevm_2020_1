import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args){
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {

            System.out.print("Введите исходную систему счисления: ");
            int originalSystemNumber = Integer.parseInt(in.readLine());

            System.out.print("Введите конечную систему счисления: ");
            int newSystemNumber = Integer.parseInt(in.readLine());

            System.out.print("Введите число, которое хотите перевести: ");
            String number = in.readLine();

            Convertator convertator = new Convertator(originalSystemNumber, newSystemNumber, number);
            String result = convertator.convertToNewNumberSystem();
            System.out.print("Результат: " + result);
        }
        catch (Exception e){
            System.out.println("Введите корректные данные");
        }
    }
}

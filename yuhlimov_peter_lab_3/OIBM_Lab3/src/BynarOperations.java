
public class BynarOperations {
	// Sum of two binary numbers
	public static String sum(int[] numOne, int[] numTwo) {
		int[] result = new int[20];
		int safe = 0;
		for (int i = 0; i < numOne.length; i++) {
			int newNumb;
			if (numOne[i] + numTwo[i] + safe >= 2) {
				newNumb = numOne[i] + numTwo[i] + safe - 2;
				safe = 1;
			} else {
				newNumb = numOne[i] + numTwo[i] + safe;
				safe = 0;
			}
			result[i] = newNumb;
		}
		return Helper.arrNumbToString(result);
	}
	// Also calculating the sum of two two-digit numbers, but returns an array
	public static int[] sum(int[] numOne, int[] numTwo, int rang) {
		int[] result = new int[20];
		// long result = 0;
		int safe = 0;
		for (int i = 0; i < numOne.length; i++) {
			int newNumb;
			if (i >= rang) {
				if (numOne[i] + numTwo[i - rang] + safe >= 2) {
					newNumb = numOne[i] + numTwo[i - rang] + safe - 2;
					safe = 1;
				} else {
					newNumb = numOne[i] + numTwo[i - rang] + safe;
					safe = 0;
				}
			} else {
				newNumb = numOne[i];
			}
			result[i] = newNumb;
		}
		return result;
	}

	// Subtracting two binary numbers
	public static String sub(int[] numOne, int[] numTwo) {
		int[] result = new int[20];
		if (Helper.arrNumbToString(numOne).length() < Helper.arrNumbToString(numTwo).length()) {
			int[] safe = numOne;
			numOne = numTwo;
			numTwo = safe;
		}
		for (int i = 0; i < numOne.length; i++) {
			if (numOne[i] < numTwo[i]) {
				for (int j = i + 1; j < numOne.length; j++) {
					if (numOne[j] > 0) {
						numOne[j]--;
						break;
					} else {
						numOne[j] += 1;
					}
				}
				numOne[i] += 2;
			}
			result[i] = numOne[i] - numTwo[i];
		}

		return Helper.arrNumbToString(result);
	}
	// Subtracting two binary numbers 2, returns an array
	public static int[] sub(int[] numOne, int[] numTwo, boolean retArray) {
		int[] result = new int[20];
		if (Helper.arrNumbToString(numOne).length() < Helper.arrNumbToString(numTwo).length()) {
			int[] safe = numOne;
			numOne = numTwo;
			numTwo = safe;
		}
		for (int i = 0; i < numOne.length; i++) {
			if (numOne[i] < numTwo[i]) {
				for (int j = i + 1; j < numOne.length; j++) {
					if (numOne[j] > 0) {
						numOne[j]--;
						break;
					} else {
						numOne[j] += 1;
					}
				}
				numOne[i] += 2;
			}
			result[i] = numOne[i] - numTwo[i];
		}
		return result;
	}

	// Calculating the product of two binary numbers
	public static String multiply(int[] numOne, int[] numTwo) {
		int[] result = new int[20];
		int rang = 0;
		for (int i = 0; i < numTwo.length; i++) {
			int[] mid_res = new int[20];
			for (int j = 0; j < numOne.length; j++) {
				mid_res[j] = numOne[j] * numTwo[i];
			}
			result = sum(result, mid_res, rang);
			rang++;
		}
		return Helper.arrNumbToString(result);
	}

	// Calculating the division of two binary numbers
	public static String div(int[] numOne, int[] numTwo) {
		int result = 0;
		while (true) {
			if (!Helper.checkNumbers(numOne, numTwo)) {
				break;
			}
			numOne = sub(numOne, numTwo,true);
			result++;
		}
		return Helper.arrNumbToString(Helper.toTwo(result));
	}
}

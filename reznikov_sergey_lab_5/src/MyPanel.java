import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {

    BooleanTable table;
    public int YSize;
    public int XSize;

    public MyPanel(BooleanTable tmp){
        table = tmp;
        YSize=table.checkYSize();
        XSize=table.checkXSize();
    }

    public int getYSize(){
        return YSize;
    }

    public int getXSize(){
        return XSize;
    }

    @Override
    public void paint(Graphics g){
        super.paint(g);
        table.Draw(g);
    }
}

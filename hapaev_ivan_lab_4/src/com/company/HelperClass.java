package com.company;

class HelperClass{
    static final int N = 16;
    static final int M = 4;
    static final String[] VAR = {"X1", "X2", "X3", "X4"};

    public static String convert(int number) {
        String result = "";
        int remains;
        int bin = 2;

        for (String сurrentDigit = ""; number > 0; number =  number / bin) {
            remains = number % bin;
            сurrentDigit = "";
            if (number % bin < 10) {
                сurrentDigit = Integer.toString(remains);
            } else {
                сurrentDigit = сurrentDigit + (char)((int)('A' + remains - 10));
            }
            result = сurrentDigit + result;
        }
        while (result.length() < M) {
            result = '0' + result;
        }
        return result;
    }

    public static int[][] outputAndFillTable(int[][] truthTable) {
        for (int i = 0; i < N; ++i) {
            String strX = convert(i);
            for (int j = 0; j < M; ++j) {
                truthTable[i][j] = strX.charAt(j) - '0';
                System.out.print(truthTable[i][j] + " ");
            }
            truthTable[i][M] = (int)(Math.random() * 2);
            System.out.println("= " + truthTable[i][M]);
        }
        return truthTable;
    }

    public static void disjunctiveNormalForm(int[][] truthTable){
        boolean notFirstIter = false;
        for (int i = 0; i < N; i++){
            if (truthTable[i][M] == 1){
                if (notFirstIter){
                    System.out.print(" + ");
                }
                System.out.print("(");
                notFirstIter = true;

                for (int j = 0; j < M; j++){
                    if (truthTable[i][j] == 0){
                        System.out.print("-");
                    }
                    System.out.print(VAR[j]);
                    if (j < M - 1){
                        System.out.print(" * ");
                    } else {
                        System.out.print(")");
                    }
                }
            }
        }
    }

    public static void conjunctiveNormalForm(int[][] truthTable) {
        boolean notFirstIter = false;

        for (int i = 0; i < N; ++i) {
            if (truthTable[i][M] == 0) {
                if (notFirstIter) {
                    System.out.print(" * ");
                }
                System.out.print("(");
                notFirstIter = true;

                for (int j = 0; j < M; ++j) {
                    if (truthTable[i][j] == 1) {
                        System.out.print("-");
                    }
                    System.out.print(VAR[j]);
                    if (j < M - 1) {
                        System.out.print(" + ");
                    } else {
                        System.out.print(")");
                    }
                }
            }
        }
    }
}
package sample;
import java.util.Random;

/**
 * функция создания таблицы истинности
 */
public class TruthTable {
    private int strings = 16;
    private int columns = 5;
    private Random random = new Random();
    public int[][] truthTable = {
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)},
    };
    /**
     * функция вывода таблицы истинности
     */
    public void printTruthTable() {
        for (int i = 0; i < strings; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(truthTable[i][j] + "  ");
            }
            System.out.println();
        }
    }
    public int getCountOfStrings() {

        return strings;
    }

    public int getCountOfColumns() {

        return columns;
    }
}

<h3>Аппанов Владислав ПИбд-21 Лаб 8 </h3>
Комплектующие ПК:</h5>

| № | Название комлектующих | Причина данного выбора
| :---: |:---------------:| :---:|
| 1 | Процессор Процессор AMD Ryzen 5 3600 | 6 ядер и базовая частота 3600 МГц
| 2 | Материнская плата GIGABYTE GA-A320M-S2H | 2 слота для оперативной памяти и совместимый сокет 
| 3 | Видеокарта MSI nVidia GeForce GTX 1650 SUPER WINDFORCE OC | 4гб видеопамяти и энергопотребление 100Вт
| 4 | Корпус ATX ZALMAN | отверстия на выдув расположены впереди и разем для usb 3.0
| 5 | Кулер для процессора DEEPCOOL GAMMAXX 300 FURY | крутится со скоростью 1800 оборотов в минуту
| 6 | Оперативная память PATRIOT Signature PSD48G266681 DDR4 | комплект из 2 плашек по 8 гб, для работы на Java будет достаточно
| 7 | SSD накопитель A-DATA XPG SX6000 Lite ASX6000LNP-128GT-C 128ГБ | для быстрой работы
| 8 | Блок питания THERMALTAKE LT-550p | 550 ват, с запасом
| 9 | Жесткий диск WD Caviar Blue WD10EZEX 1Тб | для больших программ

Компуктер до сборки: https://drive.google.com/file/d/1zBcf7-yNxeXFc4gzJiWPBLQwM1B2OOfg/view?usp=sharing
Компуктер после сборки: https://drive.google.com/file/d/117GUDDCeKXA0BjGgSjdS1D7ICwdiLqyn/view?usp=sharing
https://drive.google.com/file/d/17RcRijveQOL6ffSoLb2BmtLp6oHulioY/view?usp=sharing
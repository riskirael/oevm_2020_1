package com.company;

public class Converter {

    /**
     * Этот метод вернет число в десятичной системе счисления
     * @param startSystem Исходная система счисления
     * @param number Число в исходной ситеме счисления
     */
    public static long convertToDecimal(int startSystem, char[] number) {
        long numberDecimal = 0;

        for(int i = 0; i < number.length; ++i) {
            int currentDigit = 1;
            if (number[i] >= '0' && number[i] <= '9') { currentDigit = number[i] - '0'; }
            else if (Character.isLetter(number[i])) { currentDigit = 10 + number[i] - 'A'; }
            numberDecimal = (long) ((double) numberDecimal + (double) currentDigit * Math.pow(startSystem, number.length - i - 1));
        }
        return numberDecimal;
    }

    /**
     * Этот метод вернет число в двоичной системе счисления
     * @param numberDecimal Число в десятичной системае счисления
     */
    public static String convertToBinary (long numberDecimal) {
        int finalSystem = 2;
        StringBuilder result = new StringBuilder();
        long remainder;

        for (String currentChar; numberDecimal > 0; numberDecimal /= finalSystem) {
            remainder = numberDecimal % (long) finalSystem;
            currentChar = Long.toString(remainder);
            result.insert(0, currentChar);
        }
        return result.toString();
    }
}

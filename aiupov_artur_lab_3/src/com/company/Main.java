package com.company;
import java.awt.*;
import java.util.Scanner;
import java.lang.StringBuffer;
import java.lang.Integer;

public class Main {
    public static void main(String[] args)  throws java.io.IOException{
        // write your code here
        System.out.println("Input a number of initial notation: "); // обычный вывод на консоль, ничего необычного
        Scanner original = new Scanner(System.in); // сканер сканирует (считывает)
        int initialNotation = original.nextInt(); // вводим изначальное основание

        System.out.println("Enter the first number: "); // обычный вывод на консоль, ничего необычного
        Scanner number = new Scanner(System.in); // сканер сканирует (считывает)
        String firstNumber = number.nextLine(); // ввожу первое число
        String firstResult = new StringBuffer(firstNumber).reverse().toString(); // строка, в которой запишу перевёрнутую изначальную строку

        System.out.println("Enter the second number: ");
        Scanner secNumber = new Scanner(System.in);
        String secondNumber = secNumber.nextLine(); // ввожу второе число
        String secondRusult = new StringBuffer(secondNumber).reverse().toString(); // переворачиваю второе число

        System.out.println("Enter the operation: "); // ввожу операцию
        char operation = (char) System.in.read();

        long firstFinilNumber = 0; // число для перевода в двоичную
        long secondFinilNumber = 0; // число для перевода в двоичную
        String notFinalResultOne = ""; // число, переведённое в двоичную систему
        String notFinalResultTwo = ""; // число, переведённое в двоичную систему
        String composition = notFinalResultOne; // произведение
        String divide = notFinalResultOne; // деление
        String division = ""; // результат деления нацело
        long cnt = 0; // счётчик для деления

        firstFinilNumber = Helper.numberToTen(firstResult, firstFinilNumber, initialNotation);// первое число в 10-ной системе
        secondFinilNumber = Helper.numberToTen(secondRusult, secondFinilNumber, initialNotation);// второе число в 10-ной

        notFinalResultOne = Helper.convert(firstFinilNumber, notFinalResultOne); // первое число в двоичной системе
        String s1 = new StringBuffer(notFinalResultOne).reverse().toString();;
        System.out.println("First number: "+s1);

        notFinalResultTwo = Helper.convert(secondFinilNumber, notFinalResultTwo); // второе число в двоичной системе
        String s2 = new StringBuffer(notFinalResultTwo).reverse().toString();;
        System.out.println("Second number: "+s2);

        Integer firstN = Integer.valueOf(s1); // 1 число для проверки
        Integer secondN = Integer.valueOf(s2); // 2 число для проверки


        if (operation == '+') {
            String finalSum = Helper.addition(notFinalResultOne, notFinalResultTwo);
            System.out.println("sum = "+finalSum);
        }
        else if (operation == '-') {
            String finalDifference = Helper.subtraction(notFinalResultOne, notFinalResultTwo, firstN, secondN);
            System.out.println("difference = "+finalDifference);
        }

        else if(operation == '*') {
            for (long j = 0; j < secondFinilNumber; j++) {
              composition = Helper.addition(composition, notFinalResultOne);
              composition = new StringBuffer(composition).reverse().toString();
            }
            composition = new StringBuffer(composition).reverse().toString();
            System.out.println("composition = "+composition);
        }

        else if(operation == '/') {
            for (long j = firstFinilNumber; j >= secondFinilNumber; j -= secondFinilNumber) {
                divide = Helper.subtraction(divide, notFinalResultOne, firstN, secondN);
                divide = new StringBuffer(divide).reverse().toString();
                cnt++;
            }
            division = Helper.convert(cnt, division);
            division = new StringBuffer(division).reverse().toString();
            System.out.println("divide = "+division);
        }
    }
}

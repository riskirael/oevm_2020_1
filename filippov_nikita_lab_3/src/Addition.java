import java.util.ArrayList;

import static java.lang.Math.max;

public class Addition implements IArithmetic {
    private ArrayList<Integer> result;

    /**
     * Метод сложения 2х чисел в 2с.с, передающихся в ArrayList
     */
    @Override
    public ArrayList<Integer> startArithmeticOperation(ArrayList<Integer> a, ArrayList<Integer> b) {
        this.result = new ArrayList<>();
        int tmp = 0;
        for (int i = 0; i < max(a.size(), b.size()); i++) {
            if (i >= a.size() || i >= b.size()) {
                if (i >= a.size()) {
                    tmp += b.get(i);
                } else {
                    tmp += a.get(i);
                }
            } else {
                tmp += a.get(i) + b.get(i);
            }
            switch (tmp) {
                case 0:
                case 1: {
                    this.result.add(tmp);
                    tmp = 0;
                    break;
                }
                case 2: {
                    tmp -= 2;
                    this.result.add(tmp);
                    tmp++;
                    break;
                }
                case 3: {
                    tmp -= 2;
                    this.result.add(tmp);
                    break;
                }
            }
        }
        if (tmp == 1) {
            this.result.add(1);
        }
        return this.result;
    }
}
import java.util.Random;

public class Methods {
    static int sizeI = 16;
    static int sizeJ = 5;

    public void printArray(int[][] array){
        for(int i = 0;i<sizeI;i++){
            for(int j = 0;j<sizeJ;j++){
                System.out.print(array[i][j]+" ");
            }
            System.out.print("\n");
        }
    }

    public String DNF(int[][] array) {
        int X4 = 3;
        int F = 4;
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < sizeI; i++) {
            if (array[i][F] == 1) {//если значение функции 1
                strBuilder.append("( ");//открываем (
                for (int j = 0; j < X4; j++) {
                    if (array[i][j] == 1) { //если значение Xj 1
                        strBuilder.append("X").append(j + 1).append(" * ");//пишем "Xj * "
                    } else { //если значение Xj 0
                        strBuilder.append("-X").append(j + 1).append(" * ");//пишем "-Xj * "
                    }
                }
                if (array[i][X4] == 1) {//если значение X4 1
                    strBuilder.append("X4");//пишем "X4"
                } else {//если значение X4 0
                    strBuilder.append("-X4");//пишем "-X4"
                }
                strBuilder.append(" ) + ");//закрываем ), ставим знак +
            }
        }
        strBuilder.delete(strBuilder.toString().length() - 3, strBuilder.toString().length() - 1);// убираем последний знак *
        return strBuilder.toString();
    }

    public String KNF(int[][] array) {
        int X4 = 3;
        int F = 4;
        StringBuilder strBuilder = new StringBuilder();

        for (int i = 0; i < sizeI; i++)
            if (array[i][F] == 0) { //если значение функции 0
                strBuilder.append("( "); //открываем (
                for (int j = 0; j < X4; j++) {
                    if (array[i][j] == 0) { //если значение Xj 0
                        strBuilder.append("X").append(j + 1).append(" + "); //пишем "Xj + "
                    } else { //если значение 1
                        strBuilder.append("-X").append(j + 1).append(" + "); //пишем "-Xj + "
                    }
                }
                if (array[i][X4] == 0) { //если значение X4 0
                    strBuilder.append("X4"); //пишем "X4"
                } else { //если значение X4 1
                    strBuilder.append("-X4");//пишем "-X4"
                }
                strBuilder.append(" ) * ");//закрываем ), ставим знак *
            }

        strBuilder.delete(strBuilder.toString().length() - 3, strBuilder.toString().length() - 1); // убираем последний знак *
        return strBuilder.toString();
    }

}

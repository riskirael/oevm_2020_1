package EVM_Lab_1;

import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        byte notOld = 0;
        byte notNew = 0;
        String num = "";
        notOld = Byte.parseByte(Input.getCorrectNotation(true));
        notNew = Byte.parseByte(Input.getCorrectNotation(false));
        num = Input.getCorrectNum(notOld);
        if (num.equals("0") || num.equals("1")) {
            System.out.println("Результат: " + num);
        } else {
            System.out.println("Результат: " + Converter.convertToNewNotation(notNew, Converter.convertToDec(notOld, num)));
        }
    }
}

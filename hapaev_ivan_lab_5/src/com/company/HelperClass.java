package com.company;
import java.awt.*;
import java.io.FileInputStream;
import java.util.Properties;

public class HelperClass {

    static int N = new Variables().getPropertyValueInt("N");
    static int M = new Variables().getPropertyValueInt("M");
    static int X = new Variables().getPropertyValueInt("X");
    static int Y = new Variables().getPropertyValueInt("Y");
    static int width = new Variables().getPropertyValueInt("width");
    static int height = new Variables().getPropertyValueInt("height");



    public static String convert(int number) {
        String result = "";
        int remains;
        int bin = 2;

        for (String сurrentDigit = ""; number > 0; number =  number / bin) {
            remains = number % bin;
            сurrentDigit = "";
            if (number % bin < 10) {
                сurrentDigit = Integer.toString(remains);
            } else {
                сurrentDigit = сurrentDigit + (char)((int)('A' + remains - 10));
            }
            result = сurrentDigit + result;
        }
        while (result.length() < M) {
            result = '0' + result;
        }
        return result;
    }

    public static int[][] outputAndFillTable(int[][] truthTable) {
        for (int i = 0; i < N; ++i) {
            String strX = convert(i);
            for (int j = 0; j < M; ++j) {
                truthTable[i][j] = strX.charAt(j) - '0';
            }
            truthTable[i][M] = (int)(Math.random() * 2);
        }
        return truthTable;
    }

    public static String convertToDisjunctiveNormalForm(int[][] truthTable){
        String[] VAR = {"X1", "X2", "X3", "X4"};
        boolean notFirstIter = false;
        StringBuilder strBuilder = new StringBuilder();

        for (int i = 0; i < N; i++){
            if (truthTable[i][M] == 1){
                if (notFirstIter){
                    strBuilder.append(" + ");
                }
                strBuilder.append("(");
                notFirstIter = true;

                for (int j = 0; j < M; j++){
                    if (truthTable[i][j] == 0){
                        strBuilder.append("-");
                    }
                    strBuilder.append(VAR[j]);
                    if (j < M - 1){
                        strBuilder.append(" * ");
                    } else {
                        strBuilder.append(") +");
                    }
                }
                strBuilder.append("\n");
            }
        }
        strBuilder.delete(strBuilder.toString().length() - 2, strBuilder.toString().length() - 1).toString();
        return strBuilder.toString();
    }

    public static String convertToConjunctiveNormalForm(int[][] truthTable) {
        String[] VAR = {"X1", "X2", "X3", "X4"};
        boolean notFirstIter = false;
        StringBuilder strBuilder = new StringBuilder();


        for (int i = 0; i < N; ++i) {
            if (truthTable[i][M] == 0) {
                if (notFirstIter) {
                    strBuilder.append(" * ");
                }
                strBuilder.append("(");
                notFirstIter = true;

                for (int j = 0; j < M; ++j) {
                    if (truthTable[i][j] == 1) {
                        strBuilder.append("-");
                    }
                    strBuilder.append(VAR[j]);
                    if (j < M - 1) {
                        strBuilder.append(" + ");
                    } else {
                        strBuilder.append(") *");
                    }
                }
                strBuilder.append("\n");
            }
        }
        strBuilder.delete(strBuilder.toString().length() - 2, strBuilder.toString().length() - 1).toString();
        return strBuilder.toString();
    }

    public static void draw(int truthTable[][], Graphics g) {
        for (int i = 0; i < M; i++) {
            g.drawRect(X + width * i, Y, width, height);
            g.drawString("X" + (i + 1), X * 2 + width * i, Y * 3);
        }

        g.drawRect(X + width * M, Y, width, height);
        g.drawString("Y", X + 10 + width * (M), Y + 20);

        Y += 30;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j <= M; j++) {
                g.drawRect(X + width * j, Y + height * i, width, height);
                g.drawString(truthTable[i][j] + "", X + 10 + width * j, Y + 20 + height * i);
            }
        }
        recolor(truthTable, g);

        Y -= 30;
    }

    public static void recolor(int truthTable[][], Graphics g) {
        if (Main.knf) {
            for (int i = 0; i < N; i++) {
                if (truthTable[i][M] == 0) {
                    g.setColor(Color.magenta);
                    for (int j = 0; j <= M; j++) {
                        g.drawRect(X + width * j, Y + height * i, width, height);
                        g.drawString(truthTable[i][j] + "", X + 10 + width * j, Y + 20 + height * i);
                    }
                } else {
                    g.setColor(Color.lightGray);
                    for (int j = 0; j <= M; j++) {
                        g.fillRect(X + width * j, Y + height * i, width, height);
                        g.drawString(truthTable[i][j] + "", X + 10 + width * j, Y + 20 + height * i);
                    }
                }
            }
        }
        else if (Main.dnf) {
            for (int i = 0; i < N; i++) {
                if (truthTable[i][M] == 1) {
                    g.setColor(Color.magenta);
                    for (int j = 0; j <= M; j++) {
                        g.drawRect(X + width * j, Y + height * i, width, height);
                        g.drawString(truthTable[i][j] + "", X + 10 + width * j, Y + 20 + height * i);
                    }
                } else {
                    g.setColor(Color.lightGray);
                    for (int j = 0; j <= M; j++) {
                        g.fillRect(X + width * j, Y + height * i, width, height);
                        g.drawString(truthTable[i][j] + "", X + 10 + width * j, Y + 20 + height * i);
                    }
                }

            }
        }
    }
}
package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ASMFileWriter {

    //Список с целочисленными перееменными
    ArrayList<String> arrayOfVariables = new ArrayList<>();

    //Хэшмапа со строками
    HashMap<String, String> arrayOfStringVariables = new HashMap<>();

    //Начальные данные любой программы
    String starting = ";This is laboratory work number 7\n" +
            ";implemented by Prytkin Timofey\n" +
            "\n" +
            "format PE console\n" +
            "\n" +
            "entry start\n" +
            "\n" +
            "include 'win32a.inc'\n" +
            "\n" +
            "section '.data' data readable writable\n";

    //Переменные программы
    String variables = "        spaceStr db '%d', 0\n" +
            "        dopStr db '%d', 0ah, 0\n";

    //Код программы
    String code = "section '.code' code readable executable\n" +
            "\n" +
            "         start:\n";

    //Конец программы
    String ending = "         finish:\n" +
            "\n" +
            "                call [getch]\n" +
            "\n" +
            "                call [ExitProcess]\n" +
            "\n" +
            "section '.idata' import data readable\n" +
            "\n" +
            "        library kernel, 'kernel32.dll',\\\n" +
            "                msvcrt, 'msvcrt.dll'\n" +
            "\n" +
            "        import kernel,\\\n" +
            "               ExitProcess, 'ExitProcess'\n" +
            "\n" +
            "        import msvcrt,\\\n" +
            "               printf, 'printf',\\\n" +
            "               scanf, 'scanf',\\\n" +
            "               getch, '_getch'";


    /**
     * Метод записи в файл .ASM
     */
    public void write(){

        addAllVariables();

        try(FileWriter writer = new FileWriter("prytkin_timofey_lab_7.ASM", false))
        {
            writer.write(starting);
            writer.write(variables);
            writer.write(code);
            writer.write(ending);

            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }


    }

    /**
     * Метод инициализации списка целочисленных переменных
     * @param arrayList Список целочисленных переменных
     */
    public void addToVariables(ArrayList<String> arrayList) {
        arrayOfVariables = arrayList;
    }

    /**
     * Метод добавления всех переменных в блок переменных
     */
    public void addAllVariables() {
        for (String string : arrayOfVariables) {
            variables += "        " + string + " dd ?\n";
        }

        for (String key: arrayOfStringVariables.keySet()) {
            variables += "        " + key + " db '" + arrayOfStringVariables.get(key) + "', 0\n";
        }
    }

    /**
     * Метод добавления кода write('...');
     * @param string содержимое ...
     */
    public void addToCodeWrite(String string) {
        arrayOfStringVariables.put("str" + (arrayOfStringVariables.size() + 1), string);

        code += "                push " + "str" + arrayOfStringVariables.size() + "\n" +
                "                call [printf]\n";
    }

    /**
     * Метод добавления кода readln('...');
     * @param string содержимое ...
     */
    public void addToCodeReadLn(String string) {
        if(arrayOfVariables.contains(string)){
            code += "                push " + string + "\n" +
                    "                push spaceStr\n" +
                    "                call [scanf]\n\n";
        }
    }

    /**
     * Метод добавления кода res := x1 =-/* x2;
     * @param res Результат операции
     * @param operator Оператор + - / *
     * @param firstNum Первое число
     * @param secondNum Второе число
     */
    public void addToCodeOperation(String res, String firstNum, String operator, String secondNum) {

        if (arrayOfVariables.contains(res) && arrayOfVariables.contains(firstNum) && arrayOfVariables.contains(secondNum)) {


            switch (operator) {
                case "+":
                    code += "                mov ecx, [" +  firstNum + "]\n" +
                            "                add ecx, [" +  secondNum + "]\n" +
                            "                mov [" + res + "], ecx\n";
                    break;
                case "-":
                    code += "                mov ecx, [" +  firstNum + "]\n" +
                            "                sub ecx, [" +  secondNum + "]\n" +
                            "                mov [" + res + "], ecx\n";
                    break;
                case "*":
                    code += "                mov ecx, [" +  firstNum + "]\n" +
                            "                imul ecx, [" +  secondNum + "]\n" +
                            "                mov [" + res + "], ecx\n";
                    break;
                case "/":
                    code += "                mov eax, [" +  firstNum + "]\n" +
                            "                mov ecx, [" +  secondNum + "]\n" +
                            "                div ecx\n" +
                            "                mov [" + res + "], eax\n";
                    break;
            }
        }
    }

    /**
     * Метод добавления кода writeln('...');
     * @param string содержимое ...
     */
    public void addToCodeWriteLn(String string) {
        if (arrayOfVariables.contains(string)){

            code += "                push [" + string + "]\n" +
                    "                push dopStr\n" +
                    "                call [printf]\n\n";
        }
    }
}

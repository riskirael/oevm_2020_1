package sample;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        MinimizingBooleanFunctions minimizing = new MinimizingBooleanFunctions();
        TruthTable truthTable = new TruthTable();
        System.out.println("X1 X2 X3 X4 F");
        truthTable.printTruthTable();
        int choise = 0;
        while (choise < 1 || choise > 2) {
            System.out.println("Выберите: 1 - Привести форму к ДНФ, 2 - Привести форму к КНФ");
            choise = sc.nextInt();
        }
        if (choise == 1) {
            minimizing.convertToDisjunctiveNormalForm(truthTable.truthTable);
        }
        if (choise == 2) {
            minimizing.convertToConjuctiveNormalForm(truthTable.truthTable);
        }
    }
}


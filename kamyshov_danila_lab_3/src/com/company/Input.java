package com.company;

import java.util.Scanner;

public class Input {

    public static void main(String... args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите систему счисления: ");
        int numberSystem = scanner.nextInt();
        System.out.print("Введите первое число: ");
        char[] firstDigit = scanner.next().toCharArray();
        System.out.print("Введите второе число: ");
        char[] secondDigit = scanner.next().toCharArray();
        System.out.print("Выберите операцию: \n 1 - '+' \n 2 - '-' \n 3 - '*' \n 4 - '/' \n");
        int arithmeticOperation = scanner.nextInt();
        int firstNumberDecimal = TransferToNumberSystem.convertToDecimal(numberSystem, firstDigit);
        int secondNumberDecimal = TransferToNumberSystem.convertToDecimal(numberSystem, secondDigit);
        char[] firstNumberBinary = TransferToNumberSystem.convertToBinary(firstNumberDecimal).toCharArray();
        char[] secondNumberBinary = TransferToNumberSystem.convertToBinary(secondNumberDecimal).toCharArray();
        switch (arithmeticOperation) {
            case 1 -> System.out.print("Сумма = " + BinaryOperations.sumOfBinaryNumbers(firstNumberBinary, secondNumberBinary));
            case 2 -> System.out.print("Разность = " + BinaryOperations.subtractionOfNumbers(firstNumberBinary, secondNumberBinary));
            case 3 -> System.out.print("Произведение = " + BinaryOperations.multiplicationOfNumbers(firstNumberBinary, secondNumberBinary));
            case 4 -> System.out.print("Частное = " + BinaryOperations.divisionOfNumbers(firstNumberBinary, secondNumberBinary));
            default -> System.out.print("Неизвестная операция");
        }
    }
}
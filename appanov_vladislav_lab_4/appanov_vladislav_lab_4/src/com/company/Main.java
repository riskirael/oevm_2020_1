package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        MinimizingBooleanFunctions MBF = new MinimizingBooleanFunctions();
        Scanner in = new Scanner(System.in);

        MBF.printForm();

        System.out.println("ДНФ или КНФ?");
        String form = in.next();

        if(form.equals("ДНФ")){
            MBF.creatureDNF();
        }

        if(form.equals("КНФ")){
            MBF.creatureKNF();
        }

        if(!form.equals("ДНФ") && !form.equals("КНФ")){
            System.out.println("Ошибка");
        }
    }
}

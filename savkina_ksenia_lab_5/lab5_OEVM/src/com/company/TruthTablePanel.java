package com.company;

import javax.swing.*;
import java.awt.Graphics;

public class TruthTablePanel extends JPanel {
    TruthTable truthTable;
    public TruthTablePanel(TruthTable table) {
        truthTable = table;
    }

    public void paint(Graphics g)  {
        super.paint(g);
        truthTable.draw(g, this.getWidth(), this.getHeight());
    }
}

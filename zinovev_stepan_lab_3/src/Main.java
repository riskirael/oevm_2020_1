import java.util.Scanner;

public class Main {
    private static int numSystem;
    private static String firstValue;
    private static String secondValue;
    private static String operation;

    private static void dataInput ()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите систему счисления: ");
        numSystem = scanner.nextInt();
        System.out.print("Введите первое число: ");
        firstValue = scanner.next();
        System.out.print("Введите второе число: ");
        secondValue = scanner.next();
        System.out.print("Введите операцию (+, -, /, *): ");
        operation = scanner.next();
    }

    private static void programLogic ()
    {
        int numSystemLowerBorder = 2;
        int numSystemUpperBorder = 16;
        String problem = "Ошибка.";
        if (!(numSystem >= numSystemLowerBorder && numSystem <= numSystemUpperBorder)) {
            System.out.println("Введите верную систему счисления.");
        } else if (Converter.convert(numSystem, 2, firstValue).equals(problem) || Converter.convert(numSystem, 2, firstValue).equals(problem)) {
            System.out.println("Введите верное значение для выбранной системы счисления.");
        } else if (getOperationResult(operation, firstValue, secondValue).equals(problem)) {
            System.out.println("Введите верный номер операции.");
        } else {
            String firstValueNumSystem = Converter.convert(numSystem, 2, firstValue);
            String secondValueNumSystem = Converter.convert(numSystem, 2, secondValue);
            String result = getOperationResult(operation, firstValueNumSystem, secondValueNumSystem);
            System.out.println(firstValueNumSystem + " " + operation + " " + secondValueNumSystem + " = " + result);
        }
    }

    private static String getOperationResult(String operation, String firstValue, String secondValue) {
        String result;
        switch (operation) {
            case "+":
                result = MathNumeralSystem.augment(firstValue, secondValue);
                return result;
            case "-":
                result = MathNumeralSystem.substract(firstValue, secondValue);
                return result;
            case "*":
                result = MathNumeralSystem.multiplicate(firstValue, secondValue);
                return result;
            case "/":
                result = MathNumeralSystem.divide(firstValue, secondValue);
                return result;
            default:
                return "Ошибка.";
        }
    }

    public static void main(String[] args) {
        dataInput();
        programLogic();
    }
}

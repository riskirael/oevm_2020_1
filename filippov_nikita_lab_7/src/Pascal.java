import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Pascal {
    StringBuilder pascalCode;

    public Pascal(String filename) {
        pascalCode = new StringBuilder();
        readCode(filename);
    }

    private void readCode(String filename) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("./" + filename));
            int symbol = br.read();
            while (symbol != -1) {
                pascalCode.append((char) symbol);
                symbol = br.read();
            }
            br.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public String getPascalCode() {
        return pascalCode.toString();
    }
}

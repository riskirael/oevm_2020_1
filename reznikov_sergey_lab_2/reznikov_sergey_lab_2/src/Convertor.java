import java.io.IOException;

public class Convertor {

    final private int DECIMAL_BASE = 10;
    final private int LETTERS_DISTANCE = 10;

    public String baseToBase (int base1, int base2, String num) throws IOException {
        int decimal;
        if (base1 == DECIMAL_BASE) {
            decimal = Integer.parseInt(num);
        }
        else {
            decimal = convertFromBase(num.toCharArray(), base1);
        }

        if(decimal == -1) {
            throw new IOException("Wrong input");
        }
        else {
            return convertToBase(decimal, base2);
        }
    }

    private String convertToBase (int num, int base) {
        StringBuilder result = new StringBuilder("");
        if(num == 0){
            return "0";
        }
        while (num > 0) {
            int tmp = num % base;
            if (tmp > 9) {
                result.append((char) ('A' + (tmp - LETTERS_DISTANCE)));
            }
            else {
                result.append(tmp);
            }
            num = num / base;
        }
        return result.reverse().toString();
    }

    private int convertFromBase(char[] num, int base) {

        int result = 0;
        for (int index = 0; index < num.length; index++) {
            int tmp;
            if (num[index] >= 'A' && num[index] <= 'F') {
                tmp = num[index] - 'A' + LETTERS_DISTANCE;
            }
            else {
                tmp = num[index] - '0';
            }
            if (tmp >= base) {
                return -1;
            }
            result += tmp * Math.pow(base, num.length - index - 1);
        }
        return result;
    }
}

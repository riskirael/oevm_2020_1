package First_Laba;

public class TheMainProcesses {

    private double convertTo10SS(DataEntry dataEntry) {
        System.out.println("Переводим число в 10 систему счисления.");
        double valueIn10SS = 0;
        double k = 1;
        for (int i = 0; i < dataEntry.getValueInIntMassive().length; i++) {
            valueIn10SS = valueIn10SS + dataEntry.getValueInIntMassive()[i] * Math.pow(dataEntry.getFrom(), dataEntry.getValueInIntMassive().length - k);
            k++;
        }
        return valueIn10SS;
    }

    private String convertToNeededSS(double valueIn10SS, DataEntry dataEntry) {

        int checkPoint = (int) valueIn10SS;
        int valueIn10SSInInt = (int) valueIn10SS;
        int countOfSymbols = 1;
        while (valueIn10SSInInt > dataEntry.getTo()) {
            valueIn10SSInInt = valueIn10SSInInt / dataEntry.getTo();
            countOfSymbols++;
        }

        char[] resultInChar = new char[countOfSymbols];
        int[] resultInInt = new int[countOfSymbols];

        System.out.println("Рассчитываем результат в требуемой системе счисления.");
        for (int i = countOfSymbols - 1; i >= 0; i--) {
            resultInInt[i] = checkPoint % dataEntry.getTo();
            checkPoint = checkPoint / dataEntry.getTo();
        }

        for (int i = 0; i < countOfSymbols; i++) {
            for (int j = 0; j < dataEntry.getCharCheckOutMassive().length; j++) {
                if (resultInInt[i] == dataEntry.getIntCheckOutMassive()[j]) {
                    resultInChar[i] = dataEntry.getCharCheckOutMassive()[j];
                }
            }
        }

        return new String (resultInChar);

    }

    public void theMainProcessesWorkInside() {

        DataEntry dataEntry = new DataEntry();
        dataEntry.initialization();

        for (int i = dataEntry.getValueInCharMassive().length - 1; i >= 0; i--) {
            for (int j = 0; j < dataEntry.getCharCheckOutMassive().length; j++) {
                if (dataEntry.getValueInCharMassive()[i] == dataEntry.getCharCheckOutMassive()[j]) {
                    dataEntry.getValueInIntMassive()[i] = dataEntry.getCharCheckOutMassive()[j];
                }
            }
        }

        double valueIn10SS = convertTo10SS(dataEntry);
        String result = convertToNeededSS(valueIn10SS, dataEntry);
        System.out.println(result);
    }
}
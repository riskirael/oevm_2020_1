Лабораторная работа №3: Модерилорование выполнения арифметических операций
----------------------
* Лабораторная работа запускается через IDEA с конфигурацией Main
* Для выполнения программы используется java.io.BufferedReader, java.io.InputStreamReader, java.io.IOException.
* Программа принимает значения
    * исходную систему счисления
    * первое число
    * второе число
    * номер операции

На выходе выдаётся строка с ответом по заданным условиям

Примеры работы
----------------------
вводимая система с. | первое число | второе число | номер арифметической операции | вывод
------------------|---------------|--------------|-------------------|-------------
2| 1010 |10 | 2 |  1000
12 | AA14 | 781 | 2 | 100010011011111
5 | 13401 | 3333 | 3  | 1111101110011000100
8| 17053| 2044 |1| 10001001001111
10 |150 |13 |4 |1011
16| FFF| 3| 1|1000000000010

package com.company;

public class Transfer {

    private int a;
    private char[] arrOne;
    private char[] arrTwo;
    private Operation operation;

    Transfer(int system, char[] arrOne, char[] arrTwo, String operation) {

        this.a = system;
        this.arrOne = arrOne;
        this.arrTwo = arrTwo;

        switch (operation) {
            case "+":
                this.operation = Operation.ADD;
                break;

            case "-":
                this.operation = Operation.SUB;
                break;

            case "*":
                this.operation = Operation.MUIL;
                break;

            case "/":
                this.operation = Operation.DIV;
                break;
        }
    }

    public void numberSystems() {

        StringBuilder out = new StringBuilder();

        //Переводим в десятичную СС
        int[] numbers = transferIn10SS(arrOne, arrTwo);
        int numberOneIn10 = numbers[0];
        int numberTwoIn10 = numbers[1];

        //Переводим в двоичную СС
        String[] strNumbers = transferInEndSS(numberOneIn10, numberTwoIn10);
        String numberOneIn2 = strNumbers[0];
        String numberTwoIn2 = strNumbers[1];

        int count = numberTwoIn10; //используется в произведение, то есть сколько раз мы сложим с собой первое или второе число
        int maxLength = 0;
        boolean negativeOut = false; //флаг о том, что результатом разности будет отрицательное число

        //Если второе число больше второго и требуемая операция не деление
        if(Long.parseLong(numberOneIn2) < Long.parseLong(numberTwoIn2) && operation != Operation.DIV) {
            maxLength = numberTwoIn2.length() * 2;
            negativeOut = true;
            count = numberOneIn10;
        }
        //Иначе если второе число больше второго и требуемая операция деление
        else if(Long.parseLong(numberOneIn2) < Long.parseLong(numberTwoIn2) && operation == Operation.DIV) {
            System.out.print(out.append(0).toString());
            return;
        } else {
            maxLength = numberOneIn2.length() * 2;
            count = numberTwoIn10;
        }

        //Заполняем незначащими нулями
        numberOneIn2 = fillingZero(numberOneIn2, maxLength);
        numberTwoIn2 = fillingZero(numberTwoIn2, maxLength);

        //Переводим в массив чисел (для удобства)
        int numberArrOne[] = transferToArrNumbers(numberOneIn2);
        int numberArrTwo[] = transferToArrNumbers(numberTwoIn2);
        int tmpArr[] = transferToArrNumbers(numberTwoIn2);

        //Если первое число меньше второго, то меняем их местами (для удобства операций)
        if(negativeOut) {

            for(int i = 0; i < maxLength + 1; i++) {
                numberArrTwo[i] = numberArrOne[i];
                numberArrOne[i] = tmpArr[i];
            }
        }

        //Вызываем операцию
        out = callOperation(operation, numberArrOne, numberArrTwo, maxLength, count, numberOneIn2);

        //Печатаем отформатированную строку
        formatOutput(out);
    }

    private void formatOutput(StringBuilder out) {

        //Удаляем при выводе незначащие нули
        while (out.length() > 0 && out.charAt(0) == '0') {
            out.deleteCharAt(0);
        }

        if(operation == Operation.SUB) {
            System.out.print("-" + out);
        } else {
            System.out.print(out);
        }
    }

    private String fillingZero(String numberIn2, int maxLength) {

        while(numberIn2.length() < maxLength + 1) {
            numberIn2 = "0" + numberIn2;
        }

        return numberIn2;
    }

    private int[] transferToArrNumbers(String numberIn2) {

        String[]strArr = numberIn2.split("");
        int numberArr[] = new int[strArr.length];

        for (int i = 0; i < strArr.length; i++) {
            numberArr[i] = Integer.parseInt(strArr[i]);
        }

        return numberArr;
    }

    private int[] transferIn10SS(char[] numberOne, char[] numberTwo) {

        int numberOneIn10 = 0; //Первое число в 10-ой СС
        int numberTwoIn10 = 0; //Второе число в 10-ой СС
        int[] arr = new int[2];

        for (int i = 0; i < numberOne.length; i++) {
            int temp = 1;

            if (((int)numberOne[i] >= (int)'0') && ((int)numberOne[i] <= (int)'9')) {
                temp = (int)numberOne[i] - (int)'0';
            } else if (((int)numberOne[i] >= (int)'A') && ((int)numberOne[i] <= (int)'Z')) {
                temp = (int)numberOne[i] + 10 - (int)'A';
            }

            numberOneIn10 += temp * (Math.pow(a, numberOne.length - i - 1));
        }

        for (int i = 0; i < numberTwo.length; i++) {
            int temp = 1;

            if (((int)numberTwo[i] >= (int)'0') && ((int)numberTwo[i] <= (int)'9')) {
                temp = (int)numberTwo[i] - (int)'0';
            } else if (((int)numberTwo[i] >= (int)'A') && ((int)numberTwo[i] <= (int)'Z')) {
                temp = (int)numberTwo[i] + 10 - (int)'A';
            }

            numberTwoIn10 += temp * (Math.pow(a, numberTwo.length - i - 1));
        }

        arr[0] = numberOneIn10;
        arr[1] = numberTwoIn10;

        return arr;
    }

    private String[] transferInEndSS(int numberOneIn10, int numberTwoIn10) {

        String[] arr = new String[2];

        String numberOneIn2 = "";//Первое число в 2-ой СС
        String numberTwoIn2 = "";//Первое число в 2-ой СС
        int tmp = 0;

        while (numberOneIn10 >= 2) {
            tmp = numberOneIn10 % 2;
            numberOneIn2 = String.valueOf(tmp) + numberOneIn2;
            numberOneIn10 /= 2;
        }

        numberOneIn2 = String.valueOf(numberOneIn10) + numberOneIn2;

        while (numberTwoIn10 >= 2) {
            tmp = numberTwoIn10 % 2;
            numberTwoIn2 = String.valueOf(tmp) + numberTwoIn2;
            numberTwoIn10 /= 2;
        }

        numberTwoIn2 = String.valueOf(numberTwoIn10) + numberTwoIn2;

        arr[0] = numberOneIn2;
        arr[1] = numberTwoIn2;

        return arr;
    }

    private StringBuilder callOperation(Operation operation, int[] numberArrOne, int[] numberArrTwo, int maxLength, int count, String numberOneIn2) {

        StringBuilder out = new StringBuilder();

        switch (operation) {

            case ADD:
                out.append(BinaryOperations.add(numberArrOne, numberArrTwo, maxLength));
                break;

            case SUB:
                out.append(BinaryOperations.subtract(numberArrOne, numberArrTwo, maxLength));
                break;

            case MUIL:
                out.append(BinaryOperations.multiply(numberArrOne, maxLength, count));
                break;

            case DIV:
                out.append(BinaryOperations.divide(numberArrTwo, maxLength, numberOneIn2));
                break;
        }

        return out;
    }
}

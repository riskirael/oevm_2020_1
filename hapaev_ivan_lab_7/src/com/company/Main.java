package com.company;

public class Main {

    public static void main(String[] args) {

        PascalRead pascalRead = new PascalRead();

        AssemblerWrite assemblerWrite = new AssemblerWrite();

        ParseString parseString = new ParseString(pascalRead.read(), assemblerWrite);
        parseString.parseVarAndBody();

        assemblerWrite.write();
    }
}

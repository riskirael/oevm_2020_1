import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {

    private JFrame frame;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main window = new Main();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public Main() {
        initialize();
    }

    private void initialize() {
        final int BUTTON_LENGTH = 100;
        final int TEXT_AREA_SIZE = 250;

        BooleanTable table = new BooleanTable();
        MyPanel panel = new MyPanel(table);

        frame = new JFrame();
        frame.setBounds(100, 100, panel.getXSize() + 20 + BUTTON_LENGTH + 20 + TEXT_AREA_SIZE,
                panel.getYSize() + 40);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        panel.setBounds(10, 10, panel.getXSize(), panel.getYSize());
        frame.getContentPane().add(panel);

        JTextArea textArea = new JTextArea();
        textArea.setBounds(panel.getXSize() + 30 + BUTTON_LENGTH, 10, TEXT_AREA_SIZE - 20, panel.getYSize());
        frame.getContentPane().add(textArea);

        JButton tableCreateButton = new JButton("Create Table");
        tableCreateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.changeNumbers();
                panel.repaint();
                textArea.setText("");
            }
        });
        tableCreateButton.setBounds(panel.getXSize() + 20, 10, BUTTON_LENGTH, 30);
        frame.getContentPane().add(tableCreateButton);

        JButton KFormButton = new JButton("K form");
        KFormButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.KFormColor();
                panel.repaint();
                String str = table.getKForm();
                textArea.setText(str);
            }
        });
        KFormButton.setBounds(panel.getXSize() + 20, 50, BUTTON_LENGTH, 30);
        frame.getContentPane().add(KFormButton);

        JButton DFormButton = new JButton("D Form");
        DFormButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.DFormColor();
                panel.repaint();
                String str = table.getDForm();
                textArea.setText(str);
            }
        });
        DFormButton.setBounds(panel.getXSize() + 20, 90, BUTTON_LENGTH, 30);
        frame.getContentPane().add(DFormButton);

    }
}

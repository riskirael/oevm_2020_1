public class Converter {
    protected static int convertTo10(char[] number, int initialSN) throws NotValidateException {
        int result = 0;
        int k = 0;
        for (int i = number.length - 1; i >= 0; i--, k++) {
            result += validate(number[i]) * Math.pow(initialSN, k);
        }
        return result;
    }

    protected static int validate(char digit) throws NotValidateException {
        int number;
        if (digit >= '0' && digit <= '9') {
            number = digit - '0';
        } else {
            if (digit >= 'A' && digit <= 'J') {
                number = digit - 'A' + 10;
            } else throw new NotValidateException();
        }
        return  number;
    }

    protected static String convertToFinal(int number, int finalSN) {
        String result = "";
        String letter;
        while (number > 0) {
            letter = "";
            int res = number / finalSN;
            int remainder = number % finalSN;
            if (remainder >= 10) {
                letter += (char) (remainder + 55);
            } else {
                letter = Integer.toString(remainder);
            }
            result = letter + result;
            number = res;
        }
        return result;
    }

    protected static String convert(int initialSN, int finalSN, char[] number) throws NotValidateException {
        int k = convertTo10(number, initialSN);
        return convertToFinal(k, finalSN);
    }
}

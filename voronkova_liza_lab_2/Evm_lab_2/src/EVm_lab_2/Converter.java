package EVm_lab_2;

public class Converter {

    public static final int Dec = 10;

    public static final int Bin = 2;

    public static final int Hex = 16;

    public static final int downAsciiBorder = 97;

    public static final int upperAsciiBorder = 102;

    public static String convertTo(String num, int initialNot, int finalNot) throws Exception {
        if (num.equals("0") || num.equals("1")) {
            return num;
        }

        int numInDec;
        try {
            numInDec = convertToDec(num, initialNot);
        } catch (Exception e) {
            throw new Exception();
        }

        if (finalNot == Dec) {
            return Integer.toString(numInDec);
        }

        StringBuilder result = new StringBuilder("");
        while (numInDec != 0) {
            if (numInDec % finalNot < Dec) {
                result.append(numInDec % finalNot);
            } else {
                result.append((char) (numInDec % finalNot + 87));
            }

            numInDec /= finalNot;
        }
        return result.reverse().toString();
    }

    private static int convertToDec(String num, int initialNot) {
        int result = 0;
        int k = 0;
        char[] numArr = num.toCharArray();
        for (int i = numArr.length - 1; i >= 0; i--) {
            if (Character.isDigit(numArr[i])) {
                result += (Character.getNumericValue(numArr[i]) * Math.pow(initialNot, k));
            } else {
                result += (numArr[i] - 87) * Math.pow(initialNot, k);
            }
            k++;
        }
        return result;
    }
}

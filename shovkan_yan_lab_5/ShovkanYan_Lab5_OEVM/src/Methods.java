import java.awt.*;

public class Methods {

    private int sizeI = 16;
    private int sizeJ = 5;

    private int X = 10;
    private int Y = 10;
    private int width = 30;
    private int height = 30;

    public void draw(int array[][], Graphics g) {

        for (int i = 0; i < sizeJ - 1; i++) {
            g.drawRect(X + width * i, Y, width, height);
            g.drawString("X" + (i + 1), X * 2 + width * i, Y * 3);
        }

        g.drawRect(X + width * (sizeJ - 1), Y, width, height);
        g.drawString("F", X + 10 + width * (sizeJ - 1), Y + 20);

        Y += 30;

        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                g.drawRect(X + width * j, Y + height * i, width, height);
                g.drawString(array[i][j] + "", X + 10 + width * j, Y + 20 + height * i);
            }
        }
        if (MainWindow._KNF == true) {
            drawKNF(array, g);
        }

        if (MainWindow._DNF == true) {
            drawDNF(array, g);
        }

        Y -= 30;

    }

    private void drawKNF(int array[][], Graphics g) {
        if (array[MainWindow._step][sizeJ - 1] == 0) {
            g.setColor(Color.GREEN);
        } else {
            g.setColor(Color.RED);
        }

        for (int j = 0; j < sizeJ; j++) {
            g.drawRect(X + width * j, Y + height * MainWindow._step, width, height);
            g.drawString(array[MainWindow._step][j] + "", X + 10 + width * j, Y + 20 + height * MainWindow._step);
        }
    }

    private void drawDNF(int array[][], Graphics g) {
        if (array[MainWindow._step][sizeJ - 1] == 1) {
            g.setColor(Color.GREEN);
        } else {
            g.setColor(Color.RED);
        }
        for (int j = 0; j < sizeJ; j++) {
            g.drawRect(X + width * j, Y + height * MainWindow._step, width, height);
            g.drawString(array[MainWindow._step][j] + "", X + 10 + width * j, Y + 20 + height * MainWindow._step);
        }
    }

    public String DNF(int array[][]) {
        int X4 = 3;
        int F = 4;

        StringBuilder strBuilder = new StringBuilder();

        if (array[MainWindow._step][F] == 1) {//если значение функции 1
            strBuilder.append("( ");//открываем (
            for (int j = 0; j < X4; j++) {
                if (array[MainWindow._step][j] == 1) { //если значение Xj 1
                    strBuilder.append("X").append(j + 1).append(" * ");//пишем "Xj * "
                } else { //если значение Xj 0
                    strBuilder.append("-X").append(j + 1).append(" * ");//пишем "-Xj * "
                }
            }
            if (array[MainWindow._step][X4] == 1) {//если значение X4 1
                strBuilder.append("X4");//пишем "X4"
            } else {//если значение X4 0
                strBuilder.append("-X4");//пишем "-X4"
            }
            strBuilder.append(" ) + ");//закрываем ), ставим знак +

            return strBuilder.append("\n").toString();
        }
        return "";

    }

    public String KNF(int array[][]) {
        int X4 = 3;
        int F = 4;

        StringBuilder strBuilder = new StringBuilder();

        if (array[MainWindow._step][F] == 0) { //если значение функции 0
            strBuilder.append("( "); //открываем (
            for (int j = 0; j < X4; j++) {
                if (array[MainWindow._step][j] == 0) { //если значение Xj 0
                    strBuilder.append("X").append(j + 1).append(" + "); //пишем "Xj + "
                } else { //если значение 1
                    strBuilder.append("-X").append(j + 1).append(" + "); //пишем "-Xj + "
                }
            }
            if (array[MainWindow._step][X4] == 0) { //если значение X4 0
                strBuilder.append("X4"); //пишем "X4"
            } else { //если значение X4 1
                strBuilder.append("-X4");//пишем "-X4"
            }
            strBuilder.append(" ) * ");//закрываем ), ставим знак *

            return strBuilder.append("\n").toString();
        }

        return "";
    }

}


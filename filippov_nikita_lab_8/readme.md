<!DOCTYPE html>
<html>
<head>
    <meta charset ="utf-8">
</head>
<body>
Лабораторная работа №8<br>
выполнил: Филиппов Никита<br>
группа: ПИбд-22<br>

Задача:
Разобрать и собрать ЭВМ

Решение задачи:

Выполнение данной лабораторной работы будет усложнено следующими факторами:
<ol>
    <li>Вид ЭВМ – ноутбук </li>
    <li>Ноутбук не принадлежит мне</li>
    <li>Ноутбук был передан в мои руки в разобранном состоянии</li>
    <li>Работоспособность «ноутбука» не гарантирована</li>
</ol>

Начинаем сборку с материнской платы.<br> 
<a href="https://drive.google.com/file/d/19qV_zDbTwBhkF5h88trSCPqg9J-yNOQ-/view?usp=sharing">Материнская плата ноутбука</a><br>
Устанавливаем ЦП<br>
<a href="https://drive.google.com/file/d/1a9QTRZ-skenzmHu1FODbn4mRtV_Ow4dd/view?usp=sharing">ЦП установлен</a><br>
Наносим термопасту на ЦП и чип видеокарты. Прикручиваем радиатор и устанавливаем батарейку CR2032 BIOSа.<br>
<a href="https://drive.google.com/file/d/1sITH7WOBkgeffE9rZzLL9IDVj5p2Vsyr/view?usp=sharing">Система охлаждения установлена</a><br>
Устанавливаем модуль Bluetooth<br>
<a href="https://drive.google.com/file/d/143Ble2vKmDQ0YqbVh34R_4S1iROw8_y7/view?usp=sharing">Модуль BT установлен</a><br>
Устанавливаем плату USB/POWER/LAN в корпус ноутбука. Прикручиваем и подсоединяем экран.<br>
<a href="https://drive.google.com/file/d/1gNToDCMW-VYAcsVIEkcgH4T9OPX2NsH9/view?usp=sharing">Плата USB/POWER/LAN установлена</a><br>
Соединяем мать и плату, располагаем материнку в корпусе. Присоединяем шлейфы все которые видим во все что можем. Подсоединяем жесткий диск<br>
<a href="https://drive.google.com/file/d/1htT0W20N-Uh2E40pm2rItsqAuAY4YBNg/view?usp=sharing">Материнка в сборе</a><br>
Устанавливаем верхнюю часть корпуса ноутбука. Опять же все шлейфы куда глаза глядят. Вставляем дисковод<br>
<a href="https://drive.google.com/file/d/10-ICT2yBZakBnrqTA5hzrqoozjO-1MBs/view?usp=sharing">Верхняя часть корпуса установлена</a><br>
Устанавливаем клавиатуру<br>
<a href="https://drive.google.com/file/d/1L3Vv56HXMZofZpVVc7NO5CnAOIYWhjbz/view?usp=sharing">Клавиатура установлена</a><br>
Вставляем планки оперативной памяти, устанавливаем аккумулятор<br>
<a href="https://drive.google.com/file/d/1WjN6GKigVQEeG-o4RmXwTRel3h0fKsIq/view?usp=sharing">Планки оперативной памяти установлены</a><br>
Устанавливаем виндовс и радуемся жизни<br>
<a href="https://drive.google.com/file/d/10VKZX205ScZ9dq-hW8pRRQHHhXQizD7T/view?usp=sharing">Результат</a><br>
</body>
</html>
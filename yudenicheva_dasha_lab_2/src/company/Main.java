package company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Укажите исходную систему счисления числа: ");
        int initial = scanner.nextInt();
        System.out.println("Укажите систему, в которую хотите перевести число: ");
        int desired = scanner.nextInt();
        System.out.println("Укажите число: ");
        char[] number = scanner.next().toCharArray();
        Convertor convert = new Convertor(initial, desired, number);
        convert.checkResultOutput();
    }
}

package com.company;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Calculation calculation = new Calculation();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите исходную систему счисления (2-16)");
        long originalNS = scanner.nextLong();
        scanner.nextLine();
        System.out.println("Введите первое число");
        String number1 = scanner.nextLine();
        System.out.println("Введите второе число");
        String number2 = scanner.nextLine();
        System.out.println("Введите арифметическую операцию (+,-,*,/)");
        char operation = (char) System.in.read();
        scanner.close();
        if (originalNS >= 2 && originalNS <= 16) {
            if (calculation.isNumOriginalNS(originalNS, number1) && calculation.isNumOriginalNS(originalNS, number2)) {
                System.out.println("Результат:");
                System.out.println(calculation.solve(operation, number1, number2, originalNS));
            } else {
                System.out.println("Данных цифр в исходной системе счисления не существует");
            }
        } else {
            System.out.println("Введеная система счисления не входит в промежуток (2-16)");
        }
    }
}


package com.company;
import java.util.*;
import java.lang.*;

public class Main {
    public static void main(String[] args) throws Exception{
        System.out.print("Введите исходную систему счисления: ");
        Scanner initial = new Scanner(System.in);
        int initialSystem = initial.nextInt();

        System.out.print("Введите первое число: ");
        Scanner firstNumS = new Scanner(System.in);
        String firstNum = firstNumS.nextLine();

        System.out.print("Введите второе число: ");
        Scanner secondNumS = new Scanner(System.in);
        String secondNum = secondNumS.nextLine();

        System.out.print("Введите операцию: ");
        Scanner operationS = new Scanner(System.in);
        String operation = operationS.nextLine();

        char[] firstNumCh = firstNum.toCharArray();
        char[] secondNumCh = secondNum.toCharArray();
        char[] operationCh = operation.toCharArray();
        int firstNumberInt = 0;
        int secondNumberInt = 0;
        String firstNumberBin = "";
        String secondNumberBin = "";
        String result = "";

        firstNumCh = HelperClass.toUpper(firstNum, firstNumCh);
        firstNumberInt = HelperClass.convertTo10Base(firstNum, firstNumCh, firstNumberInt, initialSystem);

        secondNumCh = HelperClass.toUpper(secondNum, secondNumCh);
        secondNumberInt = HelperClass.convertTo10Base(secondNum, secondNumCh, secondNumberInt, initialSystem);

        firstNumberBin = HelperClass.convertTo2Base(firstNumberInt, firstNumberBin);
        secondNumberBin = HelperClass.convertTo2Base(secondNumberInt, secondNumberBin);

        try{
            result = HelperClass.interactions(operationCh, firstNumberBin, secondNumberBin, result, firstNumberInt, secondNumberInt);
            System.out.print(firstNum + " " + operation + " " + secondNum + " = " + result);
        } catch (Exception e){
            System.out.println("Неверная арифметическая операция");
        }
    }
}
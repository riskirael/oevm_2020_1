package com.company;

public class Correctness {
    public static void correctBase(int initial, int next){
        if(initial < 2 || initial > 16  || next < 2  || next > 16 ){
            System.out.print("Base must be >2 or <16 ");
            System.exit(1);
        }
    }
    public static void correctNumber(int initial, char[] number){
        for (int i = 0; i < number.length; i++){
            char digit = Character.toUpperCase(number[i]);

            if (Character.isDigit(digit) && (digit-'0') >= initial ) {
                System.out.print("Invalid number");
                System.exit(1);
            } else if (Character.isLetter(digit) && (digit - 'A') + Main.OFFSET >= initial) {
                System.out.print("Invalid number");
                System.exit(1);
            } else if (!Character.isLetter(digit) && !Character.isDigit(digit)) {
                System.out.print("Invalid number");
                System.exit(1);
            }
        }
    }
}

Лабораторная работа №4: Изучение методов трансляции
----------------------
### Задание:

Разработать транслятор программ Pascal-FASM.
Предусмотреть проверку синтаксиса и семантики исходной программы.
Результатомработы транслятора является программа для ассемблера FASM,
идентичная по функционалу исходной программе.
Полученная программа должна компилироваться и выполняться без ошибок.
Исходная программа на языке программирования Pascal имеет вид:

var  
    x, y: integer;  
    res1, res2, res3, res4: integer;  
begin  
    write(‘input x: ’); readln(x);  
    write(‘input y: ’); readln(y);  
    res1 := x + y; write(‘x + y = ’); writeln(res1);  
    res2 := x - y; write(‘x - y = ’); writeln(res2);  
    res3 := x * y; write(‘x * y = ’); writeln(res3);  
    res4 := x / y; write(‘x / y = ’); writeln(res4);  
end.  
-----------------------------------------------
* Лабораторная работа запускается через IDEA с конфигурацией Main
* Для выполнения программы используется java.io.FileWriter, java.io.BufferedReader, java.io.IOException, java.io.FileReader,java.util.ArrayLis, java.util.regex.Matcher, java.util.regex.Pattern.
* Программа принимает значения
    * два числа, для проведения арифметических операций

Примеры работы
----------------------
 Паскаль: 
 ```
 var
 	x, y: integer;
 	res1, res2, res3, res4: integer;
 begin
 	write('input x: '); readln(x);
 	write('input y: '); readln(y);
 	res1:= x - y; write('x + y = '); writeln(res1);
 	res2:= x + y; write('x - y = '); writeln(res2);	
 	res3:= x * y; write('x * y = '); writeln(res3);
 	res4:= x / y; write('x / y = '); writeln(res4);
 end.
```
Ассемблер: 
```
format PE console

entry start

include 'win32a.inc'

section '.data' data readable writable
string0 db 'input x: ', 0
string1 db 'input y: ', 0
string2 db 'x + y = ', 0
string3 db 'x - y = ', 0
string4 db 'x * y = ', 0
string5 db 'x / y = ', 0
spaceStr db '%d', 0
dopStr db '%d', 0ah, 0 
x dd ?
y dd ?
res1 dd ?
res2 dd ?
res3 dd ?
res4 dd ?
section '.code' code readable executable
   start:
push string0
call [printf]
push x
push spaceStr
call [scanf]

push string1
call [printf]
push y
push spaceStr
call [scanf]

mov ecx, [x]
sub ecx, [y]
mov [res1], ecx
push string2
call [printf]
push [res1]
push dopStr
call [printf]

mov ecx, [x]
add ecx, [y]
mov [res2], ecx
push string3
call [printf]
push [res2]
push dopStr
call [printf]

mov ecx, [x]
imul ecx, [y]
mov [res3], ecx
push string4
call [printf]
push [res3]
push dopStr
call [printf]

mov eax, [x]
mov ecx, [y]
div ecx
mov [res4], eax
push string5
call [printf]
push [res4]
push dopStr
call [printf]

         finish:

                call [getch]

                call [ExitProcess]

section '.idata' import data readable

        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvcrt,\
               printf, 'printf',\
               scanf, 'scanf',\
               getch, '_getch'
```
[Ссылка на видео](https://yadi.sk/i/tmDBFd1kib3LZA)
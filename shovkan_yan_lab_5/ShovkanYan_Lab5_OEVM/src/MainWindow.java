import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.*;

public class MainWindow {
    Methods methods = new Methods();
    Random random = new Random();
    StringBuilder strBuilder = new StringBuilder();

    private int[][] array = {
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)},
    };
    static int _step = -1;
    static boolean _DNF = false;
    static boolean _KNF = false;

    private JPanel MyPanel = new MyPanel(array);
    private JFrame frame;
    private JButton btnNextStep = new JButton("Next step");
    private JButton btnDNF = new JButton("DNF");
    private JButton btnKNF = new JButton("KNF");
    private JTextArea txtFunction = new JTextArea();

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainWindow window = new MainWindow();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public MainWindow() {

        initialize();

    }

    private void initialize() {

        frame = new JFrame();
        frame.setBounds(100, 100, 700, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        MyPanel.setBounds(0, 0, 170, 600);
        frame.getContentPane().add(MyPanel);

        txtFunction.setEditable(false);
        txtFunction.setBounds(490, 10, 200, 600);
        frame.getContentPane().add(txtFunction);

        btnNextStep.setEnabled(false);
        btnNextStep.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _step++;
                if (_DNF == true) {
                    txtFunction.setText(strBuilder.append(methods.DNF(array)).toString());
                }
                if (_KNF == true) {
                    txtFunction.setText(strBuilder.append(methods.KNF(array)).toString());
                }
                if (_step == 15) {
                    btnNextStep.setEnabled(false);
                    txtFunction.setText(strBuilder.delete(strBuilder.toString().length() - 3, strBuilder.toString().length() - 1).toString());
                }

                MyPanel.repaint();
            }
        });
        btnNextStep.setBounds(180, 10, 90, 30);
        frame.getContentPane().add(btnNextStep);

        btnDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnNextStep.setEnabled(true);
                btnKNF.setEnabled(false);
                btnDNF.setEnabled(false);
                _step = 0;
                _DNF = true;
                MyPanel.repaint();
            }
        });
        btnDNF.setBounds(280, 10, 90, 30);
        frame.getContentPane().add(btnDNF);

        btnKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnNextStep.setEnabled(true);
                btnKNF.setEnabled(false);
                btnDNF.setEnabled(false);
                _step = 0;
                _KNF = true;
                MyPanel.repaint();
            }
        });
        btnKNF.setBounds(380, 10, 90, 30);
        frame.getContentPane().add(btnKNF);
    }
}
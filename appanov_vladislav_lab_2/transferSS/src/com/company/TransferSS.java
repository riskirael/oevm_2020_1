package com.company;

import java.util.Scanner;

public class TransferSS {
    private int startSS = 0;
    private int endSS = 0;
    private String numStartString = "";

    public TransferSS(int startSS, int endSS, String numStartString ){
        this.startSS = startSS;
        this.endSS = endSS;
        this.numStartString = numStartString;
    }

    private int transferNumIn10SS(char[] arrChar){
        int cash = 0;
        int degree = 0;
        int numIn10SS = 0;
        int minSS = 2;
        int maxSS = 16;
        int assistan = 10;

        if(startSS < minSS || startSS > maxSS || endSS < minSS || endSS > maxSS){
            return -1;
        }

        for(int i = arrChar.length - 1; i >= 0; i--){
            if(arrChar[i] >= '0' && arrChar[i] <= '9'){
                cash = Character.getNumericValue(arrChar[i]);
            } else{
                if(arrChar[i] >= 'A' && arrChar[i] <= 'F'){
                    cash = arrChar[i] - 'A' + assistan;
                } else{
                    return -1;
                }
            }

            if(cash >= startSS){
                return -1;
            }

            numIn10SS += cash * Math.pow(startSS, degree);
            degree++;
        }

        return numIn10SS;
    }

    private StringBuilder transferNumInEndSS(int numIn10SS){
        int assistan = 10;
        int assistan2 = 9;
        StringBuilder endNumStr = new StringBuilder();

        if(numIn10SS == 0){
            endNumStr.append(0);
            return endNumStr;
        }

        while(numIn10SS != 0){
            if(numIn10SS % endSS > assistan2) {
                endNumStr.append((char)(numIn10SS - assistan + 'A'));
            } else{
                endNumStr.append(numIn10SS % endSS);
            }
            numIn10SS = numIn10SS / endSS;
        }

        endNumStr.reverse();
        return endNumStr;
    }

    public void transferSS(){
        char[] arrChar = numStartString.toCharArray();
        int numIn10SS = 0;
        StringBuilder endNumStr;

        numIn10SS = transferNumIn10SS(arrChar);

        if(numIn10SS == -1){
            System.out.println("Ошибка");
            return;
        }

        endNumStr = transferNumInEndSS(numIn10SS);
        System.out.println(endNumStr);
    }



}

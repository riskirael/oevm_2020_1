package com.company;

import java.util.Scanner;

public class Main {
    static int firstScale;
    static int secondScale;
    static String number;
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        input();
        convertNumber();
    }

    public static void input() {

        System.out.println("Input first scale of notation:");

        firstScale = scanner.nextInt();

        System.out.println("Input second scale of notation:");
        secondScale = scanner.nextInt();

        System.out.println("Input the number:");
        number = scanner.next();
    }

    public static void convertNumber() {
        Convertor convertor = new Convertor(firstScale, secondScale, number);
        int DecimalNumber = convertor.convertToDecimal();
        if (DecimalNumber == -1) {
            System.out.println("Wrong");
            return;
        }

        System.out.println("Your result:");
        System.out.println(convertor.convertToSecondScale(DecimalNumber));
    }

}

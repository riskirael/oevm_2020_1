Лабораторная работа №1

# "Исследование состояния производства элементарной базы"


_Комплектующие_

| Наименование | Для чего нужен|
|---|---|
| Процессор Intel Core i9-10900K BOX | основная часть компьютера, отвечает за его работу.|
| Материнская плата GIGABYTE Z490 AORUS XTREME WF | подключает все части компьютера.|
| Корпус GAMER STORM Quadstellar [DP-EATX-QUADSTLR] | является вместилещем всех элементов ПК.|
| Видеокарта GIGABYTE AORUS GeForce RTX 2080 Ti XTREME WATERFORCE [GV-N208TAORUSX W-11GC] | Компонент ПК, отвечающий за графическую часть.|
| Система охлаждения EKWB EK-KIT P240 | водянка, охлаждающая систему ПК.|
| Оперативная память Corsair Vengeance LPX [CMK64GX4M4B3600C18] 64 ГБ | Часть памяти отвечающая за обработку временных данных, задействованных ПК.|
| 2048 ГБ SSD M.2 накопитель Apacer AS2280Q4 [AP2TBAS2280Q4-1] | Основное хранилеще данных. M2 новый тип памяти, схожий с SSD, но имеющий большую скорость обработки информации.|
| Блок питания Thermaltake Toughpower DPS G RGB 1500W [PS-TPG-1500DPCTEU-T] | Обеспечение комплектующих ПК электричеством.|

Компьютер для разного рода задач(Minecraft тянет)



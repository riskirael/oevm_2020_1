public class Convertor {
    private static final char[] CODECHAR = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static final int[] CODEINT = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

	public static String convertToUnsignedBinary(int originalSON, String num) {
        int value = convertToDecimal(originalSON, unsignedFormat(num));
        int ranks = 0;

        int _newSON = 2;//перевод в 2 СС
        while (value >= _newSON) {
            ranks++;
            value /= _newSON;
        }
        ranks++;

        char[] Result = new char[ranks];
        value = convertToDecimal(originalSON, num);

        for (int i = ranks - 1; i > -1; i--) {
            for (int j = 0; j < _newSON; j++) {
                if (value % _newSON == CODEINT[j]) {
                    Result[i] = CODECHAR[j];
                    value /= _newSON;
                    break;
                }
            }
        }
        return new String(Result);
    }

    private static int convertToDecimal(int originalSON, String num) {
        char[] valueCharArray = num.toCharArray();
        int value = 0;

        for (int i = 0; i < valueCharArray.length; i++) {
            int power = valueCharArray.length - i - 1;

            for (int j = 0; j < originalSON; j++) {
                if (valueCharArray[i] == CODECHAR[j]) {
                    value += CODEINT[j] * (int) Math.pow(originalSON, power);
                    break;
                }
            }
        }
        return value;
    }    

    private static String unsignedFormat(String value) {
        StringBuilder unsignedValue = new StringBuilder();

        if (value.toCharArray()[0] == '-') {
            char[] valueArray = value.toCharArray();

            for (int i = 1; i < valueArray.length; i++) {
                unsignedValue.append(valueArray[i]);
            }
            return unsignedValue.toString();
        }
        else {
            return value;
        }
    }
}
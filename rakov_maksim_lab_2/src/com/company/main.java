package com.company;

import java.util.Scanner;

    public class main {
        public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Введите изначальную систему счисления: ");
        int startSystem = s.nextInt();
        System.out.print("Введите конечную систему счисления: ");
        int endSystem = s.nextInt();
        System.out.print("Введите число для перевода: ");
        char[] number = s.next().toCharArray();
        String outcome = HelperClass.convertToOtherSystem(startSystem, endSystem, number);
        System.out.print(outcome);
    }
}

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        TranslationNumber tNumber = new TranslationNumber();
        Calculation calculation = new Calculation();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Укажите систему счисления");
        int systemC = Integer.parseInt(reader.readLine());

        System.out.println("Введите первое число");
        String firstNumber = reader.readLine();
        firstNumber.toUpperCase();

        System.out.println("Введите второе число");
        String secondNumber = reader.readLine();
        secondNumber.toUpperCase();

        System.out.println("Введите номер арифметическое операции");
        System.out.println("1 - сложение");
        System.out.println("2 - вычитание");
        System.out.println("3 - умножение");
        System.out.println("4 - деление");
        String operate = reader.readLine();

        char[] firstNumberBinary = tNumber.transferToTheBinary(tNumber.transferToTheDecimal(systemC, firstNumber)).toString().toCharArray();
        char[] secondNumberBinary = tNumber.transferToTheBinary(tNumber.transferToTheDecimal(systemC, secondNumber)).toString().toCharArray();

        boolean a = tNumber.transferToTheDecimal(systemC, firstNumber) > 0 && tNumber.transferToTheDecimal(systemC, secondNumber) > 0;
        boolean b = tNumber.transferToTheDecimal(systemC, firstNumber) < 0 && tNumber.transferToTheDecimal(systemC, secondNumber) < 0;
        boolean c = tNumber.transferToTheDecimal(systemC, firstNumber) > 0 && tNumber.transferToTheDecimal(systemC, secondNumber) < 0;
        boolean d = tNumber.transferToTheDecimal(systemC, firstNumber) < 0 && tNumber.transferToTheDecimal(systemC, secondNumber) > 0;
        switch (operate) {
            case "1":
                if (a) {
                    System.out.println(calculation.sum(firstNumberBinary, secondNumberBinary, false));
                }
                if (b) {
                    System.out.println(calculation.sum(firstNumberBinary, secondNumberBinary, true));
                }
                if (c) {
                    System.out.println(calculation.subtraction(firstNumberBinary, secondNumberBinary,false));
                }
                if (d) {
                    System.out.println(calculation.subtraction(secondNumberBinary, firstNumberBinary,false));
                }
                break;
            case "2":
                if (a) {
                    System.out.println(calculation.subtraction(firstNumberBinary, secondNumberBinary,false));
                }
                if (b) {
                    System.out.println(calculation.sum(firstNumberBinary, secondNumberBinary, true));
                }
                if (c) {
                    System.out.println(calculation.sum(firstNumberBinary, secondNumberBinary,false));
                }
                if (d) {
                    System.out.println(calculation.sum(firstNumberBinary, secondNumberBinary,true));
                }
                break;
            case "3":
                if (a||b) {
                    System.out.println(calculation.multiply(firstNumberBinary, secondNumberBinary,false));
                }

                if (c||d) {
                    System.out.println(calculation.multiply(firstNumberBinary, secondNumberBinary,true));
                }
                break;
            case "4":
                if (a||b) {
                    System.out.println(calculation.divide(firstNumberBinary, secondNumberBinary,false));
                }

                if (c||d) {
                    System.out.println(calculation.divide(firstNumberBinary, secondNumberBinary,true));
                }

                break;
        }
    }
}

package com.exmple.program;

public class Helper
{
    // Два массива выступают в роли словаря для кодирования/декодирования

    private char[] codeChar = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private int[] codeInt = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    private int _fromNS;
    private int _toNS;
    private String _value;

    public void data(int fromNS, int toNS, String value)
    {
        _fromNS = fromNS;
        _toNS = toNS;
        _value = value;
    }
    private long convertTo10NS() {
        char[] valueCharArray = _value.toCharArray();
        long valueLong = 0;

        for (int i = 0; i < valueCharArray.length; i++) {
            int power = valueCharArray.length - i - 1;

            for (int j = 0; j < _fromNS; j++) {
                if (valueCharArray[i] == codeChar[j]) {
                    valueLong += codeInt[j] * (int) Math.pow(_fromNS, power);
                    break;
                }
            }
        }
        return valueLong;
    }

    public String convertToNumeralSystemString() {
        long valueLong = convertTo10NS();
        int ranks = 0;

        while (valueLong >= _toNS) {
            ranks++;
            valueLong /= _toNS;
        }
        ranks++;

        char[] valueResultCharArray = new char[ranks];
        valueLong = convertTo10NS();

        for (int i = ranks - 1; i > -1; i--) {
            for (int j = 0; j < _toNS; j++) {
                if (valueLong % _toNS == codeInt[j]) {
                    valueResultCharArray[i] = codeChar[j];
                    valueLong /= _toNS;
                    break;
                }
            }
        }
        return new String(valueResultCharArray);
    }
}

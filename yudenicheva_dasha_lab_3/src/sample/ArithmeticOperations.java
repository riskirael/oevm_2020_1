package sample;

import java.util.Arrays;

public class ArithmeticOperations {
    /**
     * функция разности двух двоичных чисел
     */
    public static String differenceOfBinaryNum(char[] subtrahend, char[] minuend) {
        boolean minus = false;

        if (Convertor.convertToDec(2, minuend) < Convertor.convertToDec(2, subtrahend)) {
            minus = true;
            char[] tmp = subtrahend;
            subtrahend = minuend;         //меняем местами с помощью доп переменной tmp
            minuend = tmp;
        } else if (Convertor.convertToDec(2, minuend) == Convertor.convertToDec(2, subtrahend)) {
            return "0";
        }

        StringBuilder finish = new StringBuilder();
        int difference = minuend.length - subtrahend.length;
        char[] reverse = new char[minuend.length];

        //обратный код
        for (int i = 0; i < reverse.length; ++i) {
            if (i < difference) {
                reverse[i] = '1';
            } else {
                if (subtrahend[i - difference] == '0') {
                    reverse[i] = '1';
                } else {
                    reverse[i] = '0';
                }
            }
        }
        //дополнительный код
        char[] add = sumOfBinaryNum(reverse, new char[]{'1'}).toCharArray();
        //дополнительный код + уменьшаемое
        char[] addSum = sumOfBinaryNum(add, minuend).toCharArray();
        boolean zero = true;

        for (int i = 1; i < addSum.length; ++i) {
            if (addSum[i] == '1') {
                zero = false;
            } else {
                if (zero) {
                    continue;
                }
            }
            finish.append(addSum[i]);
        }
        if (minus) {
            finish.insert(0, '-');
        }
        return finish.toString();
    }

    /**
     * функция суммы двух двоичных чисел
     */
    public static String sumOfBinaryNum(char[] s1, char[] s2) {
        StringBuilder finish = new StringBuilder();
        char curChar;
        int tmp = 0;
        int i = s1.length - 1;
        int j = s2.length - 1;

        for (; i >= 0 || j >= 0 || tmp == 1; --i, --j) {
            int number2;
            int number1;
            //первое двоичное число
            if (i < 0) {
                number1 = 0;
            } else if (s1[i] == '0') {
                number1 = 0;
            } else {
                number1 = 1;
            }

            //второе двоичное число
            if (j < 0) {
                number2 = 0;
            } else if (s2[j] == '0') {
                number2 = 0;
            } else {
                number2 = 1;
            }

            int currentSum = number1 + number2 + tmp;

            if (currentSum == 0) {
                curChar = '0';
                tmp = 0;
            } else if (currentSum == 1) {
                curChar = '1';
                tmp = 0;
            } else if (currentSum == 2) {
                curChar = '0';
                tmp = 1;
            } else {
                curChar = '1';
                tmp = 1;
            }//currentSum == 3
            finish.insert(0, curChar);
        }
        return finish.toString();
    }



    /**
     * функция произведения двух двоичных чисел
     */
    public static String compositionOfBinaryNum(char[] firstMultiplier, char[] secondMultiplier) {
        char[] finish = {};

        for (int i = 0; i < Convertor.convertToDec(2, secondMultiplier); ++i) {
            finish = (sumOfBinaryNum(finish, firstMultiplier)).toCharArray();
        }
        return new String(finish);
    }
    /**
     * функция частного двух двоичных чисел
     */
    public static String divisionOfBinaryNum(char[] dividend, char[] divider) {
        long finish = 0;

        while (Convertor.convertToDec(2, dividend) >= Convertor.convertToDec(2, divider) && !Arrays.equals(dividend, new char[]{'0'})) {
            dividend = (differenceOfBinaryNum(dividend, divider)).toCharArray();
            finish++;
        }
        return Convertor.convertToBinary(finish);
    }
}

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PascalParser {
    private AssemblerWriter _assembler;

    private String _result;
    private String varsBlock;
    private String codsBlock;

    private ArrayList<String> _varsString = new ArrayList<>();
    private ArrayList<String> _codsString = new ArrayList<>();
    private ArrayList<String> _vars = new ArrayList<>();

    public PascalParser(String str, AssemblerWriter writer) {
        _result = str;
        _assembler = writer;
    }

    public void parsePascalFile() {
        Pattern patternVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherVariables = patternVariables.matcher(_result);

        while (matcherVariables.find()) {
            varsBlock = _result.substring(matcherVariables.start(), matcherVariables.end());
        }

        Pattern patternCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherCode = patternCode.matcher(_result);

        while (matcherCode.find()) {
            codsBlock = _result.substring(matcherCode.start(), matcherCode.end());
        }

        parseStrings();
        parseCodsStrings();
    }

    public void parseStrings() {
        Pattern patternVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherVariables = patternVariables.matcher(varsBlock);

        while (matcherVariables.find()) {
            _varsString.add(varsBlock.substring(matcherVariables.start(), matcherVariables.end()));
        }

        parseInteger();
    }

    public void parseInteger() {
        Pattern patternVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherVariables;
        String var;

        for (String string : _varsString) {
            matcherVariables = patternVariables.matcher(string);

            while (matcherVariables.find()) {
                var = string.substring(matcherVariables.start(), matcherVariables.end());
                if (!var.equals("integer")) { _vars.add(var); }
            }
        }
        _assembler.addToVariables(_vars);
    }

    public void parseCodsStrings(){
        Pattern stringPattern = Pattern.compile("[a-zA-Z].*;");
        Matcher stringMatcher = stringPattern.matcher(codsBlock);

        while (stringMatcher.find()) {
            _codsString.add(codsBlock.substring(stringMatcher.start(), stringMatcher.end()));
        }
        createCode();
    }

    public void createCode(){
        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        Pattern patternReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternOperation =
                Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");

        _codsString.forEach(string ->
        {
            if (string.matches(patternWrite.toString())) {
                Matcher matcherCode = patternWrite.matcher(string);

                if (matcherCode.find()) {
                    _assembler.addToCodeWrite(matcherCode.group(1));
                }
            }

            else if (string.matches(patternReadLn.toString())) {
                Matcher matcherCode = patternReadLn.matcher(string);

                if (matcherCode.find()) {
                    _assembler.addToCodeReadLn(matcherCode.group(1));
                }
            }

            else if (string.matches(patternOperation.toString())) {
                Matcher matcherCode = patternOperation.matcher(string);

                if (matcherCode.find()) {
                    _assembler.addToCodeOperation(matcherCode.group(1), matcherCode.group(2), matcherCode.group(3), matcherCode.group(4));
                }
            }

            else if (string.matches(patternWriteLn.toString())) {
                Matcher matcherCode = patternWriteLn.matcher(string);

                if (matcherCode.find()) {
                    _assembler.addToCodeWriteLn(matcherCode.group(1));
                }
            }
        });
    }
}

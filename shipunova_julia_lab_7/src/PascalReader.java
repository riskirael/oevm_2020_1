import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PascalReader {
    private String path;

    public PascalReader(String path) {
        this.path = path;
    }

    public void readPasFile(CommandStorage commandStorage) {
        try (FileReader reader = new FileReader(path)) {
            Scanner scanner = new Scanner(reader);
            StringBuilder textStringBuilder = new StringBuilder();
            while (scanner.hasNextLine()) {
                textStringBuilder.append(scanner.nextLine()).append("\n");
            }
            String code = textStringBuilder.toString();
            codeProcessing(code, commandStorage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private  void codeProcessing(String code, CommandStorage commandStorage) {
        varProcessing(code, commandStorage);
        commandProcessing(code, commandStorage);
        System.out.println("Файл по адресу " + path + " успешно прочитан");
    }

    private void varProcessing(String code, CommandStorage commandStorage) {
        Pattern patternVariablesString = Pattern.compile("(?<=var\\n)[\\w\\W]*(?=\\nbegin)");
        Matcher matcherVariablesString = patternVariablesString.matcher(code);

        if (matcherVariablesString.find()) {
            String variables = matcherVariablesString.group();

            Pattern patternVariables = Pattern.compile("\\b[a-zA-Z][\\w]*\\b");
            Matcher matcherVariables = patternVariables.matcher(variables);

            while (matcherVariables.find()) {
                if (!matcherVariables.group().equals("integer")) {
                    commandStorage.addVar(matcherVariables.group() + " dd ?\n");
                }
            }
        }
    }

    private void commandProcessing(String code, CommandStorage commandStorage) {
        Pattern patternCommandsString = Pattern.compile("(?<=begin\\n)[\\w\\W]*(?=\\nend.)");
        Matcher matcherCommandString = patternCommandsString.matcher(code);
        if (matcherCommandString.find()) {
            String commands = matcherCommandString.group();

            Pattern patternCommands = Pattern.compile("[\\w][\\w /,=*+:()'-]*;");
            Matcher matcherCommands = patternCommands.matcher(commands);

            while (matcherCommands.find()) {

                String command = matcherCommands.group();
                String valueWithoutQuotes = null;
                if (command.contains("(")) {
                    valueWithoutQuotes = command.substring(command.indexOf('(') + 1, command.indexOf(')'));
                }

                if (command.contains("readln")) {
                    commandStorage.addCommandToRead(valueWithoutQuotes);
                } else if (command.contains("write")) {
                    writeProcessing(command, commandStorage, valueWithoutQuotes);
                } else if (command.contains(":=")) {
                    operationProcessing(command, commandStorage);
                }
            }
        }
    }

    private int VarCount = 0;

    private void writeProcessing(String command, CommandStorage commandStorage, String valueWithoutQuotes) {
        String endVariable = "', 0\n";
        String endVariableNewLine = ", 0dh, 0ah, 0\n";
        if (command.contains("'")) {
            String value = command.substring(command.indexOf('(') + 2, command.indexOf(')') - 1);
            String newVariable = "string" + VarCount + " db '" + value;

            if (command.contains("writeln")) {
                newVariable += endVariableNewLine;
            } else {
                newVariable += endVariable;
            }

            commandStorage.addVar(newVariable);
            commandStorage.addCommandToWrite("string" + VarCount);
            VarCount++;
        } else {
            if (command.contains("writeln")) {
                commandStorage.addCommandToWriteln(valueWithoutQuotes);
            } else {
                commandStorage.addCommandToWrite(valueWithoutQuotes);
            }
        }
    }

    private void operationProcessing(String command, CommandStorage commandStorage) {
        String result = command.substring(0, command.indexOf(":") - 1);
        if (command.contains("+")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("+") - 1);
            String secondElement = command.substring(command.indexOf("+") + 2, command.length() - 1);
            commandStorage.addCommandToMath(result + " " + firstElement + " " + secondElement + " add");
        } else if (command.contains("-")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("-") - 1);
            String secondElement = command.substring(command.indexOf("-") + 2, command.length() - 1);
            commandStorage.addCommandToMath(result + " " + firstElement + " " + secondElement + " sub");
        } else if (command.contains("*")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("*") - 1);
            String secondElement = command.substring(command.indexOf("*") + 2, command.length() - 1);
            commandStorage.addCommandToMath(result + " " + firstElement + " " + secondElement + " imul");
        } else if (command.contains("div")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("div") - 1);
            String secondElement = command.substring(command.indexOf("div") + 4, command.length() - 1);
            commandStorage.addCommandToMath(result + " " + firstElement + " " + secondElement + " div");
        }
    }
}

public class Reducer {

    private int X1 = 0;
    private int X2 = 1;
    private int X3 = 2;
    private int X4 = 3;
    private int F = 4;

    public String DNF(int[][] array, int sizeI, int sizeJ) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < sizeI; i++) {
            if (array[i][F] == 1) {
                if (array[i][X1] == 1) {
                    result.append("(X1 *");
                } else {
                    result.append("(-X1 *");
                }
                if (array[i][X2] == 1) {
                    result.append(" X2 *");
                } else {
                    result.append(" -X2 *");
                }
                if (array[i][X3] == 1) {
                    result.append(" X3 *");
                } else {
                    result.append(" -X3 *");
                }
                if (array[i][X4] == 1) {
                    result.append(" X4) + ");
                } else {
                    result.append(" -X4) + ");
                }
                result.append("\n");
            }
        }

        if (!result.equals("")) {
            return result.delete(result.length() - 3, result.length()).toString();
        }
        return result.toString();
    }

    public String KNF(int[][] array, int sizeI, int sizeJ) {
        StringBuilder result = new StringBuilder();
        
        for (int i = 0; i < sizeI; i++) {
            if (array[i][F] == 0) {
                if (array[i][X1] == 1) {
                    result.append("(-X1 +");
                } else {
                    result.append("(X1 +");
                }
                if (array[i][X2] == 1) {
                    result.append(" -X2 +");
                } else {
                    result.append(" X2 +");
                }
                if (array[i][X3] == 1) {
                    result.append(" -X3 +");
                } else {
                    result.append(" X3 +");
                }
                if (array[i][X4] == 1) {
                    result.append(" -X4) * ");
                } else {
                    result.append(" X4) * ");
                }
                result.append("\n");

            }
        }

        if (!result.equals("")) {
            return result.delete(result.length() - 3, result.length()).toString();
        }
        return result.toString();
    }
}

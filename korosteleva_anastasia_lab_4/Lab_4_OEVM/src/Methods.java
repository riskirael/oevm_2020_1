public class Methods {
    public void printArray(int[][] array, int sizeI, int sizeJ) {
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.print("\n");
        }
    }

    public String DNF(int[][] array, int sizeI, int sizeJ) {
        StringBuilder result = new StringBuilder();

        int X1 = 0;
        int X2 = 1;
        int X3 = 2;
        int X4 = 3;
        int F = 4;

        for (int i = 0; i < sizeI; i++) {
            if (array[i][F] == 1) {
                if (array[i][X1] == 1) {
                    result.append("(X1 *");
                } else {
                    result.append("(-X1 *");
                }
                if (array[i][X2] == 1) {
                    result.append(" X2 *");
                } else {
                    result.append(" -X2 *");
                }
                if (array[i][X3] == 1) {
                    result.append(" X3 *");
                } else {
                    result.append(" -X3 *");
                }
                if (array[i][X4] == 1) {
                    result.append(" X4) + ");
                } else {
                    result.append(" -X4) + ");
                }
            }

        }

        if(result.toString() != ""){
            return result.delete(result.length()-3,result.length()).toString();
        }
        return result.toString();
    }

    public String KNF(int[][] array, int sizeI, int sizeJ) {
        StringBuilder result = new StringBuilder();

        int X1 = 0;
        int X2 = 1;
        int X3 = 2;
        int X4 = 3;
        int F = 4;

        for (int i = 0; i < sizeI; i++) {
            if (array[i][F] == 0) {
                if (array[i][X1] == 1) {
                    result.append("(-X1 +");
                } else {
                    result.append("(X1 +");
                }
                if (array[i][X2] == 1) {
                    result.append(" -X2 +");
                } else {
                    result.append(" X2 +");
                }
                if (array[i][X3] == 1) {
                    result.append(" -X3 +");
                } else {
                    result.append(" X3 +");
                }
                if (array[i][X4] == 1) {
                    result.append(" -X4) * ");
                } else {
                    result.append(" X4) * ");
                }
            }

        }

        if(result.toString() != ""){
            return result.delete(result.length()-3,result.length()).toString();
        }
        return result.toString();
    }
}

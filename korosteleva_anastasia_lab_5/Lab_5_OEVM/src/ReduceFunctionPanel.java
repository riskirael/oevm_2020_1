import javax.swing.*;
import java.awt.*;

public class ReduceFunctionPanel extends JPanel {

    private int[][] array;
    private boolean KNF = false;
    private boolean DNF = false;
    private int sizeI;
    private int sizeJ;
    private int indent = 10;
    private int sizeOfOneCell = 30;

    ReduceFunctionPanel(int sizeI, int sizeJ) {
        this.sizeI = sizeI;
        this.sizeJ = sizeJ;
    }

    public void paint(Graphics g) {
        super.paint(g);
        int line = 0;
        for (int i = 0; i < sizeI; i++) {
            if (DNF && array[i][sizeJ - 1] == 1) {
                for (int j = 0; j < sizeJ; j++) {
                    g.drawRect(indent + j * sizeOfOneCell, indent + line * sizeOfOneCell, sizeOfOneCell, sizeOfOneCell);
                    g.drawString(array[i][j] + "", indent * 2 + j * sizeOfOneCell, indent * 3 + line * sizeOfOneCell);
                }
                line++;
            }
            if (KNF && array[i][sizeJ - 1] == 0) {
                for (int j = 0; j < sizeJ; j++) {
                    g.drawRect(indent + j * sizeOfOneCell, indent + line * sizeOfOneCell, sizeOfOneCell, sizeOfOneCell);
                    g.drawString(array[i][j] + "", indent * 2 + j * sizeOfOneCell, indent * 3 + line * sizeOfOneCell);
                }
                line++;
            }
        }
    }

    public void setNewArray(int[][] array) {
        KNF = false;
        DNF = false;
        this.array = array;
    }

    public void drawDNF() {
        KNF = false;
        DNF = true;
    }

    public void drawKNF() {
        KNF = true;
        DNF = false;
    }
}

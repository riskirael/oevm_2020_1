import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        PascalReader reader = new PascalReader();
        AsmWriter writer = new AsmWriter();
        ParserAnalyzer parseString = new ParserAnalyzer(reader.read(), writer);

        parseString.parse();
        writer.createAsm();
    }
}

# Лабораторная работа №1. 
## Максим Раков ИСЭбд-21

### ***Комплектующие ПК:***

***Процессор*** Intel Core i3-9100F OEM - недорогой процессор третьего поколение, но и не слабый

***Материнская плата*** ASRock H310CM-HDV/M.2 - совмещается с сокетом

***Корпус*** Gigabyte C200 GLASS [GB-C200G] черный - корпус выбрал опираясь на фирму, Gigabyte очень хорошая фирма

***Видеокарта*** GIGABYTE GeForce GTX 1650 GAMING OC [GV-N1650GAMING OC-4GD] - больше брать смысла нет так как цена будет еще дороже , это самый оптимальный вариант, мог еще взять 1050TI но двойное охлаждение меня привлекло больше

***Кулер*** для процессора DEEPCOOL Ice Edge Mini FS V2.0 [DP-MCH2-IEMV2] - надеюсь процессор будет нормально работать с таким куллером))

***Оперативная память*** Kingston HyperX FURY Black [HX432C16FB3K2/8] 8 ГБ - у кингстон очень классные оперативки,  а 8 гб самый оптимальный выбор

***256 ГБ SSD-накопитель*** Kingston KC600 [SKC600/256G] - как и оперативка , ссд выбирал кингстон, на 512 много, на 128  мало, 256 оптимально

***2 ТБ Жесткий диск*** Seagate BarraCuda [ST2000DM008] - слышал о барракуде, взял оптимальный вариант на 2 ТБ

***Блок питания*** Deepcool DN 500W [DN500] - обычный блок питания на 500 вт, больше думаю и не нужно

package com.company;

import javax.swing.*;
import java.awt.*;

public class TableVisualization extends JPanel {
    private static String iteration;
    private final int[][] arrayForVisualization;

    public TableVisualization(int[][] array) {
        arrayForVisualization = array;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        HelperDraw.drawTable(arrayForVisualization, iteration, g);
    }

    public static void setIteration(String setIteration) {
        iteration = setIteration;
    }
}
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class Frame {

    private JTextFieldLimit textFieldCC;
    private JTextFieldLimit textFieldDigit1;
    private JTextFieldLimit textFieldDigit2;
    private JLabel labelCC;
    private JLabel labelDigit1;
    private JLabel labelDigit2;
    private JLabel labelComboBox;
    private JComboBox<String> comboBox;
    private JLabel labelPreview;
    private JLabel labelResult;
    private String operationString;
    private String result;

    public void initialize() {
        initFields();
        initLabels();
        initFrame();
    }

    private void initFrame() {
        JFrame frame = new JFrame("Калькулятор");
        frame.setSize(400, 400);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);

        frame.setLayout(null);
        frame.getContentPane().add(textFieldCC);
        frame.getContentPane().add(textFieldDigit1);
        frame.getContentPane().add(textFieldDigit2);
        frame.getContentPane().add(labelCC);
        frame.getContentPane().add(labelDigit1);
        frame.getContentPane().add(labelDigit2);
        frame.getContentPane().add(labelComboBox);
        frame.getContentPane().add(comboBox);
        frame.getContentPane().add(labelPreview);
        frame.getContentPane().add(labelResult);
    }

    private void initFields() {
        textFieldCC = new JTextFieldLimit(2, 1);
        textFieldDigit1 = new JTextFieldLimit(16, 2);
        textFieldDigit2 = new JTextFieldLimit(16, 2);
        comboBox = new JComboBox<>(new String[]{"+ Сложить", "- Вычесть", "* Умножить", "/ Разделить"});

        textFieldCC.setBounds(200, 30, 160, 25);
        textFieldDigit1.setBounds(200, 80, 160, 25);
        textFieldDigit2.setBounds(200, 130, 160, 25);
        comboBox.setBounds(200, 180, 160, 25);

        DocumentListener docListener = new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                setAnswer(textFieldCC.getText(), textFieldDigit1.getText(), textFieldDigit2.getText(), comboBox.getSelectedIndex());
            }

            public void removeUpdate(DocumentEvent e) {
                setAnswer(textFieldCC.getText(), textFieldDigit1.getText(), textFieldDigit2.getText(), comboBox.getSelectedIndex());
            }

            public void insertUpdate(DocumentEvent e) {
                setAnswer(textFieldCC.getText(), textFieldDigit1.getText(), textFieldDigit2.getText(), comboBox.getSelectedIndex());
            }
        };

        textFieldCC.getDocument().addDocumentListener(docListener);
        textFieldDigit1.getDocument().addDocumentListener(docListener);
        textFieldDigit2.getDocument().addDocumentListener(docListener);
        comboBox.addItemListener(e -> setAnswer(textFieldCC.getText(), textFieldDigit1.getText(), textFieldDigit2.getText(), comboBox.getSelectedIndex()));
    }

    private void initLabels() {
        labelCC = new JLabel("Введите исходную СС");
        labelDigit1 = new JLabel("Введите первое значение");
        labelDigit2 = new JLabel("Введите второе значение");
        labelComboBox = new JLabel("Выберите операцию");
        labelPreview = new JLabel("...");
        labelResult = new JLabel();

        labelCC.setBounds(20, 30, 170, 25);
        labelDigit1.setBounds(20, 80, 170, 25);
        labelDigit2.setBounds(20, 130, 170, 25);
        labelComboBox.setBounds(20, 180, 170, 25);

        labelPreview.setFont(new Font("Verdana", Font.PLAIN, 24));
        labelPreview.setBounds(0, 230, 380, 50);
        labelPreview.setHorizontalAlignment(JLabel.CENTER);

        labelResult.setFont(new Font("Verdana", Font.PLAIN, 32));
        labelResult.setBounds(0, 270, 380, 50);
        labelResult.setHorizontalAlignment(JLabel.CENTER);
    }

    private void setAnswer(String cc, String digit1, String digit2, int operation) {
        if (!cc.equals("") && !digit1.equals("") && !digit2.equals("")) {
            int ccInt = Integer.parseInt(cc);
            if (ccInt >= 2 && ccInt <= 16) {
                if (Converter.convert(ccInt, 2, digit1).equals("-1") || Converter.convert(ccInt, 2, digit2).equals("-1")) {
                    labelPreview.setText("Некорректные значения");
                    labelResult.setText("");
                }
                else if (Converter.convert(ccInt, 2, digit1).equals("out") || Converter.convert(ccInt, 2, digit2).equals("out")) {
                    labelPreview.setText("Большие значения (>255)");
                    labelResult.setText("");
                }
                else {
                    String digit1cc2 = Converter.convert(ccInt, 2, digit1);
                    String digit2cc2 = Converter.convert(ccInt, 2, digit2);
                    setOperationString(operation, digit1cc2, digit2cc2);
                    labelPreview.setText(digit1cc2 + operationString + digit2cc2);
                    labelResult.setText(result);
                }
            } else {
                labelPreview.setText("Некорректная СС");
                labelResult.setText("");
            }
        }
    }

    private void setOperationString(int operation, String digit1, String digit2) {
        switch (operation) {
            case 0 -> {
                operationString = " + ";
                result = Arithmetic.summarize(digit1, digit2);
            }
            case 1 -> {
                operationString = " - ";
                result = Arithmetic.subtract(digit1, digit2);
            }
            case 2 -> {
                operationString = " * ";
                result = Arithmetic.multiply(digit1, digit2);
            }
            case 3 -> {
                operationString = " / ";
                result = Arithmetic.divide(digit1, digit2);
            }
        }
    }
}


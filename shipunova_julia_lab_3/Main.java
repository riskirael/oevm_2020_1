import java.util.Scanner;

public class Main {
    private static int cc;
    private static String value1;
    private static String value2;
    private static String operation;

    public static void main(String[] args) {
        dataInput();
        basicLogic();
    }

    private static void dataInput ()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите исходную систему счисления: ");
        cc = scanner.nextInt();
        System.out.print("Введите первое значение: ");
        value1 = scanner.next();
        System.out.print("Введите второе значение: ");
        value2 = scanner.next();
        System.out.print("Введите символ операции: ");
        operation = scanner.next();
    }

    private static void basicLogic ()
    {
        int ccLowerBorder = 2;
        int ccBiggerBorder = 16;
        String problem = "Ошибка программы!!!";
        if (!(cc >= ccLowerBorder && cc <= ccBiggerBorder)) {
            System.out.println("Ошибка! Неправильная СС!!!");
        } else if (Parser.convert(cc, 2, value1).equals(problem) || Parser.convert(cc, 2, value2).equals(problem)) {
            System.out.println("Ошибка! Неправильные значения!!!");
        } else if (getResult(operation, value1, value2).equals(problem)) {
            System.out.println("ОШИБКА! НЕПРАВИЛЬНАЯ ОПЕРАЦИЯ!!!");
        } else {
            String value1CC = Parser.convert(cc, 2, value1);
            String value2CC = Parser.convert(cc, 2, value2);
            String result = getResult(operation, value1CC, value2CC);
            System.out.println(value1CC + " " + operation + " " + value2CC + " = " + result);
        }
    }

    private static String getResult(String operation, String value1, String value2) {
        String result;
        switch (operation) {
            case "+":
                result = MathCC.augment(value1, value2);
                return result;
            case "-":
                result = MathCC.subtract(value1, value2);
                return result;
            case "*":
                result = MathCC.multiply(value1, value2);
                return result;
            case "/":
                result = MathCC.division(value1, value2);
                return result;
            default:
                return "Ошибка программы!!!";
        }
    }
}

package com.nodj;

public class Converter {
    private int type;
    private final int[][] matrix = new int[16][5];
    private final int F = 5;
    private final int ROWS = 16;

    public Converter() {
        int container = 1;
        int divider = 8;
        for (int i = 0; i < F - 1; i++) {
            for (int j = 0; j < ROWS; j++) {
                if (j % divider == 0) {
                    container = container == 1 ? 0 : 1;
                }
                matrix[j][i] = container;
            }
            divider /= 2;
        }
        for (int j = 0; j < ROWS; j++) {
            matrix[j][F - 1] = (int) Math.round(Math.random());
        }
    }

    public void setType(int type) {
        this.type = type;
    }

    public String toStringMatrix() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < F; j++) {
                if (j == F - 1) {
                    stringBuilder.append("| ").append(matrix[i][j]);
                } else {
                    stringBuilder.append(matrix[i][j]).append(" ");
                }
            }
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }

    public String convert() {
        if (type == 1) {
            return showDNF();
        } else {
            return showKNF();
        }
    }

    private String showKNF() {
        boolean start = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ROWS; i++) {
            if (matrix[i][F - 1] == 0) {
                if (start) {
                    sb.append("(");
                    start = false;
                } else {
                    sb.append(" *")
                            .append('\n')
                            .append('(');
                }

                for (int j = 0; j < F - 1; j++) {
                    if (j == 0) {
                        if (matrix[i][j] == 1)
                            sb.append("-x").append(j);
                        else
                            sb.append("x").append(j);
                    } else {
                        if (matrix[i][j] == 1)
                            sb.append(" + -x").append(j);
                        else
                            sb.append(" + x").append(j);
                    }
                }
                sb.append(")");
            }

        }

        return sb.toString();
    }

    private String showDNF() {
        boolean start = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ROWS; i++) {
            if (matrix[i][F - 1] == 1) {
                if (start) {
                    start = false;
                } else {
                    sb.append(" +").append('\n');
                }

                for (int j = 0; j < F - 1; j++) {
                    if (j == 0) {
                        if (matrix[i][j] == 0)
                            sb.append("-x").append(j);
                        else
                            sb.append("x").append(j);
                    } else {
                        if (matrix[i][j] == 0)
                            sb.append(" -x").append(j);
                        else
                            sb.append(" x").append(j);
                    }
                }
            }

        }

        return sb.toString();
    }
}

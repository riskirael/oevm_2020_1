import java.awt.*;

public class DrawMethods {
    private static final int _rows = 16;
    private static final int _column = 5;

    private static int _posX = 10;
    private static int _posY = 10;
    private static int _cellWidth = 30;
    private static int _cellHeight = 30;

    public static void draw(int array[][], String operation, Graphics g) {
        for (int i = 0; i < _column - 1; i++) {
            g.drawRect(_posX + _cellWidth * i, _posY, _cellWidth, _cellHeight);
            g.drawString("X" + (i + 1), _posX * 2 + _cellWidth * i, _posY * 3);
        }

        g.drawRect(_posX + _cellWidth * (_column - 1), _posY, _cellWidth, _cellHeight);
        g.drawString("F", _posX + 10 + _cellWidth * (_column - 1), _posY + 20);

        _posY += 30;

        for (int i = 0; i < _rows; i++) {
            for (int j = 0; j < _column; j++) {
                g.drawRect(_posX + _cellWidth * j, _posY + _cellHeight * i, _cellWidth, _cellHeight);
                g.drawString(array[i][j] + "", _posX + 10 + _cellWidth * j, _posY + 20 + _cellHeight * i);
            }
        }

        if (MainForm.KNF) {
            drawKNF(array, g);
        }
        else if (MainForm.DNF) {
            drawDNF(array, g);
        }

        _posY -= 30;
    }

    private static void drawDNF(int[][] array, Graphics g) {
        for (int i = 0; i < _rows; i++) {
            if (array[i][_column - 1] == 1) {
                g.setColor(Color.GREEN);
            }
            else {
                g.setColor(Color.RED);
            }

            for (int j = 0; j < _column; j++) {
                g.drawRect(_posX + _cellWidth * j, _posY + _cellHeight * i, _cellWidth, _cellHeight);
                g.drawString(array[i][j] + "", _posX + 10 + _cellWidth * j, _posY + 20 + _cellHeight * i);
            }
        }
    }

    private static void drawKNF(int[][] array, Graphics g) {
        for (int i = 0; i < _rows; i++) {
            if (array[i][_column - 1] == 0) {
                g.setColor(Color.GREEN);
            }
            else {
                g.setColor(Color.RED);
            }

            for (int j = 0; j < _column; j++) {
                g.drawRect(_posX + _cellWidth * j, _posY + _cellHeight * i, _cellWidth, _cellHeight);
                g.drawString(array[i][j] + "", _posX + 10 + _cellWidth * j, _posY + 20 + _cellHeight * i);
            }
        }
    }
}
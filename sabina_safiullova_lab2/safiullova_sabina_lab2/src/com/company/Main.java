package com.company;

import java.util.Scanner;

public class Main {
    public static final int OFFSET = 10;

    public static void main(String[] args) {
        int initial, next;
        Scanner in = new Scanner(System.in);

        System.out.print("Input the original system: ");
        initial = in.nextInt();
        System.out.print("Input the base of the new number system: ");
        next = in.nextInt();

        Scanner in1 = new Scanner(System.in);
        System.out.print("Input the number: ");
        String num = in1.nextLine();
        char[] number = num.toCharArray();

        Correctness.correctBase(initial, next); // Проверка на систему счисления
        Correctness.correctNumber(initial, number); //Проверка на принадлежность числа к СС
        convertToDecimal(initial, number, next); //Перевод в десятичную систему
    }

    private static void convertToDecimal(int initial, char[] number, int next) {
        long decimal =0;
        int length= number.length;

        if (initial != 10) {
            for (int i = 0; i < length; i++) {
                char digit = Character.toUpperCase(number[i]);

                if (Character.isLetter(digit)) {
                    decimal += (digit - 'A' + OFFSET) * (Math.pow(initial, length - 1 - i));
                } else if (Character.isDigit(digit)) {
                    decimal += (number[i] - '0') * (Math.pow(initial, length - 1 - i));
                } else {
                    System.exit(1);
                }
            }
        }
        convert( next, decimal);
    }
    private static void convert( int next, long decimal) {
        long divide= decimal;
        int quantity=1;

        while (Math.pow(next,quantity) <= decimal){
            quantity++;
        }

        int [] newNum = new int[quantity];
        for (int k=0; k<quantity; k++){
            if(divide >= next){
                newNum[k]= (int)(divide%(long)next);
                divide= divide/next;
            }
        }
        newNum[quantity-1]=(int)divide%next;
        char[] newNumber = new char[quantity];

        for (int q=quantity-1; q>=0; q--) {
            if (newNum[q] < 10) {
                int n = newNum[q];
                newNumber[quantity - q-1] = Character.forDigit(n, 10);
            } else if (newNum[q] >= 10) {
                newNumber[quantity - q-1] = (char) ('A' + newNum[q] - OFFSET);
            }
        }

        for(int p=0; p< newNumber.length; p++){
            System.out.print(newNumber[p]);
        }
    }
}

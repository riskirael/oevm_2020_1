
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        init();
        getAnswer();
    }

    private static int ss1, ss2;
    private static String value;

    private static void init()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите исходную систему счисления: ");
        ss1 = scanner.nextInt();
        System.out.println("Введите систему счисления, в которую хотите перевести: ");
        ss2 = scanner.nextInt();
        System.out.println("Введите число: ");
        value = scanner.next();
    }

    private static void getAnswer()
    {
        int ssLowLimit = 2, ssHeightLimit = 16;
        if ((ss1 >= ssLowLimit && ss1 <= ssHeightLimit) && (ss2 >= ssLowLimit && ss2 <= ssHeightLimit)) {
            System.out.println("Ответ: " + convert(ss1, ss2, value));
        } else {
            System.out.println("Пожалуйста введите другую систему счисления!!!");
        }
    }

    private static String convert(int ss1, int ss2, String value) {
        int ss10 = 0; //значение в 10 сс
        int fromAsciiToDigit = 48;
        int fromAsciiToLetter = 55;

        String problem = "Ошибка программы";

        char[] valueString = value.toCharArray(); // переводим int в массив символов
        for (int i = 0; i < valueString.length; i++) { // проходим по каждому символу
            if (Character.isDigit(valueString[i])) { // проверяем является ли символ цифрой
                if (valueString[i] - fromAsciiToDigit < ss1 && valueString[i] - fromAsciiToDigit >= 0) {
                    // если символ подходит под исходную сс, преобразуем его из АSCII
                    ss10 += (valueString[i] - fromAsciiToDigit) * Math.pow(ss1, valueString.length - 1 - i);
                } else {
                    return problem;
                }
            } else if (Character.isLetter(valueString[i])) {
                if (valueString[i] - fromAsciiToLetter < ss1 && valueString[i] - fromAsciiToLetter >= 0) {
                    // если символ подходит под исходную сс, преобразуем его из АSCII
                    ss10 += (valueString[i] - fromAsciiToLetter) * Math.pow(ss1, valueString.length - 1 - i);
                } else {
                    return problem;
                }
            } else {
                return problem;
            }
        }

        StringBuilder valueSs2 = new StringBuilder(); //значение в нужной сс
        if (ss10 == 0) {
            return "0";
        } else {
            while (ss10 > 0) { // преобразуем в нужную сс
                if (Character.isDigit(ss10 % ss2 + fromAsciiToDigit)) {
                    valueSs2.append((char) (ss10 % ss2 + fromAsciiToDigit));
                } else {
                    valueSs2.append((char) (ss10 % ss2 + fromAsciiToLetter));
                }
                ss10 /= ss2;
            }
            String answer = valueSs2.reverse().toString();
            return answer; //отражаем строчку и возвращаем её
        }
    }
}

public class Solution {
    int buffer = 0;
    int err = 0;

    public void transferToThe10Digit(int start_system, String number) {
        char[] Array = number.toCharArray();
        for (int i = Array.length - 1; i >= 0; i--) {
            double pow = Math.pow(start_system, Math.abs(i - Array.length + 1));
            if (Character.isDigit(Array[i])) {
                if ((int) Array[i] - 48 > start_system) {
                    System.out.println("цифра не подходит данной с.с.");
                    err++;
                    return;
                }
                buffer += ((int) Array[i] - 48) * pow;
            } else {
                if (start_system < 10) {
                    System.out.println("неверное значение");
                    err++;
                    return;
                }
                if (Array[i] >= 'A' && Array[i] <= 'F') {
                    buffer += (10 + Array[i] - 'A') * pow;
                }
            }
        }
    }

    public void endTransfer(int finish_system, StringBuffer answer) {
        while (buffer >= finish_system) {
            if (buffer % finish_system >= 10) {
                switch (buffer % finish_system) {
                    case 10:
                        answer.append("A");
                        break;
                    case 11:
                        answer.append("B");
                        break;
                    case 12:
                        answer.append("C");
                        break;
                    case 13:
                        answer.append("D");
                        break;
                    case 14:
                        answer.append("E");
                        break;
                    case 15:
                        answer.append("F");
                        break;
                }
            } else {
                answer.append(buffer % finish_system);

            }
            buffer /= finish_system;
        }
        if (buffer >= 10) {
            switch (buffer) {
                case 10:
                    answer.append("A");
                    break;
                case 11:
                    answer.append("B");
                    break;
                case 12:
                    answer.append("C");
                    break;
                case 13:
                    answer.append("D");
                    break;
                case 14:
                    answer.append("E");
                    break;
                case 15:
                    answer.append("F");
                    break;
            }
        } else {
            answer.append(buffer);
        }
        answer.reverse();
        System.out.println(answer);
    }
}


package com.company;

public class TruthTable {
    private int N = 16;
    private int M = 5;
    public int[][] truthTable;
    private String[] x = {"x1", "x2", "x3", "x4"};

    public char[] convertToBinaryNotation(int number) {
        String result = "";
        int remainder = 1;
        int binaryNotation = 2;
        String currentSymbol = "";
        while (number > 0) {
            remainder = number % binaryNotation;
            currentSymbol = "";
            if (number % binaryNotation < 10) {
                currentSymbol = Integer.toString(remainder);
            } else {
                currentSymbol = currentSymbol + (char) ((int) ('A' + remainder - 10));
            }
            result = currentSymbol + result;
            number /= binaryNotation;
        }
        int countOfSymbols = 4;
        while (result.length() < countOfSymbols) {
            result = '0' + result;
        }
        return result.toCharArray();
    }

    public TruthTable() {
        int[][] truthTable = new int[N][M];
        for (int i = 0; i < N; ++i) {
            char[] strX = convertToBinaryNotation(i);
            for (int j = 0; j < M - 1; ++j) {
                truthTable[i][j] = strX[j] - '0';
            }
            truthTable[i][M - 1] = (int) (Math.random() * 2);
        }
        this.truthTable = truthTable;
    }

    public String toDisjunctiveNormalForm(int[][] generatedArray) {
        String result = new String();
        boolean startDNF = true;
        for (int i = 0; i < N; ++i) {
            if (generatedArray[i][M - 1] == 1) {
                if (!startDNF) {
                    result += " +\n";
                }
                result += "(";
                startDNF = false;
                for (int j = 0; j < M - 1; ++j) {
                    if (generatedArray[i][j] == 0) {
                        result += "!";
                    }
                    result += x[j];
                    if (j < M - 2) {
                        result += " * ";
                    } else {
                        result += ")";
                    }
                }
            }
        }
        return result;
    }

    public String toConjunctiveNormalForm(int[][] generatedArray) {
        String result = new String();
        boolean startCNF = true;
        for (int i = 0; i < N; ++i) {
            if (generatedArray[i][M - 1] == 0) {
                if (!startCNF) {
                    result += " *\n";
                }
                result += "(";
                startCNF = false;
                for (int j = 0; j < M - 1; ++j) {
                    if (generatedArray[i][j] == 1) {
                        result += "!";
                    }
                    result += x[j];
                    if (j < M - 2) {
                        result += " + ";
                    } else {
                        result += ")";
                    }
                }
            }
        }
        return result;
    }
}



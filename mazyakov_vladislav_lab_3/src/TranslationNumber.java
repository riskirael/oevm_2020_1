public class TranslationNumber {

    public int transferToTheDecimal(int start_system, String number) {
        boolean minus = false;
        int answer = 0;
        char[] Array = number.toCharArray();
        if (Array[0]=='-') {
            minus=true;
        }
        for (int i = Array.length - 1; i >= 0; i--) {
            double pow = Math.pow(start_system, Math.abs(i - Array.length + 1));
            if (Character.isDigit(Array[i])) {
                answer += ((int) Array[i] - 48) * pow;
            } else {
                if (Array[i] >= 'A' && Array[i] <= 'F') {
                    answer += (10 + Array[i] - 'A') * pow;
                }
            }
        }
        if (minus) {
            answer=answer*-1;
        }
        return answer;
    }

    public StringBuilder transferToTheBinary(int number) {
        boolean minus=(number<0);
        if (minus) {
            number*=-1;
        }
        int finish_system = 2;
        StringBuilder answer = new StringBuilder();
        while (number >= finish_system) {
            answer.append(number % finish_system);
            number /= finish_system;
        }
        answer.append(number);
        answer.reverse();
        if (minus) {
            answer.insert(0,'-');
        }
        return answer;
    }
}


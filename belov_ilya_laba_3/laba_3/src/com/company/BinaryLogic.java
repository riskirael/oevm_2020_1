package com.company;

public class BinaryLogic {
    public static String add (char[] firstTerm, char[] secondTerm) {
        String result = "";
        String curChar = "0";
        // 0 пишем 1 в уме)
        int remember = 0;

        int j = secondTerm.length - 1;
        for (int i = firstTerm.length - 1; i >= 0 || j >= 0 || remember == 1; --i, --j) {
            int first1, second1;

            if (i < 0) {
                first1 = 0;
            }
            else if (firstTerm[i] == '1') {
                first1 = 1;
            } else {
                // если 1 то 1, плюс заполняем
                first1 = 0;
            }

            if (j < 0) {
                // незначащами нулями
                second1 = 0;
            }
            else if (secondTerm[j] == '1') {
                second1 = 1;
            } else {
                second1 = 0;
            }

            int curSum = first1 + second1 + remember;
            // выбираем, что заисать и что запомнить
            if (curSum == 3) {
                curChar = "1";
                remember = 1;
            } else if (curSum == 2) {
                curChar = "0";
                remember = 1;
            } else if (curSum == 1) {
                curChar = "1";
                remember = 0;
            } else {
                curChar = "0";
                remember = 0;
            }
            result = curChar + result;
        }
        return result;
    }
    /**
    * функция вычитания, в аргументах вычитаемое и уменьшаемое
    */
    public static String subtract(char[] subtrahend, char[] minuend) {
        String result = "";

        int cnt = subtrahend.length - minuend.length;
        char[] minuendAdditional = new char[subtrahend.length];
        // обратный код
        for (int i = 0; i < cnt; ++i) {
            // незначимые нулю = 1
            minuendAdditional[i] = '1';
        }
        for (int i = cnt; i < minuendAdditional.length; ++i) {
            if (minuend[i - cnt] == '1') {
                // вместо едениц нули
                minuendAdditional[i] = '0';
            }
            else {
                // ну и наоборот
                minuendAdditional[i] = '1';
            }
        }

        char[] one = {'1'};
        // дополнительный код
        minuendAdditional = add(minuendAdditional, one).toCharArray();
        // складываем
        char[] dopSum = add(minuendAdditional, subtrahend).toCharArray();

        if (dopSum.length > 1) {
            // незначащие нули
            boolean insignificantZeros = true;
            // пропускаем первый разряд и незначащие нули
            for (int i = 1; i < dopSum.length; ++i) {
                if (dopSum[i] == '1') {
                    insignificantZeros = false;
                }
                else {
                    if (insignificantZeros) {
                        continue;
                    }
                }
                result += dopSum[i];
            }
        }
        return result;
    }

    public static String multiply (char[] firstFactor, char[] secondFactor) {
        // складываем вот столько раз
        long cnt = Helper.convertTo10(secondFactor, 2);

        char[] dopFactor = firstFactor;
        for (int i = 0; i < cnt - 1; ++i) {
            firstFactor = (add(firstFactor, dopFactor)).toCharArray();
        }
        String result = new String(firstFactor);
        return result;
    }
    /**
    * функция деления, в аргументах делимое, делитель
    */
    public static String divide (char[] dividend, char[] divider) {
        long cnt = 0;
        while (dividend.length > 0) { // вычитаем пока не дойдём до нуля
            dividend = (subtract(dividend, divider)).toCharArray();
            cnt++;
        }
        String result = Helper.convertTo2(cnt);
        return result;
    }
}

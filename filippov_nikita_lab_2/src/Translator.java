import static java.lang.Math.pow;

public class Translator {
    private StringBuilder result = new StringBuilder();
    private long decimalSystem = 0;
    private int startSystem;
    private int endSystem;
    private String number;

    /**
     * Конструктор, принимающий введенные данные и вызывающий метод перевода в 10с.с
     */
    public Translator(int start, int end, String enter) {
        this.startSystem = start;
        this.endSystem = end;
        this.number = enter;
        translateTo10();
    }

    /**
     * Перевод в 10с.с
     */
    private void translateTo10() {
        for (int i = 0; i < number.length(); i++) {
            if (Character.isLetter(number.charAt(i))) {
                if (number.charAt(i) - 55 < startSystem) {
                    decimalSystem += (number.charAt(i) - 55) * pow(startSystem, number.length() - 1 - i);
                } else {
                    return;
                }
            } else {
                if (Character.getNumericValue(number.charAt(i)) < startSystem) {
                    decimalSystem += (number.charAt(i) - 48) * pow(startSystem, number.length() - 1 - i);
                } else {
                    return;
                }
            }
        }
    }

    /**
     * Перевод из 10с.с в конечную
     */
    public void translateToEnd() {
        if (decimalSystem < endSystem) // Если результат меньше конечной системы счисления
        {
            if (decimalSystem < 10) {
                this.result = this.result.append(decimalSystem);
            } else {
                this.result = this.result.append((char) (decimalSystem + 55));
            }
        } else {
            while (decimalSystem / endSystem != 0) {
                if (decimalSystem % endSystem < 10) {
                    this.result.append(decimalSystem % endSystem);
                } else {
                    this.result.append((char) (decimalSystem % endSystem + 55));
                }
                decimalSystem /= endSystem;
            }
            if (decimalSystem < 10) {
                this.result.append(decimalSystem);
            } else {
                this.result.append((char) (decimalSystem + 55));
            }
        }
    }

    /**
     * вывод результата
     */
    public String getResult() {
        if (this.result.length() == 0) {
            this.result.replace(0, result.length(), "Ошибка ввода");
        } else {
            this.result.reverse();
        }
        return this.result.toString();
    }
}

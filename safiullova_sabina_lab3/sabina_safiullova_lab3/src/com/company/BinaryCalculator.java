package com.company;

import java.util.ArrayList;

public class BinaryCalculator {
    public static ArrayList<Long> add(long firstNum, long secondNum) {
        long carry = 0;
        ArrayList<Long> arrayListForAdd = new ArrayList<>();

        while (firstNum != 0 || secondNum != 0) {
            arrayListForAdd.add((firstNum % 10 + secondNum % 10 + carry) % 2);

            carry = ((firstNum % 10 + secondNum % 10 + carry) / 2);

            firstNum = firstNum / 10;
            secondNum = secondNum / 10;
        }

        if (carry != 0) {
            arrayListForAdd.add(carry);
        }

        return arrayListForAdd;
    }
    public static ArrayList<Long> subtract(long firstNum, long secondNum) {
        long loan = 0;
        ArrayList<Long> arrayListForSub = new ArrayList<>();

        if (firstNum < secondNum) {
            System.out.print("Calculator doesn't support negative numbers:(");
        }

        while (firstNum != 0 || secondNum != 0) {
            if (firstNum % 10 >=  secondNum % 10) {
                arrayListForSub.add(Math.abs((firstNum % 10 - secondNum % 10 - loan) % 2 ));

                if (firstNum % 10 ==  secondNum % 10 && loan ==1) {
                    loan = 1;
                } else {
                    loan = 0;
                }

            } else {
                arrayListForSub.add(Math.abs((secondNum % 10 - firstNum % 10 - loan) % 2));
                loan = 1;
            }

            firstNum = firstNum / 10;
            secondNum = secondNum / 10;
        }

        return arrayListForSub;
    }
    public static void print(ArrayList<Long> arrayListForPrint) {
        boolean zero = true;

        for (int i = arrayListForPrint.size()-1; i >= 0; i--) {

            if (arrayListForPrint.get(i) == 1) {
                zero = false;
            }

            if (!zero) {
                System.out.print(arrayListForPrint.get(i));
            }
        }
    }
}

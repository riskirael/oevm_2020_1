## Лабораторная работа №8 "Физические компоненты ЭВМ"

Выполнил студент группы **ИСЭбд-21 Александров Макар**

### Задание:

*Разобрать и собрать ПК*

**[До разборки](https://www.dropbox.com/s/6v73rqkic8kgpoj/%D0%B4%D0%BE.jpg?dl=0)**

**[Комлектующие](https://www.dropbox.com/s/2ikkwkk9og3xprx/%D0%BA%D0%BE%D0%BC%D0%BF%D0%BB%D0%B5%D0%BA%D1%82%D1%83%D1%8E%D1%89%D0%B8%D0%B5.jpg?dl=0)**
Не стал снимать кулер с материнской платы, так как пару месяцев назад менял термопасту.


**[После сборки](https://www.dropbox.com/s/pwisyugosfl2xki/%D0%BF%D0%BE%D1%81%D0%BB%D0%B5.jpg?dl=0)**

**[ПК выжил после разборки и сборки](https://www.dropbox.com/s/1j8yeg4gcww1v9h/%D0%BF%D0%BE%D1%81%D0%BB%D0%B5%20%D1%81%D0%B1%D0%BE%D1%80%D0%BA%D0%B8.jpg?dl=0)**

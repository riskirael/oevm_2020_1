public class Convertor
{
    private final char[] CODECHAR = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private final int[] CODEINT = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    private int _originalSON;
    private int _newSON;
    private String _num;

    public Convertor(int originalSON, int newSON, String num) {
        _originalSON = originalSON;
        _newSON = newSON;
        _num = num;
    }

    private int convertToDecimal() {
        char[] valueCharArray = _num.toCharArray();
        int value = 0;

        for (int i = 0; i < valueCharArray.length; i++) {
            int power = valueCharArray.length - i - 1;

            for (int j = 0; j < _originalSON; j++) {
                if (valueCharArray[i] == CODECHAR[j]) {
                    value += CODEINT[j] * (int) Math.pow(_originalSON, power);
                    break;
                }
            }
        }
        return value;
    }

    public String convert() {
        int value = convertToDecimal();
        int ranks = 0;

        while (value >= _newSON) {
            ranks++;
            value /= _newSON;
        }
        ranks++;

        char[] Result = new char[ranks];
        value = convertToDecimal();

        for (int i = ranks - 1; i > -1; i--) {
            for (int j = 0; j < _newSON; j++) {
                if (value % _newSON == CODEINT[j]) {
                    Result[i] = CODECHAR[j];
                    value /= _newSON;
                    break;
                }
            }
        }
        return new String(Result);
    }
}
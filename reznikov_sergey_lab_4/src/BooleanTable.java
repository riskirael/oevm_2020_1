import java.util.Random;

public class BooleanTable {

    char[][] table;
    public final int ELEMENTS_NUMBER = 4;
    final private char[] words = {'a', 'b', 'c', 'd', 'e', 'f', 'g'};

    public BooleanTable() {
        table = new char[(int) Math.pow(2, ELEMENTS_NUMBER)][];

        for (int i = 0; i < table.length; i++) {
            table[i] = new char[ELEMENTS_NUMBER + 1];
            for (int k = 0; k < ELEMENTS_NUMBER + 1; k++) {
                String tmp = "" + (int) (Math.random() * 2);
                table[i][k] = tmp.toCharArray()[0];
            }
        }
    }

    public char[][] getTable() {
        return table;
    }

    public String getKForm() {
        StringBuffer str = new StringBuffer("");
        str.append("f(");
        for(int i=0;i<ELEMENTS_NUMBER;i++){
            str.append(words[i] + ", ");
        }
        str.delete(str.length()-2,str.length());
        str.append(") = ");

        for (int i = 0; i < table.length; i++) {
            if (table[i][ELEMENTS_NUMBER] == '0') {
                str.append("(");
                for (int k = 0; k < ELEMENTS_NUMBER; k++) {
                    if (table[i][k] == '1') {
                        str.append("-" + words[k]);
                    } else {
                        str.append(words[k]);
                    }
                    if (k != ELEMENTS_NUMBER - 1) str.append(" + ");
                }
                str.append(") * ");
            }
        }

        str.delete(str.length()-3,str.length()-1);
        return str.toString();
    }

    public String getDForm() {
        StringBuffer str = new StringBuffer("");
        str.append("f(");
        for(int i=0;i<ELEMENTS_NUMBER;i++){
            str.append(words[i] + ", ");
        }
        str.delete(str.length()-2,str.length());
        str.append(") = ");

        for (int i = 0; i < table.length; i++) {
            if (table[i][ELEMENTS_NUMBER] == '1') {
                str.append("(");
                for (int k = 0; k < ELEMENTS_NUMBER; k++) {
                    if (table[i][k] == '0') {
                        str.append("-" + words[k]);
                    } else {
                        str.append(words[k]);
                    }
                    if (k != ELEMENTS_NUMBER - 1) str.append(" * ");
                }
                str.append(") + ");
            }
        }

        str.delete(str.length()-3,str.length()-1);
        return str.toString();
    }

}

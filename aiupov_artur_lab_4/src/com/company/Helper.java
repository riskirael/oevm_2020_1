package com.company;
import java.lang.StringBuffer;

public class Helper {

    static final int COLUMN = 15; // длина столбца
    static final int SYMBOL = 4; // длина строки
    static final int X1 = 0; // индекс первого икса
    static final int X2 = 1; // индекс второго икса
    static final int X3 = 2; // индекс третьего икса
    static final int X4 = 3; // индекс четвёртого икса

    public static String convert(int finalNumber, String notFinalResult) { // перевод в двоичную систему
        while(((finalNumber / 2 != 0)) || (finalNumber % 2 != 0)) { // пока остаток не ноль делаю рил ток вещи
            notFinalResult += finalNumber % 2; // строка к которой на каждой
            // итерации прибавляется остаток от деления
            finalNumber = finalNumber / 2; // делю нацело, чтобы цикл не заклинило
        }
        notFinalResult = new StringBuffer(notFinalResult).reverse().toString(); // переворачиваю строку потому что ничего не работает
        while(notFinalResult.length() < SYMBOL){ // если длина строки меньше 4 прибавляю нули в начало
            notFinalResult = "0" + notFinalResult;
        }
        return notFinalResult;
    }

    public static void outputOnDisplay(int[][] arrayOfNumbers) {
        String line = ""; // нужная строка для вывода
        char oneSymbolOfLine = ' '; // нужная символ, который буду выводить
        int randNumber = 0; // последнее случайное число в строке

        for (int i = 0; i <= COLUMN; i++) { // проходим по всем строкам
            line = "";
            line = convert(i, line);
            for (int j = 0; j < SYMBOL; j++) {
                oneSymbolOfLine = line.charAt(j); // беру один символ, а потом вывожу его
                if (oneSymbolOfLine == '1') { // в зависимости од значения вношу в массив 1 или 0
                    arrayOfNumbers[i][j] = 1;
                } else {
                    arrayOfNumbers[i][j] = 0;
                }
                System.out.print(" " + oneSymbolOfLine + " ");
            }
            randNumber = (int)(Math.random()*2); // рандомлю число от 0 до 1
            arrayOfNumbers[i][SYMBOL] = randNumber; // присваиваем значение числа массиву
            System.out.println("= " + randNumber);
        }
    }

    public static void knfFormula(int[][] arrayOfNumbers) {
        char signX1 = ' '; // знак первого икса
        char signX2 = ' '; // знак второго икса
        char signX3 = ' '; // знак третьего икса
        char signX4 = ' '; // знак четвертого икса
        boolean firstIteration = true; // проверкан на первую итерацию

        for (int i = 0; i <= COLUMN; i++) {
            if (arrayOfNumbers[i][SYMBOL] == 0) { // проверка последней цифры в строке
                if (arrayOfNumbers[i][X1] == 0) { // проверка на знак первого икса
                    signX1 = '-';
                }
                if (arrayOfNumbers[i][X2] == 0) { // проверка на знак второго икса
                    signX2 = '-';
                }
                if (arrayOfNumbers[i][X3] == 0) { // проверка на знак третьего икса
                    signX3 = '-';
                }
                if (arrayOfNumbers[i][X4] == 0) { // проверка на знак четвёртого икса
                    signX4 = '-';
                }
                if (firstIteration) { // проверка на первую итерацию
                    System.out.print("( " + signX1 + "X1 + " + signX2 + "X2 + " + signX3 + "X3 + " + signX4 + "X4 )");
                    firstIteration = false;
                } else {
                    System.out.print(" * ( " + signX1 + "X1 * " + signX2 + "X2 * " + signX3 + "X3 * " + signX4 + "X4 )");
                }
            }
        }
    }

    public static void dnfFormula(int[][] arrayOfNumbers) {
        char signX1 = ' '; // знак первого икса
        char signX2 = ' '; // знак второго икса
        char signX3 = ' '; // знак третьего икса
        char signX4 = ' '; // знак четвёртого икса
        boolean firstIteration = true; // проверкан на первую итерацию

        for (int i = 0; i <= COLUMN; i++) {
            if (arrayOfNumbers[i][SYMBOL] == 1) {  // проверка последней цифры в строке
                if (arrayOfNumbers[i][X1] == 0) { // проверка на знак первого икса
                    signX1 = '-';
                }
                if (arrayOfNumbers[i][X2] == 0) { // проверка на знак второго икса
                    signX2 = '-';
                }
                if (arrayOfNumbers[i][X3] == 0) { // проверка на знак третьего икса
                    signX3 = '-';
                }
                if (arrayOfNumbers[i][X4] == 0) { // проверка на знак четвёртого икса
                    signX4 = '-';
                }
                if (firstIteration) { // проверка на первую итерацию
                    System.out.print("( " + signX1 + "X1 * " + signX2 + "X2 * " + signX3 + "X3 * " + signX4 + "X4 )");
                    firstIteration = false;
                } else {
                    System.out.print(" + ( " + signX1 + "X1 * " + signX2 + "X2 * " + signX3 + "X3 * " + signX4 + "X4 )");
                }
            }
        }
    }

}

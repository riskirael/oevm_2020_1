package com.company;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.Properties;
import java.awt.Color;

public class Main {
    Drawing method = new Drawing();
    StringBuilder strBuilder = new StringBuilder();

    static boolean _DNF = false;
    static boolean _KNF = false;

    static Property prop = new Property();

    final static int N = prop.getPropertyInt("N");
    final static int M = prop.getPropertyInt("M");

    public static int[][] truthTable = new int[N][M]; // таблица истинности

    public static void main(String[] args) {

        truthTable = Helper.fillTruthTable(N, M, truthTable); // заполняем таблицу истинности

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main window = new Main();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
    private JPanel MyPanel = new MyPanel(truthTable);
    private JFrame frame;
    private JButton btnDNF = new JButton("DNF");
    private JButton btnKNF = new JButton("KNF");
    private JTextArea txtFunction = new JTextArea();

    public Main() {
        printOnMyPanel();
    }

    private void printOnMyPanel() {
        frame = new JFrame();
        frame.setBounds(100, 100, 700, 720);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        MyPanel.setBounds(0, 0, 270, 720);
        frame.getContentPane().add(MyPanel);

        txtFunction.setEditable(false);
        txtFunction.setBounds(300, 10, 350, 720);
        frame.getContentPane().add(txtFunction);

        btnDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnKNF.setEnabled(false);
                btnDNF.setEnabled(false);
                _DNF = true;
                txtFunction.setText(strBuilder.append(method.convertToDNF(truthTable, N, M)).toString());
                MyPanel.repaint();
            }
        });
        btnDNF.setBounds(200, 10, 90, 30);
        frame.getContentPane().add(btnDNF);


        btnKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnKNF.setEnabled(false);
                btnDNF.setEnabled(false);
                _KNF = true;
                txtFunction.setText(strBuilder.append(method.convertToKNF(truthTable, N, M)).toString());
                MyPanel.repaint();
            }
        });
        btnKNF.setBounds(200, 80, 90, 30);
        frame.getContentPane().add(btnKNF);
    }
}

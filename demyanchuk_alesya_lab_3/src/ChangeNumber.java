public class ChangeNumber {
    /**
     * метод преобразует число в 10 СС ( умножаем на основание в степени с конца)
     */
    static int transferToTenDegree(int sourceSystem, String number) {
        number = number.toUpperCase();
        String[] mass = number.split( "" );
        for (int i = 0; i < mass.length; i++) {
            //преобразуем символы в числа
            switch (mass[i]) {
                case "A": {
                    if (sourceSystem >= 11)
                        mass[i] = String.valueOf( 10 );
                    else {
                        new Exception( "Ошибка ввода, данный символ отсутсвует в этой СС" );
                    }
                    break;
                }
                case "B": {
                    if (sourceSystem >= 12)
                        mass[i] = String.valueOf( 11 );
                    else {
                        new Exception( "Ошибка ввода, данный символ отсутсвует в этой СС" );
                    }
                    break;
                }
                case "C": {
                    if (sourceSystem >= 13)
                        mass[i] = String.valueOf( 12 );
                    else {
                        new Exception( "Ошибка ввода, данный символ отсутсвует в этой СС" );
                    }
                    break;
                }
                case "D": {
                    if (sourceSystem >= 14)
                        mass[i] = String.valueOf( 13 );
                    else {
                        new Exception( "Ошибка ввода, данный символ отсутсвует в этой СС" );
                    }
                    break;
                }
                case "E": {
                    if (sourceSystem >= 15)
                        mass[i] = String.valueOf( 14 );
                    else {
                        new Exception( "Ошибка ввода, данный символ отсутсвует в этой СС" );
                    }
                    break;
                }
                case "F": {
                    if (sourceSystem == 16)
                        mass[i] = String.valueOf( 15 );
                    else {
                        new Exception( "Ошибка ввода, данный символ отсутсвует в этой СС" );
                    }
                    break;
                }
                default: {
                    break;
                }
            }

        }
        int newNumber = 0;
        int m = 0;
        for (int i = mass.length - 1; i >= 0; i--) {
            int n = Integer.parseInt( mass[i] );
            newNumber = (int) (newNumber + (n * Math.pow( sourceSystem, m )));
            m++;
        }
        return newNumber;
    }

    /**
     * метод преобразует число из любой СС в любую СС: преобразуем в десятичную потом в ту что нужно делением в столбик
     */
    static String transferToTwoDegree(int sourceSystem, String number) {
        String newNumber = "";
        int SystemEnd = 2;
        int n = 0;
        int[] mass = new int[10000000];
        if (sourceSystem != 10) {
            number = String.valueOf( transferToTenDegree( sourceSystem, number ) );
        }
        int i = 0;
        int newNumb = Integer.parseInt( number );
        while (newNumb >= SystemEnd) {
            mass[i] = newNumb % SystemEnd;
            newNumb = newNumb / SystemEnd;
            n++;
            i++;
        }
        mass[i] = newNumb;
        for (i = n; i >= 0; i--) {
            newNumber += mass[i];
        }
        return newNumber;
    }
}

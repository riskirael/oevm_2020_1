package com.company;

public class TruthTable {

    private final int countOfRows;
    private final int countOfColumns;
    public int[][] truthTable;

    public int[][] getTruthTable() { return truthTable; }

    public TruthTable(int countOfRows, int countOfColumns){
        this.countOfRows = countOfRows;
        this.countOfColumns = countOfColumns;
        truthTable = new int[countOfRows][countOfColumns];
    }

    /**
     * Этот метод вернет число в двоичной системе счисления дополненный спереди нулями до 4-разрядного
     */
    public void fillTruthTable() {
        for (int i = 0; i < countOfRows; ++i) {
            char[] iterativeString = Converter.convertToBinary(i).toCharArray();
            for (int j = 0; j < countOfColumns - 1; ++j) { truthTable[i][j] = iterativeString[j] - '0'; }
            truthTable[i][countOfColumns - 1] = (int) (Math.random() * 2);
        }
    }

    /**
     * Этот метод произведет вывод в консоль таблицы истинности
     */
    public void printTruthTable() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("| X1| X2| X3| X4| F |\n");
        for (int i = 0; i < countOfRows; ++i) {
            stringBuilder.append("|");
            for (int j = 0; j < countOfColumns - 1; ++j) {
                stringBuilder.append(" ").append(truthTable[i][j]).append(" |");
            }
            stringBuilder.append(" ").append(truthTable[i][countOfColumns - 1]).append(" |\n");
        }
        System.out.print(stringBuilder);
    }
}

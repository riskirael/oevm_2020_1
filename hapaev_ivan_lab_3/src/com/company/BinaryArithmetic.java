package com.company;

public class BinaryArithmetic {

    public static String add(String firstNumberBin, String secondNumberBin){
        String result = "";
        String currentDigit = "0";
        int digitInMind = 0;
        int firstNumLen = firstNumberBin.length() - 1;
        int secondNumLen = secondNumberBin.length() - 1;

        while (firstNumLen >= 0 || secondNumLen >= 0 || digitInMind == 1){
            int firstNum = 0;
            int secondNum = 0;

            if (firstNumLen < 0) {
                firstNum = 0;
            }
            else if (firstNumberBin.charAt(firstNumLen) == '1'){
                firstNum = 1;
            }
            else {
                firstNum = 0;
            }

            if (secondNumLen < 0){
                secondNum = 0;
            }
            else if (secondNumberBin.charAt(secondNumLen) == '1'){
                secondNum = 1;
            }
            else {
                secondNum = 0;
            }

            int summa = firstNum + secondNum + digitInMind;

            switch (summa){
                case 0:
                    digitInMind = 0;
                    currentDigit = "0";
                    break;
                case 1:
                    digitInMind = 0;
                    currentDigit = "1";
                    break;
                case 2:
                    digitInMind = 1;
                    currentDigit = "0";
                    break;
                case 3:
                    digitInMind = 1;
                    currentDigit = "1";
                    break;
            }

            firstNumLen--;
            secondNumLen--;
            result += currentDigit;
        }

        return new StringBuffer(result).reverse().toString();
    }

    public static String subtract(String firstNum, String secondNum, int firstNumberInt, int secondNumberInt){
        String result = "";
        String _1 = "1";
        int cnt = firstNum.length() - secondNum.length();
        char[] additionalCode = new char[firstNum.length()];

        for (int i = 0; i < cnt; i++){
            additionalCode[i] = '1';
        }
        for (int i = cnt; i < additionalCode.length; ++i) {
            if (secondNum.charAt(i - cnt) == '1') {
                additionalCode[i] = '0';
            } else if (secondNum.charAt(i - cnt) == '0') {
                additionalCode[i] = '1';
            }
        }
        String additionalCodeStr = String.valueOf(additionalCode);
        additionalCodeStr = add(additionalCodeStr, _1);
        String summa = add(additionalCodeStr, firstNum);

        if (summa.length() > 1) {
            boolean zeros = true;
            for (int i = 1; i < summa.length(); i++) {
                if (summa.charAt(i) == '1'){
                    zeros = false;
                }
                else {
                    if (zeros) {
                        continue;
                    }
                }
                result += summa.charAt(i);
            }
        }
        if (secondNumberInt > firstNumberInt){
            result = "-" + result;
        }
        return result;
    }

    public static String multiply(String firstNum, int secondNumberInt){
        String result = "";
        for (int i = 0; i < secondNumberInt; i++){
            result = add(result, firstNum);
        }
        return result;
    }

    public static String divide(String firstNum, String secondNum, int firstNumberInt, int secondNumberInt){
        String result = "";
        String finResult = "";
        int cnt = 0;
        for (int i = firstNumberInt; i >= secondNumberInt; i -= secondNumberInt){
            result = subtract(firstNum, secondNum, firstNumberInt, secondNumberInt);
            cnt++;
        }
        finResult = HelperClass.convertTo2Base(cnt, finResult);
        return finResult;
    }
}
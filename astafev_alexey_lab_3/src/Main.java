import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args){
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {

            System.out.print("Введите систему счисления: ");
            int systemNumber = Integer.parseInt(in.readLine());

            System.out.print("Введите первое число: ");
            String firstDig = in.readLine();

            System.out.print("Введите второе число: ");
            String secondDig = in.readLine();

            System.out.print("Знак операции (+,-,*,/) : ");
            char operation = (char)in.read();

            Calculation calculator = new Calculation(systemNumber, firstDig, secondDig, operation);
            System.out.print("Результат: " + calculator.methodCalculation());
        }
        catch (Exception e){
            System.out.println("Введите корректные данные!");
        }
    }
}

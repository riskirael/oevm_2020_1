package com.company;

public class KNF {
    public void KNF(int[][] table, int[] resultColumn) {
        StringBuilder resultString = new StringBuilder();
        for (int i = 0; i < table.length; i++) {
            if (resultColumn[i] == 0) {
                resultString.append("(");
                for (int j = 0; j < table[0].length; j++) {
                    if (table[i][j] == 1) {
                        resultString.append("!x" + j);
                    }
                    if (table[i][j] == 0) {
                        resultString.append("x" + j);
                    }
                    if (j != table[0].length - 1 ) {
                        resultString.append(" + ");
                    }
                }
                resultString.append(")");
            }
        }
        System.out.print(resultString);
    }
}

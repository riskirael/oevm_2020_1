## Лабораторная работа №6

### Студент группы ИСЭбд-21  *Гильманов Артур*

### Ссылка на видео: https://cloud.mail.ru/home/bandicam%202020-11-11%2014-52-30-726.mp4

##### Техническое задание:
Разработать с помощью ассемблера FASM программу для сложения, вычитания,
умножения и деления двух целых чисел.

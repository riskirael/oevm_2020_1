public interface IMinimization {
    void startMinimizing();

    void setTableTruth(int[][] tableTruth);

    StringBuilder getResult();
}

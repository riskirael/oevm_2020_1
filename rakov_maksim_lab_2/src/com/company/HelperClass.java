package com.company;

public class HelperClass {

    public static int convertToDecimal(int startSystem, char[] number) {
        int result = 0;
        int finalIntTen = 10;
        int codeA = 65;
        for (int i = 0; i < number.length; i++) {
            int tmp;
            if (number[i] >= '0' && number[i] <= '9') {
                tmp = number[i] - '0';
            } else {
                tmp = finalIntTen + number[i] - codeA;
            }
            result += tmp * Math.pow(startSystem, number.length - i - 1);
        }
        return result;
    }


    public static String convertToOtherSystem(int startSystem, int endSystem, char[] number) {
        int firstHelper = 48;
        // 49 - 1
        int secondHelper = 55;
        // 'A' - 10
        StringBuilder outcome = new StringBuilder();

        int remainder = convertToDecimal(startSystem, number);
        if (remainder == 0) {
            outcome.append("0");
            return outcome.toString();
        }

        for (int i = 0; remainder > 0; i++) {
            if (remainder % endSystem <= 9) {
                outcome.append((char) (remainder % endSystem + firstHelper));
            } else {
                outcome.append((char) (remainder % endSystem + secondHelper));
            }
            remainder = remainder / endSystem;
        }

        return outcome.reverse().toString();

    }
}



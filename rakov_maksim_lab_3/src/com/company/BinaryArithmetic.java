package com.company;

import java.util.Arrays;

public class BinaryArithmetic {
    //Сложение
    public static String sum(char[] firstDigit, char[] secondDigit) {
        StringBuilder outcome = new StringBuilder();
        char currentDigit;
        int discharge = 0;

        for (int i = firstDigit.length - 1, j = secondDigit.length - 1; i >= 0 || j >= 0 || discharge == 1; --i, --j) {

            int firstBinaryDigit;
            int secondBinaryDigit;

            if (i < 0) {
                firstBinaryDigit = 0;
            } else if (firstDigit[i] == '0') {
                firstBinaryDigit = 0;
            } else {
                firstBinaryDigit = 1;
            }

            if (j < 0) {
                secondBinaryDigit = 0;
            } else if (secondDigit[j] == '0') {
                secondBinaryDigit = 0;
            } else {
                secondBinaryDigit = 1;
            }
            int currentSum = firstBinaryDigit + secondBinaryDigit + discharge;

            if (currentSum == 0) {
                currentDigit = '0';
                discharge = 0;
            } else if (currentSum == 1) {
                currentDigit = '1';
                discharge = 0;
            } else if (currentSum == 2) {
                currentDigit = '0';
                discharge = 1;
            } else {
                currentDigit = '1';
                discharge = 1;
            }
            outcome.insert(0, currentDigit);
        }
        return outcome.toString();
    }

    //Вычитание
    public static String subtract(char[] subtrahend, char[] minuend) {
        String result = "";

        int counter = subtrahend.length - minuend.length;
        char[] minuendAdditional = new char[subtrahend.length];
        // обратный код
        for (int i = 0; i < counter; ++i) {
            minuendAdditional[i] = '1';
        }
        for (int i = counter; i < minuendAdditional.length; ++i) {
            if (minuend[i - counter] == '1') {
                minuendAdditional[i] = '0';
            }
            else {
                minuendAdditional[i] = '1';
            }
        }

        char[] one = {'1'};
        minuendAdditional = sum(minuendAdditional, one).toCharArray();
        char[] dopSum = sum(minuendAdditional, subtrahend).toCharArray();

        if (dopSum.length > 1) {
            boolean insignificantZeros = true;
            for (int i = 1; i < dopSum.length; ++i) {
                if (dopSum[i] == '1') {
                    insignificantZeros = false;
                }
                else {
                    if (insignificantZeros) {
                        continue;
                    }
                }
                result += dopSum[i];
            }
        }
        return result;
    }

    //Умножение
    public static String multiply (char[] firstBinaryDigit, char[] secondBinaryDigit) {
        char[] resultMultiply = {};
        for (int i = 0; i < Helper.convertToDecimal( secondBinaryDigit, 2); i++) {
            resultMultiply = (sum(resultMultiply, firstBinaryDigit).toCharArray());
        }
        return new String(resultMultiply);
    }

    //Деление
    public static String divide (char[] dividend, char[] divider) {
        int resultDivide = 0;
        while (Helper.convertToDecimal( dividend, 2) >= Helper.convertToDecimal(divider,2) && !Arrays.equals(dividend, new char[]{'0'})) {
            dividend = (subtract(dividend, divider).toCharArray());
            resultDivide++;
        }
        return Helper.convertToBinary(resultDivide);
    }

}

public enum LogicalOperations {
    DNF,
    KNF;

    public static LogicalOperations getOperations(String choice) {
        switch (choice) {
            case "ДНФ": {
                return LogicalOperations.DNF;
            }
            case "КНФ": {
                return LogicalOperations.KNF;
            }
        }
        return null;
    }
}


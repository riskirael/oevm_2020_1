format PE console

entry start

include 'win32a.inc'

section '.data' data readable writable


        number1 db 'Enter X ', 0
        number2 db 'Enter Y ', 0
        enterNum db '%d', 0

        X dd ?
        Y dd ?

        sumMessage db 'X + Y = %d', 0dh, 0ah, 0
        subMessage  db 'X - Y = %d', 0dh, 0ah, 0
        mulMessage db 'X * Y = %d', 0dh, 0ah, 0
        divMessage db 'X / Y = %d', 0dh, 0ah, 0

        NULL = 0

section '.idata' import data readable
        library kernel, 'kernel32.dll',\
                msvctr, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvctr,\
               printf, 'printf',\
               getch, '_getch',\
               scanf, 'scanf'

section '.code' code readable executable

        start:
                push number1
                call [printf]

                push X
                push enterNum
                call [scanf]

                push number2
                call [printf]

                push Y
                push enterNum
                call [scanf]

                mov ecx, [X]
                add ecx, [Y]

                push ecx
                push sumMessage
                call [printf]


                mov ecx, [X]
                sub ecx, [Y]
                push ecx
                push subMessage
                call [printf]


                mov ecx, [X]
                imul ecx, [Y]

                push ecx
                push mulMessage
                call [printf]


                mov eax, [X]
                cdq
                mov ecx, [Y]
                idiv ecx

                push eax
                push divMessage
                call [printf]

                call [getch]

                push NULL
                call [ExitProcess]
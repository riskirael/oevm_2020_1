package com.company;

import javax.swing.*;
import java.awt.*;

public class BooleanPanel extends JPanel {
    private final OperationClass operationClass;

    public BooleanPanel(OperationClass operationClass) {
        this.operationClass = operationClass;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        operationClass.draw(g);
    }

}


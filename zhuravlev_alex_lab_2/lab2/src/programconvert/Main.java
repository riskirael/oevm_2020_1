package programconvert;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args) {
        // write your code here
        int initialSS;
        int endSS;
        String numInitialString;

        Scanner in = new Scanner(System.in);

        System.out.print("Введите исходную C.C. : ");
        initialSS = in.nextInt();

        System.out.print("Введите конечную C.C. : ");
        endSS = in.nextInt();

        System.out.print("Введите число в исходной C.C. : ");
        numInitialString = in.next();

        Convertor convert = new Convertor(initialSS, endSS, numInitialString);
        convert.convertSS();
    }
}

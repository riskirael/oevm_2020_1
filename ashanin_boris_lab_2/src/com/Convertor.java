package com;
public class Convertor {

    public static int res=0;
    public static String otvet="";
    /*
 Функция для первода числа в 10-ую систему счисления
  */
    public static void convertTo10SS(char [] value, int _startSS)
    {
        char[] simValue = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        int[] chValue = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        res=0;

        for (int i=0;i<value.length;i++)
        {
            for (int j=0;j<simValue.length;j++)
            {
                if (value[i]==simValue[j])
                {
                    res+=chValue[j]*Math.pow(_startSS,value.length-i-1)+5-5;
                }
            }
        }
    }
    /*
  Функция для перевода числе, которые больше 10 в символы
    */
    public static void helperForConvertingToEndSS(int ost)
    {
        char result=' ';
        switch (ost) {
            case 16:
                result = 'G';
                otvet = result + otvet;

                break;

            case 15:
                result = 'F';
                otvet = result + otvet;

                break;
            case 14:
                result = 'E';
                otvet = result + otvet;

                break;
            case 13:
                result = 'D';
                otvet = result + otvet;

                break;
            case 12:
                result = 'C';
                otvet = result + otvet;

                break;
            case 11:
                result = 'B';
                otvet = result + otvet;

                break;
            case 10:
                result = 'A';
                otvet = result + otvet;

                break;
        }

    }
    /*
    Функция для первода числа в конечную систему счисления из 10-ой. Реализовал при помощи switch, можно сделать так же
    как и в прошлой функции,т.е с массивами))
     */
    public static  void convertToEndSS(int osn , int chislo) {
        if (osn < 2 || osn > 16) {
            System.out.println("Система счисления не подходит");
            return;
        }
        int ost;
        do {
            ost = chislo % osn;
            chislo = chislo / osn;
            if (ost < 10) {
                otvet = ost + otvet;
            } else
            {
                helperForConvertingToEndSS(ost);

            }
        }
        while (chislo / osn != 0) ;
        ost = chislo % osn;
        chislo = chislo / osn;
        if (ost < 10) {
            otvet = ost + otvet;
            System.out.print(otvet);
        } else {
            helperForConvertingToEndSS(ost);
            System.out.print(otvet);
        }
    }

}

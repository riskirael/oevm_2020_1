
import java.io.IOException;
import java.util.Scanner;

public class Laba2 {

    public static void main(String [] args) throws IOException {
        Convertor cv = new Convertor();

        System.out.println("10, 2, 12345");
        System.out.println(cv.baseToBase(10, 2, "12345"));

        System.out.println("2, 10, 11000000111001");
        System.out.println(cv.baseToBase(2, 10, "11000000111001"));
        System.out.println("______\n");

        System.out.println("16, 2, ABC11");
        System.out.println(cv.baseToBase(16, 2, "ABC11"));

        System.out.println("2, 16, 10101011110000010001");
        System.out.println(cv.baseToBase(2, 16, "10101011110000010001"));
        System.out.println("______\n");

        System.out.println("8, 16, 12322");
        System.out.println(cv.baseToBase(8, 16, "12322"));

        System.out.println("16, 8, 14D2");
        System.out.println(cv.baseToBase(16, 8, "14D2"));
        System.out.println("______\n");

        Scanner sc = new Scanner(System.in);
        while (true) {
            int base1 = Integer.parseInt(sc.nextLine());
            int base2 = Integer.parseInt(sc.nextLine());
            String num = sc.nextLine();

            System.out.println(base1 + " " + base2 + " " + num);
            System.out.println(cv.baseToBase(base1, base2, num));
            System.out.println("______\n");
        }
    }
}

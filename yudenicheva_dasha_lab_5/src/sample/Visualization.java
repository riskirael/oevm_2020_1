package sample;

import javax.swing.*;
import java.awt.*;

public class Visualization extends JPanel {
    private final int[][] array;

    public Visualization(int[][] array) {
        this.array = array;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Draw draw = new Draw();
        draw.draw(g, array);
    }
}


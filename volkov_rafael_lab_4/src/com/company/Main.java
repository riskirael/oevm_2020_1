package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int n = 16;
        int m = 4;

        TableOfTrue table = new TableOfTrue(n, m);
        table.printf();

        Scanner in = new Scanner(System.in);
        System.out.println("Введите 1 - для КНФ или 2 - для ДНФ");
        int variable = in.nextInt();

        if (variable == 1) {
            table.KNF();
        } else if (variable == 2) {
            table.DNF();
        }
    }
}

package com.company;

public class HelperClass {

    static int X1 = 0;
    static int X2 = 1;
    static int X3 = 2;
    static int X4 = 3;

    public static void function (int [][] arr, int sizeColumn, int sizeStr){
        for(int i = 0; i < sizeStr; i++){
            for( int j = 0; j < sizeColumn; j++){
                if(j == 4){
                    System.out.printf(" = ");
                }
                System.out.printf(String.valueOf(arr[i][j]));
            }
            System.out.printf("\n");
        }
    }

    public static String functionKNF (int[][] arr, int sizeColumn, int sizeStr) {
        String result = "";
        int count = 0;
        for (int i = 0; i < sizeStr; i++) {
            if (arr[i][sizeColumn - 1] == 0){
                if(count > 0){
                    result = result + "*";
                }
                if(arr[i][X1] == 1){
                    result = result + "(-X1";
                }
                else {
                    result = result + "(X1";
                }
                if(arr[i][X2] == 1){
                    result = result + "-X2";
                }
                else {
                    result = result + "+X2";
                }
                if(arr[i][X3] == 1){
                    result = result + "-X3";
                }
                else {
                    result = result + "+X3";
                }
                if(arr[i][X4] == 1){
                    result = result + "-X4)";
                }
                else {
                    result = result + "+X4)";
                }
                count++;
            }
        }
        return result;
    }

    public static String functionDNF (int[][] arr, int sizeColumn, int sizeStr){
        String result = "";
        int count = 0;
        for (int i = 0; i < sizeStr; i++) {
            if (arr[i][sizeColumn - 1] == 1){
                if(count > 0){
                    result = result + "+";
                }
                if(arr[i][X1] == 0){
                    result = result + "(-X1";
                }
                else {
                    result = result + "(X1";
                }
                if(arr[i][X2] == 0){
                    result = result + "*-X2";
                }
                else {
                    result = result + "*X2";
                }
                if(arr[i][X3] == 0){
                    result = result + "*-X3";
                }
                else {
                    result = result + "*X3";
                }
                if(arr[i][X4] == 0){
                    result = result + "*-X4)";
                }
                else {
                    result = result + "*X4)";
                }
                count++;
            }
        }
        return result;
    }
}

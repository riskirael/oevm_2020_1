package programconvert;

public class Convertor
{
    private int initialSS = 0;
    private int endSS = 0;
    private String numInitialString = "";

    public Convertor(int initialSS, int finalSS, String numInitialString ){
        this.initialSS = initialSS;
        this.endSS = finalSS;
        this.numInitialString = numInitialString;
    }

    private int convertToDecimalNumber(char[] arrChar){
        int numIn10ss = 0;
        int degree = 0;
        int cash = 0;
        int min = 2;
        int max = 16;
        int assistan = 10;

        if(initialSS < min || initialSS > max || endSS < min || endSS > max){
            return -1;
        }
        for(int i = arrChar.length - 1; i >= 0; i--){
            if(arrChar[i] >= '0' && arrChar[i] <= '9'){
                cash = Character.getNumericValue(arrChar[i]);
            } else{
                if(arrChar[i] >= 'A' && arrChar[i] <= 'F'){
                    cash = arrChar[i] - 'A' + assistan;
                } else{
                    return -1;
                }
            }
            if(cash >= initialSS){
                return -1;
            }

            numIn10ss += cash * Math.pow(initialSS, degree);
            degree++;
        }
        return numIn10ss;
    }

    private StringBuilder convertNumInFinalSS(int numIn10ss){
        int assistan = 10;
        int assistan2 = 9;
        StringBuilder finalNumStr = new StringBuilder();

        if(numIn10ss == 0){
            finalNumStr.append(0);
            return finalNumStr;
        }

        while(numIn10ss != 0){
            if(numIn10ss % endSS > assistan2) {
                finalNumStr.append((char)(numIn10ss - assistan + 'A'));
            } else{
                finalNumStr.append(numIn10ss % endSS);
            }
            numIn10ss = numIn10ss / endSS;
        }

        finalNumStr.reverse();
        return finalNumStr;
    }

    public void convertSS(){
        char[] arrChar = numInitialString.toCharArray();
        int numIn10SS = 0;
        StringBuilder endNumStr;

        numIn10SS = convertToDecimalNumber(arrChar);

        if(numIn10SS == -1){
            System.out.println("Error!");
            return;
        }
        endNumStr = convertNumInFinalSS(numIn10SS);
        System.out.println(endNumStr);
    }
}

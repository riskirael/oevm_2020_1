## Лабораторная работа №5
## ПИбд-21 Кочетков Тимофей

Разработать программный продукт для визуализации булевых функций
с помощью простых логический элементов.
При запуске программы случайным образом заполняются
значения функции в таблице истинности.
Пользователь указывает конечную форму логической функции
ДНФ или КНФ. После расчетов программа выводит функцию в выбранной форме.

### Как работает:

Ссылка на видео: https://drive.google.com/drive/folders/18shpzaA-oNzMfjRe9Oit_A7LbNQVN3sk?usp=sharing
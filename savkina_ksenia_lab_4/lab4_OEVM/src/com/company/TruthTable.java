package com.company;

import java.util.Random;

public class TruthTable {

    final private int[][] truthTable = createTable();

    /**
     * Создание таблицы
     */
    private int[][] createTable() {
        Random random = new Random();
        int[][] truthTableArray = new int[][]{
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)}
        };
        return truthTableArray;
    }

    /**
     * Вывод таблицы
     */
    public String printTruthTable() {
        String str = "";
        String[] header= {"x1", "x2","x3","x4","F"};
        str += "|";
        for (int j = 0; j < header.length; j++) {
            str += "| " + header[j] + " |";
        }
        str += "|" + "\n" + "-------------------------------" + "\n";
        for (int i = 0; i < truthTable.length; i++) {
            str += "|";
            for (int j = 0; j < truthTable[i].length; j++) {
                str += "  " + truthTable[i][j] + "  |";
            }
            str += "\n";
        }
        return str;
    }

    /**
     * Представление в ДНФ
     */
    public String presentInDNF() {
        String str = "";
        int f;
        boolean isFirstPassage = true;
        for (int i = 0; i < truthTable.length; i++) {
            f = truthTable[i].length - 1;
            if (truthTable[i][f] == 1) {
                if(isFirstPassage) {
                    str += "(";
                    isFirstPassage = false;
                }
                else
                    str += " + (";
                for (int j = 0; j < f; j++) {
                    if (truthTable[i][j] == 0)
                        str += "!" + getHeaderValue(j);
                    else
                        str += getHeaderValue(j);
                    if (j != f - 1)
                        str += " * ";
                }
                str += ")";
            }
        }
        return str;
    }

    /**
     * Представление в КНФ
     */
    public String presentInKNF() {
        String str = "";
        int f;
        boolean isFirstPassage = true;
        for (int i = 0; i < truthTable.length; i++) {
            f = truthTable[i].length - 1;
            if (truthTable[i][f] == 0) {
                if(isFirstPassage) {
                    str += "(";
                    isFirstPassage = false;
                }
                else
                    str += " * (";
                for (int j = 0; j < f; j++) {
                    if (truthTable[i][j] == 1)
                        str += "!" + getHeaderValue(j);
                    else
                        str += getHeaderValue(j);
                    if (j != f - 1)
                        str += " + ";
                }
                str += ")";
            }
        }
        return str;
    }

    /**
     * Получение переменной текеущего столбца
     * @param j текущий столбец
     */
    private String getHeaderValue(int j) {
        switch (j) {
            case 0:
                return "x1";
            case 1:
                return "x2";
            case 2:
                return "x3";
            case 3:
                return "x4";
            default:
                return "ошибка";
        }
    }
}

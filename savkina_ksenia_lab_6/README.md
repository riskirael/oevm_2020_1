## Лабораторная работа №6
### Основы программирования на языке ассемблера
#### Выполнила студентка группы ПИбд-21 __Савкина Ксения__
__Описание__: программа для сложения, вычитания, умножения и деления двух целых чисел. Данные могут быть введены с клавиатуры либо заданы в виде констант. Результатом работы программы служит текстовый вывод следующего вида:

X = < num1 >;

Y = < num2 >;

X + Y = < res1 >;

X – Y = < res2 >;

X * Y = < res3 >;

X / Y = < res4 >;

__Язык программировния__: `Assembler` 

__Среда разработки__: `flat assembler` 

[Ссылка на видео](https://youtu.be/M1HNUY0y39k "Работа программы")

__Тесты__:

X | Y | X + Y | X - Y | X * Y | X / Y
:-------|:-------:|:-------:|:-------:|:-------:|-------:
888 | 444 | 1332 | 444 | 394272 | 2
741 | 62 | 803 | 679 | 45942 | 11
